// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package bindings

import (
	"errors"
	"math/big"
	"strings"

	"gitlab.com/aklin/linkch"
	"gitlab.com/aklin/linkch/accounts/abi"
	"gitlab.com/aklin/linkch/accounts/abi/bind"
	"gitlab.com/aklin/linkch/common"
	"gitlab.com/aklin/linkch/core/types"
	"gitlab.com/aklin/linkch/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
	_ = abi.ConvertType
)

// AncestorMetaData contains all meta data concerning the Ancestor contract.
var AncestorMetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"AncItem2Set\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"AncItemSet\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"ancestorPublicData\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"version\",\"type\":\"string\"},{\"internalType\":\"uint256\",\"name\":\"i\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"f1\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"}],\"name\":\"getAncestorItem\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"}],\"name\":\"getAncestorItem2\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getAncestorVersion\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"ret\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"setAncestorItem\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"setAncestorItem2\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"value\",\"type\":\"string\"}],\"name\":\"setAncestorVersion\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Bin: "0x608060405234801561001057600080fd5b50610e2a806100206000396000f3fe608060405234801561001057600080fd5b50600436106100885760003560e01c806395522e7e1161005b57806395522e7e1461012a578063a911ed9414610146578063c27fc30514610178578063dc9a052b1461019657610088565b8063059ff0041461008d578063468d5ac2146100ab57806376d695a1146100c757806378127ba7146100f9575b600080fd5b6100956101b2565b6040516100a29190610c52565b60405180910390f35b6100c560048036038101906100c09190610a39565b610281565b005b6100e160048036038101906100dc9190610a10565b61047a565b6040516100f093929190610bd6565b60405180910390f35b610113600480360381019061010e9190610add565b61050c565b604051610121929190610c74565b60405180910390f35b610144600480360381019061013f9190610a9c565b6105c8565b005b610160600480360381019061015b9190610a10565b61069c565b60405161016f93929190610bd6565b60405180910390f35b61018061072e565b60405161018d9190610cc4565b60405180910390f35b6101b060048036038101906101ab9190610a39565b610734565b005b606060026000806000600181526020019081526020016000206000015481526020019081526020016000206000018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156102745780601f1061024957610100808354040283529160200191610274565b820191906000526020600020905b81548152906001019060200180831161025757829003601f168201915b5050505050905080905090565b600160006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610326576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161031d90610ca4565b60405180910390fd5b8260016000600181526020019081526020016000206001016000868152602001908152602001600020600001819055508160016000600181526020019081526020016000206001016000868152602001908152602001600020600101819055508060016000600181526020019081526020016000206001016000868152602001908152602001600020600201819055507f08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be38460016000600181526020019081526020016000206001016000878152602001908152602001600020600001546001600060018152602001908152602001600020600101600088815260200190815260200160002060010154600160006001815260200190815260200160002060010160008981526020019081526020016000206002015460405161046c9493929190610c0d565b60405180910390a150505050565b6000806000610487610899565b6001600080600060018152602001908152602001600020600001548152602001908152602001600020600101600086815260200190815260200160002060405180606001604052908160008201548152602001600182015481526020016002820154815250509050806000015181602001518260400151935093509350509193909250565b6002602052806000526040600020600091509050806000018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156105b85780601f1061058d576101008083540402835291602001916105b8565b820191906000526020600020905b81548152906001019060200180831161059b57829003601f168201915b5050505050908060010154905082565b600160006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161461066d576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161066490610ca4565b60405180910390fd5b80600260006001815260200190815260200160002060000190805190602001906106989291906108c3565b5050565b60008060006106a9610943565b6001600080600060018152602001908152602001600020600001548152602001908152602001600020600201600086815260200190815260200160002060405180606001604052908160008201548152602001600182015481526020016002820154815250509050806000015181602001518260400151935093509350509193909250565b60065481565b600160006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16146107d9576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016107d090610ca4565b60405180910390fd5b6107e1610943565b83816000018181525050828160200181815250508181604001818152505080600160006001815260200190815260200160002060020160008781526020019081526020016000206000820151816000015560208201518160010155604082015181600201559050507f4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd8582600001518360200151846040015160405161088a9493929190610c0d565b60405180910390a15050505050565b60405180606001604052806000801916815260200160008019168152602001600080191681525090565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061090457805160ff1916838001178555610932565b82800160010185558215610932579182015b82811115610931578251825591602001919060010190610916565b5b50905061093f919061096d565b5090565b60405180606001604052806000801916815260200160008019168152602001600080191681525090565b61098f91905b8082111561098b576000816000905550600101610973565b5090565b90565b6000813590506109a181610dc6565b92915050565b600082601f8301126109b857600080fd5b81356109cb6109c682610d0c565b610cdf565b915080825260208301602083018583830111156109e757600080fd5b6109f2838284610d73565b50505092915050565b600081359050610a0a81610ddd565b92915050565b600060208284031215610a2257600080fd5b6000610a3084828501610992565b91505092915050565b60008060008060808587031215610a4f57600080fd5b6000610a5d87828801610992565b9450506020610a6e87828801610992565b9350506040610a7f87828801610992565b9250506060610a9087828801610992565b91505092959194509250565b600060208284031215610aae57600080fd5b600082013567ffffffffffffffff811115610ac857600080fd5b610ad4848285016109a7565b91505092915050565b600060208284031215610aef57600080fd5b6000610afd848285016109fb565b91505092915050565b610b0f81610d5f565b82525050565b6000610b2082610d43565b610b2a8185610d4e565b9350610b3a818560208601610d82565b610b4381610db5565b840191505092915050565b6000610b5982610d38565b610b638185610d4e565b9350610b73818560208601610d82565b610b7c81610db5565b840191505092915050565b6000610b94601c83610d4e565b91507f4f6e6c79206f776e65722063616e20657865632066756e6374696f6e000000006000830152602082019050919050565b610bd081610d69565b82525050565b6000606082019050610beb6000830186610b06565b610bf86020830185610b06565b610c056040830184610b06565b949350505050565b6000608082019050610c226000830187610b06565b610c2f6020830186610b06565b610c3c6040830185610b06565b610c496060830184610b06565b95945050505050565b60006020820190508181036000830152610c6c8184610b15565b905092915050565b60006040820190508181036000830152610c8e8185610b4e565b9050610c9d6020830184610bc7565b9392505050565b60006020820190508181036000830152610cbd81610b87565b9050919050565b6000602082019050610cd96000830184610bc7565b92915050565b6000604051905081810181811067ffffffffffffffff82111715610d0257600080fd5b8060405250919050565b600067ffffffffffffffff821115610d2357600080fd5b601f19601f8301169050602081019050919050565b600081519050919050565b600081519050919050565b600082825260208201905092915050565b6000819050919050565b6000819050919050565b82818337600083830152505050565b60005b83811015610da0578082015181840152602081019050610d85565b83811115610daf576000848401525b50505050565b6000601f19601f8301169050919050565b610dcf81610d5f565b8114610dda57600080fd5b50565b610de681610d69565b8114610df157600080fd5b5056fea26469706673582212203013c7fd9961fbb7ecd123f7f5a927ad1d076ecfe5c6399162f7eb0d7b6bda5a64736f6c63430006040033",
}

// AncestorABI is the input ABI used to generate the binding from.
// Deprecated: Use AncestorMetaData.ABI instead.
var AncestorABI = AncestorMetaData.ABI

// AncestorBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use AncestorMetaData.Bin instead.
var AncestorBin = AncestorMetaData.Bin

// DeployAncestor deploys a new Ethereum contract, binding an instance of Ancestor to it.
func DeployAncestor(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *Ancestor, error) {
	parsed, err := AncestorMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(AncestorBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Ancestor{AncestorCaller: AncestorCaller{contract: contract}, AncestorTransactor: AncestorTransactor{contract: contract}, AncestorFilterer: AncestorFilterer{contract: contract}}, nil
}

// DeployAncestorSync deploys a new Ethereum contract and waits for receipt, binding an instance of AncestorSession to it.
func DeployAncestorSync(session *bind.TransactSession, backend bind.ContractBackend) (*types.Transaction, *types.Receipt, *AncestorSession, error) {
	parsed, err := abi.JSON(strings.NewReader(AncestorABI))
	if err != nil {
		return nil, nil, nil, err
	}
	session.Lock()
	address, tx, _, err := bind.DeployContract(session.TransactOpts, parsed, common.FromHex(AncestorBin), backend)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	receipt, err := session.WaitTransaction(tx)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	session.TransactOpts.Nonce.Add(session.TransactOpts.Nonce, big.NewInt(1))
	session.Unlock()
	contractSession, err := NewAncestorSession(address, backend, session)
	return tx, receipt, contractSession, err
}

// Ancestor is an auto generated Go binding around an Ethereum contract.
type Ancestor struct {
	AncestorCaller     // Read-only binding to the contract
	AncestorTransactor // Write-only binding to the contract
	AncestorFilterer   // Log filterer for contract events
}

// AncestorCaller is an auto generated read-only Go binding around an Ethereum contract.
type AncestorCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AncestorTransactor is an auto generated write-only Go binding around an Ethereum contract.
type AncestorTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AncestorFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type AncestorFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AncestorSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type AncestorSession struct {
	Contract           *Ancestor // Generic contract binding to set the session for
	transactionSession *bind.TransactSession
	Address            common.Address
}

// AncestorCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type AncestorCallerSession struct {
	Contract *AncestorCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// AncestorTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type AncestorTransactorSession struct {
	Contract     *AncestorTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// AncestorRaw is an auto generated low-level Go binding around an Ethereum contract.
type AncestorRaw struct {
	Contract *Ancestor // Generic contract binding to access the raw methods on
}

// AncestorCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type AncestorCallerRaw struct {
	Contract *AncestorCaller // Generic read-only contract binding to access the raw methods on
}

// AncestorTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type AncestorTransactorRaw struct {
	Contract *AncestorTransactor // Generic write-only contract binding to access the raw methods on
}

// NewAncestor creates a new instance of Ancestor, bound to a specific deployed contract.
func NewAncestor(address common.Address, backend bind.ContractBackend) (*Ancestor, error) {
	contract, err := bindAncestor(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Ancestor{AncestorCaller: AncestorCaller{contract: contract}, AncestorTransactor: AncestorTransactor{contract: contract}, AncestorFilterer: AncestorFilterer{contract: contract}}, nil
}

// NewAncestorCaller creates a new read-only instance of Ancestor, bound to a specific deployed contract.
func NewAncestorCaller(address common.Address, caller bind.ContractCaller) (*AncestorCaller, error) {
	contract, err := bindAncestor(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &AncestorCaller{contract: contract}, nil
}

// NewAncestorTransactor creates a new write-only instance of Ancestor, bound to a specific deployed contract.
func NewAncestorTransactor(address common.Address, transactor bind.ContractTransactor) (*AncestorTransactor, error) {
	contract, err := bindAncestor(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &AncestorTransactor{contract: contract}, nil
}

// NewAncestorFilterer creates a new log filterer instance of Ancestor, bound to a specific deployed contract.
func NewAncestorFilterer(address common.Address, filterer bind.ContractFilterer) (*AncestorFilterer, error) {
	contract, err := bindAncestor(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &AncestorFilterer{contract: contract}, nil
}

func NewAncestorSession(address common.Address, backend bind.ContractBackend, transactionSession *bind.TransactSession) (*AncestorSession, error) {
	AncestorInstance, err := NewAncestor(address, backend)
	if err != nil {
		return nil, err
	}
	return &AncestorSession{
		Contract:           AncestorInstance,
		transactionSession: transactionSession,
		Address:            address,
	}, nil
}

// bindAncestor binds a generic wrapper to an already deployed contract.
func bindAncestor(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := AncestorMetaData.GetAbi()
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, *parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Ancestor *AncestorRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Ancestor.Contract.AncestorCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Ancestor *AncestorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Ancestor.Contract.AncestorTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Ancestor *AncestorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Ancestor.Contract.AncestorTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Ancestor *AncestorCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Ancestor.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Ancestor *AncestorTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Ancestor.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Ancestor *AncestorTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Ancestor.Contract.contract.Transact(opts, method, params...)
}

// AncestorPublicData is a free data retrieval call binding the contract method 0x78127ba7.
//
// Solidity: function ancestorPublicData(uint256 ) view returns(string version, uint256 i)
func (_Ancestor *AncestorCaller) AncestorPublicData(opts *bind.CallOpts, arg0 *big.Int) (struct {
	Version string
	I       *big.Int
}, error) {
	var out []interface{}
	err := _Ancestor.contract.Call(opts, &out, "ancestorPublicData", arg0)

	outstruct := new(struct {
		Version string
		I       *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Version = *abi.ConvertType(out[0], new(string)).(*string)
	outstruct.I = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// AncestorPublicData is a free data retrieval call binding the contract method 0x78127ba7.
//
// Solidity: function ancestorPublicData(uint256 ) view returns(string version, uint256 i)
func (_Ancestor *AncestorSession) AncestorPublicData(arg0 *big.Int) (struct {
	Version string
	I       *big.Int
}, error) {
	return _Ancestor.Contract.AncestorPublicData(_Ancestor.transactionSession.CallOpts, arg0)
}

// AncestorPublicData is a free data retrieval call binding the contract method 0x78127ba7.
//
// Solidity: function ancestorPublicData(uint256 ) view returns(string version, uint256 i)
func (_Ancestor *AncestorCallerSession) AncestorPublicData(arg0 *big.Int) (struct {
	Version string
	I       *big.Int
}, error) {
	return _Ancestor.Contract.AncestorPublicData(&_Ancestor.CallOpts, arg0)
}

// F1 is a free data retrieval call binding the contract method 0xc27fc305.
//
// Solidity: function f1() view returns(uint256)
func (_Ancestor *AncestorCaller) F1(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Ancestor.contract.Call(opts, &out, "f1")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// F1 is a free data retrieval call binding the contract method 0xc27fc305.
//
// Solidity: function f1() view returns(uint256)
func (_Ancestor *AncestorSession) F1() (*big.Int, error) {
	return _Ancestor.Contract.F1(_Ancestor.transactionSession.CallOpts)
}

// F1 is a free data retrieval call binding the contract method 0xc27fc305.
//
// Solidity: function f1() view returns(uint256)
func (_Ancestor *AncestorCallerSession) F1() (*big.Int, error) {
	return _Ancestor.Contract.F1(&_Ancestor.CallOpts)
}

// GetAncestorItem is a free data retrieval call binding the contract method 0x76d695a1.
//
// Solidity: function getAncestorItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Ancestor *AncestorCaller) GetAncestorItem(opts *bind.CallOpts, key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	var out []interface{}
	err := _Ancestor.contract.Call(opts, &out, "getAncestorItem", key)

	if err != nil {
		return *new([32]byte), *new([32]byte), *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)
	out1 := *abi.ConvertType(out[1], new([32]byte)).(*[32]byte)
	out2 := *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)

	return out0, out1, out2, err

}

// GetAncestorItem is a free data retrieval call binding the contract method 0x76d695a1.
//
// Solidity: function getAncestorItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Ancestor *AncestorSession) GetAncestorItem(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Ancestor.Contract.GetAncestorItem(_Ancestor.transactionSession.CallOpts, key)
}

// GetAncestorItem is a free data retrieval call binding the contract method 0x76d695a1.
//
// Solidity: function getAncestorItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Ancestor *AncestorCallerSession) GetAncestorItem(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Ancestor.Contract.GetAncestorItem(&_Ancestor.CallOpts, key)
}

// GetAncestorItem2 is a free data retrieval call binding the contract method 0xa911ed94.
//
// Solidity: function getAncestorItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Ancestor *AncestorCaller) GetAncestorItem2(opts *bind.CallOpts, key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	var out []interface{}
	err := _Ancestor.contract.Call(opts, &out, "getAncestorItem2", key)

	if err != nil {
		return *new([32]byte), *new([32]byte), *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)
	out1 := *abi.ConvertType(out[1], new([32]byte)).(*[32]byte)
	out2 := *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)

	return out0, out1, out2, err

}

// GetAncestorItem2 is a free data retrieval call binding the contract method 0xa911ed94.
//
// Solidity: function getAncestorItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Ancestor *AncestorSession) GetAncestorItem2(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Ancestor.Contract.GetAncestorItem2(_Ancestor.transactionSession.CallOpts, key)
}

// GetAncestorItem2 is a free data retrieval call binding the contract method 0xa911ed94.
//
// Solidity: function getAncestorItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Ancestor *AncestorCallerSession) GetAncestorItem2(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Ancestor.Contract.GetAncestorItem2(&_Ancestor.CallOpts, key)
}

// GetAncestorVersion is a free data retrieval call binding the contract method 0x059ff004.
//
// Solidity: function getAncestorVersion() view returns(string ret)
func (_Ancestor *AncestorCaller) GetAncestorVersion(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Ancestor.contract.Call(opts, &out, "getAncestorVersion")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// GetAncestorVersion is a free data retrieval call binding the contract method 0x059ff004.
//
// Solidity: function getAncestorVersion() view returns(string ret)
func (_Ancestor *AncestorSession) GetAncestorVersion() (string, error) {
	return _Ancestor.Contract.GetAncestorVersion(_Ancestor.transactionSession.CallOpts)
}

// GetAncestorVersion is a free data retrieval call binding the contract method 0x059ff004.
//
// Solidity: function getAncestorVersion() view returns(string ret)
func (_Ancestor *AncestorCallerSession) GetAncestorVersion() (string, error) {
	return _Ancestor.Contract.GetAncestorVersion(&_Ancestor.CallOpts)
}

// SetAncestorItem is a paid mutator transaction binding the contract method 0x468d5ac2.
//
// Solidity: function setAncestorItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Ancestor *AncestorTransactor) SetAncestorItem(opts *bind.TransactOpts, key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Ancestor.contract.Transact(opts, "setAncestorItem", key, value, dataType, dataDescr)
}

// SetAncestorItem is a paid mutator transaction binding the contract method 0x468d5ac2.
//
// Solidity: function setAncestorItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Ancestor *AncestorSession) SetAncestorItem(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, *types.Receipt, error) {
	_Ancestor.transactionSession.Lock()
	tx, err := _Ancestor.Contract.SetAncestorItem(_Ancestor.transactionSession.TransactOpts, key, value, dataType, dataDescr)
	if err != nil {
		_Ancestor.transactionSession.Unlock()
		return nil, nil, err
	}
	_Ancestor.transactionSession.TransactOpts.Nonce.Add(_Ancestor.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Ancestor.transactionSession.Unlock()
	receipt, err := _Ancestor.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetAncestorItem is a paid mutator transaction binding the contract method 0x468d5ac2.
//
// Solidity: function setAncestorItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Ancestor *AncestorTransactorSession) SetAncestorItem(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Ancestor.Contract.SetAncestorItem(&_Ancestor.TransactOpts, key, value, dataType, dataDescr)
}

// SetAncestorItem2 is a paid mutator transaction binding the contract method 0xdc9a052b.
//
// Solidity: function setAncestorItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Ancestor *AncestorTransactor) SetAncestorItem2(opts *bind.TransactOpts, key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Ancestor.contract.Transact(opts, "setAncestorItem2", key, value, dataType, dataDescr)
}

// SetAncestorItem2 is a paid mutator transaction binding the contract method 0xdc9a052b.
//
// Solidity: function setAncestorItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Ancestor *AncestorSession) SetAncestorItem2(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, *types.Receipt, error) {
	_Ancestor.transactionSession.Lock()
	tx, err := _Ancestor.Contract.SetAncestorItem2(_Ancestor.transactionSession.TransactOpts, key, value, dataType, dataDescr)
	if err != nil {
		_Ancestor.transactionSession.Unlock()
		return nil, nil, err
	}
	_Ancestor.transactionSession.TransactOpts.Nonce.Add(_Ancestor.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Ancestor.transactionSession.Unlock()
	receipt, err := _Ancestor.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetAncestorItem2 is a paid mutator transaction binding the contract method 0xdc9a052b.
//
// Solidity: function setAncestorItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Ancestor *AncestorTransactorSession) SetAncestorItem2(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Ancestor.Contract.SetAncestorItem2(&_Ancestor.TransactOpts, key, value, dataType, dataDescr)
}

// SetAncestorVersion is a paid mutator transaction binding the contract method 0x95522e7e.
//
// Solidity: function setAncestorVersion(string value) returns()
func (_Ancestor *AncestorTransactor) SetAncestorVersion(opts *bind.TransactOpts, value string) (*types.Transaction, error) {
	return _Ancestor.contract.Transact(opts, "setAncestorVersion", value)
}

// SetAncestorVersion is a paid mutator transaction binding the contract method 0x95522e7e.
//
// Solidity: function setAncestorVersion(string value) returns()
func (_Ancestor *AncestorSession) SetAncestorVersion(value string) (*types.Transaction, *types.Receipt, error) {
	_Ancestor.transactionSession.Lock()
	tx, err := _Ancestor.Contract.SetAncestorVersion(_Ancestor.transactionSession.TransactOpts, value)
	if err != nil {
		_Ancestor.transactionSession.Unlock()
		return nil, nil, err
	}
	_Ancestor.transactionSession.TransactOpts.Nonce.Add(_Ancestor.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Ancestor.transactionSession.Unlock()
	receipt, err := _Ancestor.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetAncestorVersion is a paid mutator transaction binding the contract method 0x95522e7e.
//
// Solidity: function setAncestorVersion(string value) returns()
func (_Ancestor *AncestorTransactorSession) SetAncestorVersion(value string) (*types.Transaction, error) {
	return _Ancestor.Contract.SetAncestorVersion(&_Ancestor.TransactOpts, value)
}

// AncestorAncItem2SetIterator is returned from FilterAncItem2Set and is used to iterate over the raw logs and unpacked data for AncItem2Set events raised by the Ancestor contract.
type AncestorAncItem2SetIterator struct {
	Event *AncestorAncItem2Set // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *AncestorAncItem2SetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(AncestorAncItem2Set)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(AncestorAncItem2Set)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *AncestorAncItem2SetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *AncestorAncItem2SetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// AncestorAncItem2Set represents a AncItem2Set event raised by the Ancestor contract.
type AncestorAncItem2Set struct {
	Key       [32]byte
	Value     [32]byte
	DataType  [32]byte
	DataDescr [32]byte
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterAncItem2Set is a free log retrieval operation binding the contract event 0x4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd.
//
// Solidity: event AncItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Ancestor *AncestorFilterer) FilterAncItem2Set(opts *bind.FilterOpts) (*AncestorAncItem2SetIterator, error) {

	logs, sub, err := _Ancestor.contract.FilterLogs(opts, "AncItem2Set")
	if err != nil {
		return nil, err
	}
	return &AncestorAncItem2SetIterator{contract: _Ancestor.contract, event: "AncItem2Set", logs: logs, sub: sub}, nil
}

// WatchAncItem2Set is a free log subscription operation binding the contract event 0x4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd.
//
// Solidity: event AncItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Ancestor *AncestorFilterer) WatchAncItem2Set(opts *bind.WatchOpts, sink chan<- *AncestorAncItem2Set) (event.Subscription, error) {

	logs, sub, err := _Ancestor.contract.WatchLogs(opts, "AncItem2Set")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(AncestorAncItem2Set)
				if err := _Ancestor.contract.UnpackLog(event, "AncItem2Set", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAncItem2Set is a log parse operation binding the contract event 0x4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd.
//
// Solidity: event AncItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Ancestor *AncestorFilterer) ParseAncItem2Set(log types.Log) (*AncestorAncItem2Set, error) {
	event := new(AncestorAncItem2Set)
	if err := _Ancestor.contract.UnpackLog(event, "AncItem2Set", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// AncestorAncItemSetIterator is returned from FilterAncItemSet and is used to iterate over the raw logs and unpacked data for AncItemSet events raised by the Ancestor contract.
type AncestorAncItemSetIterator struct {
	Event *AncestorAncItemSet // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *AncestorAncItemSetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(AncestorAncItemSet)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(AncestorAncItemSet)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *AncestorAncItemSetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *AncestorAncItemSetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// AncestorAncItemSet represents a AncItemSet event raised by the Ancestor contract.
type AncestorAncItemSet struct {
	Key       [32]byte
	Value     [32]byte
	DataType  [32]byte
	DataDescr [32]byte
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterAncItemSet is a free log retrieval operation binding the contract event 0x08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be3.
//
// Solidity: event AncItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Ancestor *AncestorFilterer) FilterAncItemSet(opts *bind.FilterOpts) (*AncestorAncItemSetIterator, error) {

	logs, sub, err := _Ancestor.contract.FilterLogs(opts, "AncItemSet")
	if err != nil {
		return nil, err
	}
	return &AncestorAncItemSetIterator{contract: _Ancestor.contract, event: "AncItemSet", logs: logs, sub: sub}, nil
}

// WatchAncItemSet is a free log subscription operation binding the contract event 0x08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be3.
//
// Solidity: event AncItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Ancestor *AncestorFilterer) WatchAncItemSet(opts *bind.WatchOpts, sink chan<- *AncestorAncItemSet) (event.Subscription, error) {

	logs, sub, err := _Ancestor.contract.WatchLogs(opts, "AncItemSet")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(AncestorAncItemSet)
				if err := _Ancestor.contract.UnpackLog(event, "AncItemSet", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAncItemSet is a log parse operation binding the contract event 0x08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be3.
//
// Solidity: event AncItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Ancestor *AncestorFilterer) ParseAncItemSet(log types.Log) (*AncestorAncItemSet, error) {
	event := new(AncestorAncItemSet)
	if err := _Ancestor.contract.UnpackLog(event, "AncItemSet", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
