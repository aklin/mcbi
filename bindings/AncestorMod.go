// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package bindings

import (
	"errors"
	"math/big"
	"strings"

	"gitlab.com/aklin/linkch"
	"gitlab.com/aklin/linkch/accounts/abi"
	"gitlab.com/aklin/linkch/accounts/abi/bind"
	"gitlab.com/aklin/linkch/common"
	"gitlab.com/aklin/linkch/core/types"
	"gitlab.com/aklin/linkch/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
	_ = abi.ConvertType
)

// AncestorModMetaData contains all meta data concerning the AncestorMod contract.
var AncestorModMetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"AncItem2Set\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"AncItemSet\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"uint16\",\"name\":\"sfTiny\",\"type\":\"uint16\"},{\"indexed\":false,\"internalType\":\"bool\",\"name\":\"sfBool\",\"type\":\"bool\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"sfInt\",\"type\":\"uint256\"}],\"name\":\"AncModItem2Set\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"uint16\",\"name\":\"sfTiny\",\"type\":\"uint16\"},{\"indexed\":false,\"internalType\":\"bool\",\"name\":\"sfBool\",\"type\":\"bool\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"sfInt\",\"type\":\"uint256\"}],\"name\":\"AncModItemSet\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"ancestorPublicData\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"version\",\"type\":\"string\"},{\"internalType\":\"uint256\",\"name\":\"i\",\"type\":\"uint256\"},{\"internalType\":\"uint16\",\"name\":\"suffixTiny\",\"type\":\"uint16\"},{\"internalType\":\"bool\",\"name\":\"suffixBool\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"suffixInt\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"f1\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"}],\"name\":\"getAncestorItem\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"}],\"name\":\"getAncestorItem2\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"}],\"name\":\"getAncestorModItem\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"uint16\",\"name\":\"\",\"type\":\"uint16\"},{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"}],\"name\":\"getAncestorModItem2\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"uint16\",\"name\":\"\",\"type\":\"uint16\"},{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getAncestorVersion\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"ret\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"setAncestorItem\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"setAncestorItem2\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"},{\"internalType\":\"uint16\",\"name\":\"sfTiny\",\"type\":\"uint16\"},{\"internalType\":\"bool\",\"name\":\"sfBool\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"sfInt\",\"type\":\"uint256\"}],\"name\":\"setAncestorModItem\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"},{\"internalType\":\"uint16\",\"name\":\"sfTiny\",\"type\":\"uint16\"},{\"internalType\":\"bool\",\"name\":\"sfBool\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"sfInt\",\"type\":\"uint256\"}],\"name\":\"setAncestorModItem2\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"value\",\"type\":\"string\"}],\"name\":\"setAncestorVersion\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Bin: "0x608060405234801561001057600080fd5b5061198d806100206000396000f3fe608060405234801561001057600080fd5b50600436106100b45760003560e01c806395522e7e1161007157806395522e7e146101aa5780639e7c1bf2146101c6578063a911ed94146101fb578063b9e9abc31461022d578063c27fc30514610249578063dc9a052b14610267576100b4565b8063059ff004146100b95780630c02a57c146100d7578063468d5ac2146100f357806376d695a11461010f57806378127ba7146101415780637af7d3aa14610175575b600080fd5b6100c1610283565b6040516100ce9190611743565b60405180910390f35b6100f160048036038101906100ec9190611401565b610352565b005b61010d6004803603810190610108919061139e565b6106a9565b005b61012960048036038101906101249190611375565b6108a2565b604051610138939291906115f7565b60405180910390f35b61015b600480360381019061015691906114e0565b610979565b60405161016c959493929190611765565b60405180910390f35b61018f600480360381019061018a9190611375565b610a62565b6040516101a1969594939291906116e2565b60405180910390f35b6101c460048036038101906101bf919061149f565b610b54565b005b6101e060048036038101906101db9190611375565b610c28565b6040516101f2969594939291906116e2565b60405180910390f35b61021560048036038101906102109190611375565b610d1a565b604051610224939291906115f7565b60405180910390f35b61024760048036038101906102429190611401565b610df1565b005b610251610fe6565b60405161025e91906117df565b60405180910390f35b610281600480360381019061027c919061139e565b610fec565b005b606060026000806000600181526020019081526020016000206000015481526020019081526020016000206000018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156103455780601f1061031a57610100808354040283529160200191610345565b820191906000526020600020905b81548152906001019060200180831161032857829003601f168201915b5050505050905080905090565b600160006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16146103f7576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016103ee906117bf565b60405180910390fd5b856001600060018152602001908152602001600020600101600089815260200190815260200160002060000181905550846001600060018152602001908152602001600020600101600089815260200190815260200160002060010181905550836001600060018152602001908152602001600020600101600089815260200190815260200160002060020181905550826001600060018152602001908152602001600020600101600089815260200190815260200160002060030160006101000a81548161ffff021916908361ffff160217905550816001600060018152602001908152602001600020600101600089815260200190815260200160002060030160026101000a81548160ff0219169083151502179055508060016000600181526020019081526020016000206001016000898152602001908152602001600020600401819055507f828cbc4adbdd4bdc2ed972ba7982fc8c0f4cc8893ca5fcbff26d78afe2db7df987600160006001815260200190815260200160002060010160008a815260200190815260200160002060000154600160006001815260200190815260200160002060010160008b815260200190815260200160002060010154600160006001815260200190815260200160002060010160008c815260200190815260200160002060020154600160006001815260200190815260200160002060010160008d815260200190815260200160002060030160009054906101000a900461ffff16600160006001815260200190815260200160002060010160008e815260200190815260200160002060030160029054906101000a900460ff16600160006001815260200190815260200160002060010160008f8152602001908152602001600020600401546040516106989796959493929190611673565b60405180910390a150505050505050565b600160006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161461074e576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610745906117bf565b60405180910390fd5b8260016000600181526020019081526020016000206001016000868152602001908152602001600020600001819055508160016000600181526020019081526020016000206001016000868152602001908152602001600020600101819055508060016000600181526020019081526020016000206001016000868152602001908152602001600020600201819055507f08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be384600160006001815260200190815260200160002060010160008781526020019081526020016000206000015460016000600181526020019081526020016000206001016000888152602001908152602001600020600101546001600060018152602001908152602001600020600101600089815260200190815260200160002060020154604051610894949392919061162e565b60405180910390a150505050565b60008060006108af61119e565b600160008060006001815260200190815260200160002060000154815260200190815260200160002060010160008681526020019081526020016000206040518060c00160405290816000820154815260200160018201548152602001600282015481526020016003820160009054906101000a900461ffff1661ffff1661ffff1681526020016003820160029054906101000a900460ff161515151581526020016004820154815250509050806000015181602001518260400151935093509350509193909250565b6002602052806000526040600020600091509050806000018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610a255780601f106109fa57610100808354040283529160200191610a25565b820191906000526020600020905b815481529060010190602001808311610a0857829003601f168201915b5050505050908060010154908060020160009054906101000a900461ffff16908060020160029054906101000a900460ff16908060030154905085565b600080600080600080610a7361119e565b600160008060006001815260200190815260200160002060000154815260200190815260200160002060010160008981526020019081526020016000206040518060c00160405290816000820154815260200160018201548152602001600282015481526020016003820160009054906101000a900461ffff1661ffff1661ffff1681526020016003820160029054906101000a900460ff161515151581526020016004820154815250509050806000015181602001518260400151836060015184608001518560a001519650965096509650965096505091939550919395565b600160006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610bf9576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610bf0906117bf565b60405180910390fd5b8060026000600181526020019081526020016000206000019080519060200190610c249291906111e3565b5050565b600080600080600080610c39611263565b600160008060006001815260200190815260200160002060000154815260200190815260200160002060020160008981526020019081526020016000206040518060c00160405290816000820154815260200160018201548152602001600282015481526020016003820160009054906101000a900461ffff1661ffff1661ffff1681526020016003820160029054906101000a900460ff161515151581526020016004820154815250509050806000015181602001518260400151836060015184608001518560a001519650965096509650965096505091939550919395565b6000806000610d27611263565b600160008060006001815260200190815260200160002060000154815260200190815260200160002060020160008681526020019081526020016000206040518060c00160405290816000820154815260200160018201548152602001600282015481526020016003820160009054906101000a900461ffff1661ffff1661ffff1681526020016003820160029054906101000a900460ff161515151581526020016004820154815250509050806000015181602001518260400151935093509350509193909250565b600160006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610e96576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610e8d906117bf565b60405180910390fd5b610e9e611263565b86816000018181525050858160200181815250508481604001818152505083816060019061ffff16908161ffff168152505082816080019015159081151581525050818160a001818152505080600160006001815260200190815260200160002060020160008a815260200190815260200160002060008201518160000155602082015181600101556040820151816002015560608201518160030160006101000a81548161ffff021916908361ffff16021790555060808201518160030160026101000a81548160ff02191690831515021790555060a082015181600401559050507f13a9d508c1d3ff570c3b81e3f41cf96794486343bf7ef78091df2531d949096a88826000015183602001518460400151856060015186608001518760a00151604051610fd49796959493929190611673565b60405180910390a15050505050505050565b60065481565b600160006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614611091576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401611088906117bf565b60405180910390fd5b611099611263565b838160000181815250508281602001818152505081816040018181525050806001600060018152602001908152602001600020600201600087815260200190815260200160002060008201518160000155602082015181600101556040820151816002015560608201518160030160006101000a81548161ffff021916908361ffff16021790555060808201518160030160026101000a81548160ff02191690831515021790555060a082015181600401559050507f4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd8582600001518360200151846040015160405161118f949392919061162e565b60405180910390a15050505050565b6040518060c00160405280600080191681526020016000801916815260200160008019168152602001600061ffff168152602001600015158152602001600081525090565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061122457805160ff1916838001178555611252565b82800160010185558215611252579182015b82811115611251578251825591602001919060010190611236565b5b50905061125f91906112a8565b5090565b6040518060c00160405280600080191681526020016000801916815260200160008019168152602001600061ffff168152602001600015158152602001600081525090565b6112ca91905b808211156112c65760008160009055506001016112ae565b5090565b90565b6000813590506112dc816118fb565b92915050565b6000813590506112f181611912565b92915050565b600082601f83011261130857600080fd5b813561131b61131682611827565b6117fa565b9150808252602083016020830185838301111561133757600080fd5b6113428382846118a8565b50505092915050565b60008135905061135a81611929565b92915050565b60008135905061136f81611940565b92915050565b60006020828403121561138757600080fd5b6000611395848285016112e2565b91505092915050565b600080600080608085870312156113b457600080fd5b60006113c2878288016112e2565b94505060206113d3878288016112e2565b93505060406113e4878288016112e2565b92505060606113f5878288016112e2565b91505092959194509250565b600080600080600080600060e0888a03121561141c57600080fd5b600061142a8a828b016112e2565b975050602061143b8a828b016112e2565b965050604061144c8a828b016112e2565b955050606061145d8a828b016112e2565b945050608061146e8a828b0161134b565b93505060a061147f8a828b016112cd565b92505060c06114908a828b01611360565b91505092959891949750929550565b6000602082840312156114b157600080fd5b600082013567ffffffffffffffff8111156114cb57600080fd5b6114d7848285016112f7565b91505092915050565b6000602082840312156114f257600080fd5b600061150084828501611360565b91505092915050565b6115128161187a565b82525050565b61152181611886565b82525050565b60006115328261185e565b61153c8185611869565b935061154c8185602086016118b7565b611555816118ea565b840191505092915050565b600061156b82611853565b6115758185611869565b93506115858185602086016118b7565b61158e816118ea565b840191505092915050565b60006115a6601c83611869565b91507f4f6e6c79206f776e65722063616e20657865632066756e6374696f6e000000006000830152602082019050919050565b6115e281611890565b82525050565b6115f18161189e565b82525050565b600060608201905061160c6000830186611518565b6116196020830185611518565b6116266040830184611518565b949350505050565b60006080820190506116436000830187611518565b6116506020830186611518565b61165d6040830185611518565b61166a6060830184611518565b95945050505050565b600060e082019050611688600083018a611518565b6116956020830189611518565b6116a26040830188611518565b6116af6060830187611518565b6116bc60808301866115d9565b6116c960a0830185611509565b6116d660c08301846115e8565b98975050505050505050565b600060c0820190506116f76000830189611518565b6117046020830188611518565b6117116040830187611518565b61171e60608301866115d9565b61172b6080830185611509565b61173860a08301846115e8565b979650505050505050565b6000602082019050818103600083015261175d8184611527565b905092915050565b600060a082019050818103600083015261177f8188611560565b905061178e60208301876115e8565b61179b60408301866115d9565b6117a86060830185611509565b6117b560808301846115e8565b9695505050505050565b600060208201905081810360008301526117d881611599565b9050919050565b60006020820190506117f460008301846115e8565b92915050565b6000604051905081810181811067ffffffffffffffff8211171561181d57600080fd5b8060405250919050565b600067ffffffffffffffff82111561183e57600080fd5b601f19601f8301169050602081019050919050565b600081519050919050565b600081519050919050565b600082825260208201905092915050565b60008115159050919050565b6000819050919050565b600061ffff82169050919050565b6000819050919050565b82818337600083830152505050565b60005b838110156118d55780820151818401526020810190506118ba565b838111156118e4576000848401525b50505050565b6000601f19601f8301169050919050565b6119048161187a565b811461190f57600080fd5b50565b61191b81611886565b811461192657600080fd5b50565b61193281611890565b811461193d57600080fd5b50565b6119498161189e565b811461195457600080fd5b5056fea2646970667358221220ad1fecf76ca03a2607ca33d261a4292cdaf2b8f3d727073c8d811b96a4b323b464736f6c63430006040033",
}

// AncestorModABI is the input ABI used to generate the binding from.
// Deprecated: Use AncestorModMetaData.ABI instead.
var AncestorModABI = AncestorModMetaData.ABI

// AncestorModBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use AncestorModMetaData.Bin instead.
var AncestorModBin = AncestorModMetaData.Bin

// DeployAncestorMod deploys a new Ethereum contract, binding an instance of AncestorMod to it.
func DeployAncestorMod(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *AncestorMod, error) {
	parsed, err := AncestorModMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(AncestorModBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &AncestorMod{AncestorModCaller: AncestorModCaller{contract: contract}, AncestorModTransactor: AncestorModTransactor{contract: contract}, AncestorModFilterer: AncestorModFilterer{contract: contract}}, nil
}

// DeployAncestorModSync deploys a new Ethereum contract and waits for receipt, binding an instance of AncestorModSession to it.
func DeployAncestorModSync(session *bind.TransactSession, backend bind.ContractBackend) (*types.Transaction, *types.Receipt, *AncestorModSession, error) {
	parsed, err := abi.JSON(strings.NewReader(AncestorModABI))
	if err != nil {
		return nil, nil, nil, err
	}
	session.Lock()
	address, tx, _, err := bind.DeployContract(session.TransactOpts, parsed, common.FromHex(AncestorModBin), backend)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	receipt, err := session.WaitTransaction(tx)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	session.TransactOpts.Nonce.Add(session.TransactOpts.Nonce, big.NewInt(1))
	session.Unlock()
	contractSession, err := NewAncestorModSession(address, backend, session)
	return tx, receipt, contractSession, err
}

// AncestorMod is an auto generated Go binding around an Ethereum contract.
type AncestorMod struct {
	AncestorModCaller     // Read-only binding to the contract
	AncestorModTransactor // Write-only binding to the contract
	AncestorModFilterer   // Log filterer for contract events
}

// AncestorModCaller is an auto generated read-only Go binding around an Ethereum contract.
type AncestorModCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AncestorModTransactor is an auto generated write-only Go binding around an Ethereum contract.
type AncestorModTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AncestorModFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type AncestorModFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AncestorModSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type AncestorModSession struct {
	Contract           *AncestorMod // Generic contract binding to set the session for
	transactionSession *bind.TransactSession
	Address            common.Address
}

// AncestorModCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type AncestorModCallerSession struct {
	Contract *AncestorModCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts      // Call options to use throughout this session
}

// AncestorModTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type AncestorModTransactorSession struct {
	Contract     *AncestorModTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts      // Transaction auth options to use throughout this session
}

// AncestorModRaw is an auto generated low-level Go binding around an Ethereum contract.
type AncestorModRaw struct {
	Contract *AncestorMod // Generic contract binding to access the raw methods on
}

// AncestorModCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type AncestorModCallerRaw struct {
	Contract *AncestorModCaller // Generic read-only contract binding to access the raw methods on
}

// AncestorModTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type AncestorModTransactorRaw struct {
	Contract *AncestorModTransactor // Generic write-only contract binding to access the raw methods on
}

// NewAncestorMod creates a new instance of AncestorMod, bound to a specific deployed contract.
func NewAncestorMod(address common.Address, backend bind.ContractBackend) (*AncestorMod, error) {
	contract, err := bindAncestorMod(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &AncestorMod{AncestorModCaller: AncestorModCaller{contract: contract}, AncestorModTransactor: AncestorModTransactor{contract: contract}, AncestorModFilterer: AncestorModFilterer{contract: contract}}, nil
}

// NewAncestorModCaller creates a new read-only instance of AncestorMod, bound to a specific deployed contract.
func NewAncestorModCaller(address common.Address, caller bind.ContractCaller) (*AncestorModCaller, error) {
	contract, err := bindAncestorMod(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &AncestorModCaller{contract: contract}, nil
}

// NewAncestorModTransactor creates a new write-only instance of AncestorMod, bound to a specific deployed contract.
func NewAncestorModTransactor(address common.Address, transactor bind.ContractTransactor) (*AncestorModTransactor, error) {
	contract, err := bindAncestorMod(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &AncestorModTransactor{contract: contract}, nil
}

// NewAncestorModFilterer creates a new log filterer instance of AncestorMod, bound to a specific deployed contract.
func NewAncestorModFilterer(address common.Address, filterer bind.ContractFilterer) (*AncestorModFilterer, error) {
	contract, err := bindAncestorMod(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &AncestorModFilterer{contract: contract}, nil
}

func NewAncestorModSession(address common.Address, backend bind.ContractBackend, transactionSession *bind.TransactSession) (*AncestorModSession, error) {
	AncestorModInstance, err := NewAncestorMod(address, backend)
	if err != nil {
		return nil, err
	}
	return &AncestorModSession{
		Contract:           AncestorModInstance,
		transactionSession: transactionSession,
		Address:            address,
	}, nil
}

// bindAncestorMod binds a generic wrapper to an already deployed contract.
func bindAncestorMod(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := AncestorModMetaData.GetAbi()
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, *parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_AncestorMod *AncestorModRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _AncestorMod.Contract.AncestorModCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_AncestorMod *AncestorModRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _AncestorMod.Contract.AncestorModTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_AncestorMod *AncestorModRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _AncestorMod.Contract.AncestorModTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_AncestorMod *AncestorModCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _AncestorMod.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_AncestorMod *AncestorModTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _AncestorMod.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_AncestorMod *AncestorModTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _AncestorMod.Contract.contract.Transact(opts, method, params...)
}

// AncestorPublicData is a free data retrieval call binding the contract method 0x78127ba7.
//
// Solidity: function ancestorPublicData(uint256 ) view returns(string version, uint256 i, uint16 suffixTiny, bool suffixBool, uint256 suffixInt)
func (_AncestorMod *AncestorModCaller) AncestorPublicData(opts *bind.CallOpts, arg0 *big.Int) (struct {
	Version    string
	I          *big.Int
	SuffixTiny uint16
	SuffixBool bool
	SuffixInt  *big.Int
}, error) {
	var out []interface{}
	err := _AncestorMod.contract.Call(opts, &out, "ancestorPublicData", arg0)

	outstruct := new(struct {
		Version    string
		I          *big.Int
		SuffixTiny uint16
		SuffixBool bool
		SuffixInt  *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Version = *abi.ConvertType(out[0], new(string)).(*string)
	outstruct.I = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)
	outstruct.SuffixTiny = *abi.ConvertType(out[2], new(uint16)).(*uint16)
	outstruct.SuffixBool = *abi.ConvertType(out[3], new(bool)).(*bool)
	outstruct.SuffixInt = *abi.ConvertType(out[4], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// AncestorPublicData is a free data retrieval call binding the contract method 0x78127ba7.
//
// Solidity: function ancestorPublicData(uint256 ) view returns(string version, uint256 i, uint16 suffixTiny, bool suffixBool, uint256 suffixInt)
func (_AncestorMod *AncestorModSession) AncestorPublicData(arg0 *big.Int) (struct {
	Version    string
	I          *big.Int
	SuffixTiny uint16
	SuffixBool bool
	SuffixInt  *big.Int
}, error) {
	return _AncestorMod.Contract.AncestorPublicData(_AncestorMod.transactionSession.CallOpts, arg0)
}

// AncestorPublicData is a free data retrieval call binding the contract method 0x78127ba7.
//
// Solidity: function ancestorPublicData(uint256 ) view returns(string version, uint256 i, uint16 suffixTiny, bool suffixBool, uint256 suffixInt)
func (_AncestorMod *AncestorModCallerSession) AncestorPublicData(arg0 *big.Int) (struct {
	Version    string
	I          *big.Int
	SuffixTiny uint16
	SuffixBool bool
	SuffixInt  *big.Int
}, error) {
	return _AncestorMod.Contract.AncestorPublicData(&_AncestorMod.CallOpts, arg0)
}

// F1 is a free data retrieval call binding the contract method 0xc27fc305.
//
// Solidity: function f1() view returns(uint256)
func (_AncestorMod *AncestorModCaller) F1(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _AncestorMod.contract.Call(opts, &out, "f1")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// F1 is a free data retrieval call binding the contract method 0xc27fc305.
//
// Solidity: function f1() view returns(uint256)
func (_AncestorMod *AncestorModSession) F1() (*big.Int, error) {
	return _AncestorMod.Contract.F1(_AncestorMod.transactionSession.CallOpts)
}

// F1 is a free data retrieval call binding the contract method 0xc27fc305.
//
// Solidity: function f1() view returns(uint256)
func (_AncestorMod *AncestorModCallerSession) F1() (*big.Int, error) {
	return _AncestorMod.Contract.F1(&_AncestorMod.CallOpts)
}

// GetAncestorItem is a free data retrieval call binding the contract method 0x76d695a1.
//
// Solidity: function getAncestorItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_AncestorMod *AncestorModCaller) GetAncestorItem(opts *bind.CallOpts, key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	var out []interface{}
	err := _AncestorMod.contract.Call(opts, &out, "getAncestorItem", key)

	if err != nil {
		return *new([32]byte), *new([32]byte), *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)
	out1 := *abi.ConvertType(out[1], new([32]byte)).(*[32]byte)
	out2 := *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)

	return out0, out1, out2, err

}

// GetAncestorItem is a free data retrieval call binding the contract method 0x76d695a1.
//
// Solidity: function getAncestorItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_AncestorMod *AncestorModSession) GetAncestorItem(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _AncestorMod.Contract.GetAncestorItem(_AncestorMod.transactionSession.CallOpts, key)
}

// GetAncestorItem is a free data retrieval call binding the contract method 0x76d695a1.
//
// Solidity: function getAncestorItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_AncestorMod *AncestorModCallerSession) GetAncestorItem(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _AncestorMod.Contract.GetAncestorItem(&_AncestorMod.CallOpts, key)
}

// GetAncestorItem2 is a free data retrieval call binding the contract method 0xa911ed94.
//
// Solidity: function getAncestorItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_AncestorMod *AncestorModCaller) GetAncestorItem2(opts *bind.CallOpts, key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	var out []interface{}
	err := _AncestorMod.contract.Call(opts, &out, "getAncestorItem2", key)

	if err != nil {
		return *new([32]byte), *new([32]byte), *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)
	out1 := *abi.ConvertType(out[1], new([32]byte)).(*[32]byte)
	out2 := *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)

	return out0, out1, out2, err

}

// GetAncestorItem2 is a free data retrieval call binding the contract method 0xa911ed94.
//
// Solidity: function getAncestorItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_AncestorMod *AncestorModSession) GetAncestorItem2(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _AncestorMod.Contract.GetAncestorItem2(_AncestorMod.transactionSession.CallOpts, key)
}

// GetAncestorItem2 is a free data retrieval call binding the contract method 0xa911ed94.
//
// Solidity: function getAncestorItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_AncestorMod *AncestorModCallerSession) GetAncestorItem2(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _AncestorMod.Contract.GetAncestorItem2(&_AncestorMod.CallOpts, key)
}

// GetAncestorModItem is a free data retrieval call binding the contract method 0x7af7d3aa.
//
// Solidity: function getAncestorModItem(bytes32 key) view returns(bytes32, bytes32, bytes32, uint16, bool, uint256)
func (_AncestorMod *AncestorModCaller) GetAncestorModItem(opts *bind.CallOpts, key [32]byte) ([32]byte, [32]byte, [32]byte, uint16, bool, *big.Int, error) {
	var out []interface{}
	err := _AncestorMod.contract.Call(opts, &out, "getAncestorModItem", key)

	if err != nil {
		return *new([32]byte), *new([32]byte), *new([32]byte), *new(uint16), *new(bool), *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)
	out1 := *abi.ConvertType(out[1], new([32]byte)).(*[32]byte)
	out2 := *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)
	out3 := *abi.ConvertType(out[3], new(uint16)).(*uint16)
	out4 := *abi.ConvertType(out[4], new(bool)).(*bool)
	out5 := *abi.ConvertType(out[5], new(*big.Int)).(**big.Int)

	return out0, out1, out2, out3, out4, out5, err

}

// GetAncestorModItem is a free data retrieval call binding the contract method 0x7af7d3aa.
//
// Solidity: function getAncestorModItem(bytes32 key) view returns(bytes32, bytes32, bytes32, uint16, bool, uint256)
func (_AncestorMod *AncestorModSession) GetAncestorModItem(key [32]byte) ([32]byte, [32]byte, [32]byte, uint16, bool, *big.Int, error) {
	return _AncestorMod.Contract.GetAncestorModItem(_AncestorMod.transactionSession.CallOpts, key)
}

// GetAncestorModItem is a free data retrieval call binding the contract method 0x7af7d3aa.
//
// Solidity: function getAncestorModItem(bytes32 key) view returns(bytes32, bytes32, bytes32, uint16, bool, uint256)
func (_AncestorMod *AncestorModCallerSession) GetAncestorModItem(key [32]byte) ([32]byte, [32]byte, [32]byte, uint16, bool, *big.Int, error) {
	return _AncestorMod.Contract.GetAncestorModItem(&_AncestorMod.CallOpts, key)
}

// GetAncestorModItem2 is a free data retrieval call binding the contract method 0x9e7c1bf2.
//
// Solidity: function getAncestorModItem2(bytes32 key) view returns(bytes32, bytes32, bytes32, uint16, bool, uint256)
func (_AncestorMod *AncestorModCaller) GetAncestorModItem2(opts *bind.CallOpts, key [32]byte) ([32]byte, [32]byte, [32]byte, uint16, bool, *big.Int, error) {
	var out []interface{}
	err := _AncestorMod.contract.Call(opts, &out, "getAncestorModItem2", key)

	if err != nil {
		return *new([32]byte), *new([32]byte), *new([32]byte), *new(uint16), *new(bool), *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)
	out1 := *abi.ConvertType(out[1], new([32]byte)).(*[32]byte)
	out2 := *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)
	out3 := *abi.ConvertType(out[3], new(uint16)).(*uint16)
	out4 := *abi.ConvertType(out[4], new(bool)).(*bool)
	out5 := *abi.ConvertType(out[5], new(*big.Int)).(**big.Int)

	return out0, out1, out2, out3, out4, out5, err

}

// GetAncestorModItem2 is a free data retrieval call binding the contract method 0x9e7c1bf2.
//
// Solidity: function getAncestorModItem2(bytes32 key) view returns(bytes32, bytes32, bytes32, uint16, bool, uint256)
func (_AncestorMod *AncestorModSession) GetAncestorModItem2(key [32]byte) ([32]byte, [32]byte, [32]byte, uint16, bool, *big.Int, error) {
	return _AncestorMod.Contract.GetAncestorModItem2(_AncestorMod.transactionSession.CallOpts, key)
}

// GetAncestorModItem2 is a free data retrieval call binding the contract method 0x9e7c1bf2.
//
// Solidity: function getAncestorModItem2(bytes32 key) view returns(bytes32, bytes32, bytes32, uint16, bool, uint256)
func (_AncestorMod *AncestorModCallerSession) GetAncestorModItem2(key [32]byte) ([32]byte, [32]byte, [32]byte, uint16, bool, *big.Int, error) {
	return _AncestorMod.Contract.GetAncestorModItem2(&_AncestorMod.CallOpts, key)
}

// GetAncestorVersion is a free data retrieval call binding the contract method 0x059ff004.
//
// Solidity: function getAncestorVersion() view returns(string ret)
func (_AncestorMod *AncestorModCaller) GetAncestorVersion(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _AncestorMod.contract.Call(opts, &out, "getAncestorVersion")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// GetAncestorVersion is a free data retrieval call binding the contract method 0x059ff004.
//
// Solidity: function getAncestorVersion() view returns(string ret)
func (_AncestorMod *AncestorModSession) GetAncestorVersion() (string, error) {
	return _AncestorMod.Contract.GetAncestorVersion(_AncestorMod.transactionSession.CallOpts)
}

// GetAncestorVersion is a free data retrieval call binding the contract method 0x059ff004.
//
// Solidity: function getAncestorVersion() view returns(string ret)
func (_AncestorMod *AncestorModCallerSession) GetAncestorVersion() (string, error) {
	return _AncestorMod.Contract.GetAncestorVersion(&_AncestorMod.CallOpts)
}

// SetAncestorItem is a paid mutator transaction binding the contract method 0x468d5ac2.
//
// Solidity: function setAncestorItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_AncestorMod *AncestorModTransactor) SetAncestorItem(opts *bind.TransactOpts, key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _AncestorMod.contract.Transact(opts, "setAncestorItem", key, value, dataType, dataDescr)
}

// SetAncestorItem is a paid mutator transaction binding the contract method 0x468d5ac2.
//
// Solidity: function setAncestorItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_AncestorMod *AncestorModSession) SetAncestorItem(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, *types.Receipt, error) {
	_AncestorMod.transactionSession.Lock()
	tx, err := _AncestorMod.Contract.SetAncestorItem(_AncestorMod.transactionSession.TransactOpts, key, value, dataType, dataDescr)
	if err != nil {
		_AncestorMod.transactionSession.Unlock()
		return nil, nil, err
	}
	_AncestorMod.transactionSession.TransactOpts.Nonce.Add(_AncestorMod.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_AncestorMod.transactionSession.Unlock()
	receipt, err := _AncestorMod.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetAncestorItem is a paid mutator transaction binding the contract method 0x468d5ac2.
//
// Solidity: function setAncestorItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_AncestorMod *AncestorModTransactorSession) SetAncestorItem(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _AncestorMod.Contract.SetAncestorItem(&_AncestorMod.TransactOpts, key, value, dataType, dataDescr)
}

// SetAncestorItem2 is a paid mutator transaction binding the contract method 0xdc9a052b.
//
// Solidity: function setAncestorItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_AncestorMod *AncestorModTransactor) SetAncestorItem2(opts *bind.TransactOpts, key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _AncestorMod.contract.Transact(opts, "setAncestorItem2", key, value, dataType, dataDescr)
}

// SetAncestorItem2 is a paid mutator transaction binding the contract method 0xdc9a052b.
//
// Solidity: function setAncestorItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_AncestorMod *AncestorModSession) SetAncestorItem2(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, *types.Receipt, error) {
	_AncestorMod.transactionSession.Lock()
	tx, err := _AncestorMod.Contract.SetAncestorItem2(_AncestorMod.transactionSession.TransactOpts, key, value, dataType, dataDescr)
	if err != nil {
		_AncestorMod.transactionSession.Unlock()
		return nil, nil, err
	}
	_AncestorMod.transactionSession.TransactOpts.Nonce.Add(_AncestorMod.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_AncestorMod.transactionSession.Unlock()
	receipt, err := _AncestorMod.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetAncestorItem2 is a paid mutator transaction binding the contract method 0xdc9a052b.
//
// Solidity: function setAncestorItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_AncestorMod *AncestorModTransactorSession) SetAncestorItem2(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _AncestorMod.Contract.SetAncestorItem2(&_AncestorMod.TransactOpts, key, value, dataType, dataDescr)
}

// SetAncestorModItem is a paid mutator transaction binding the contract method 0x0c02a57c.
//
// Solidity: function setAncestorModItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr, uint16 sfTiny, bool sfBool, uint256 sfInt) returns()
func (_AncestorMod *AncestorModTransactor) SetAncestorModItem(opts *bind.TransactOpts, key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte, sfTiny uint16, sfBool bool, sfInt *big.Int) (*types.Transaction, error) {
	return _AncestorMod.contract.Transact(opts, "setAncestorModItem", key, value, dataType, dataDescr, sfTiny, sfBool, sfInt)
}

// SetAncestorModItem is a paid mutator transaction binding the contract method 0x0c02a57c.
//
// Solidity: function setAncestorModItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr, uint16 sfTiny, bool sfBool, uint256 sfInt) returns()
func (_AncestorMod *AncestorModSession) SetAncestorModItem(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte, sfTiny uint16, sfBool bool, sfInt *big.Int) (*types.Transaction, *types.Receipt, error) {
	_AncestorMod.transactionSession.Lock()
	tx, err := _AncestorMod.Contract.SetAncestorModItem(_AncestorMod.transactionSession.TransactOpts, key, value, dataType, dataDescr, sfTiny, sfBool, sfInt)
	if err != nil {
		_AncestorMod.transactionSession.Unlock()
		return nil, nil, err
	}
	_AncestorMod.transactionSession.TransactOpts.Nonce.Add(_AncestorMod.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_AncestorMod.transactionSession.Unlock()
	receipt, err := _AncestorMod.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetAncestorModItem is a paid mutator transaction binding the contract method 0x0c02a57c.
//
// Solidity: function setAncestorModItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr, uint16 sfTiny, bool sfBool, uint256 sfInt) returns()
func (_AncestorMod *AncestorModTransactorSession) SetAncestorModItem(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte, sfTiny uint16, sfBool bool, sfInt *big.Int) (*types.Transaction, error) {
	return _AncestorMod.Contract.SetAncestorModItem(&_AncestorMod.TransactOpts, key, value, dataType, dataDescr, sfTiny, sfBool, sfInt)
}

// SetAncestorModItem2 is a paid mutator transaction binding the contract method 0xb9e9abc3.
//
// Solidity: function setAncestorModItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr, uint16 sfTiny, bool sfBool, uint256 sfInt) returns()
func (_AncestorMod *AncestorModTransactor) SetAncestorModItem2(opts *bind.TransactOpts, key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte, sfTiny uint16, sfBool bool, sfInt *big.Int) (*types.Transaction, error) {
	return _AncestorMod.contract.Transact(opts, "setAncestorModItem2", key, value, dataType, dataDescr, sfTiny, sfBool, sfInt)
}

// SetAncestorModItem2 is a paid mutator transaction binding the contract method 0xb9e9abc3.
//
// Solidity: function setAncestorModItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr, uint16 sfTiny, bool sfBool, uint256 sfInt) returns()
func (_AncestorMod *AncestorModSession) SetAncestorModItem2(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte, sfTiny uint16, sfBool bool, sfInt *big.Int) (*types.Transaction, *types.Receipt, error) {
	_AncestorMod.transactionSession.Lock()
	tx, err := _AncestorMod.Contract.SetAncestorModItem2(_AncestorMod.transactionSession.TransactOpts, key, value, dataType, dataDescr, sfTiny, sfBool, sfInt)
	if err != nil {
		_AncestorMod.transactionSession.Unlock()
		return nil, nil, err
	}
	_AncestorMod.transactionSession.TransactOpts.Nonce.Add(_AncestorMod.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_AncestorMod.transactionSession.Unlock()
	receipt, err := _AncestorMod.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetAncestorModItem2 is a paid mutator transaction binding the contract method 0xb9e9abc3.
//
// Solidity: function setAncestorModItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr, uint16 sfTiny, bool sfBool, uint256 sfInt) returns()
func (_AncestorMod *AncestorModTransactorSession) SetAncestorModItem2(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte, sfTiny uint16, sfBool bool, sfInt *big.Int) (*types.Transaction, error) {
	return _AncestorMod.Contract.SetAncestorModItem2(&_AncestorMod.TransactOpts, key, value, dataType, dataDescr, sfTiny, sfBool, sfInt)
}

// SetAncestorVersion is a paid mutator transaction binding the contract method 0x95522e7e.
//
// Solidity: function setAncestorVersion(string value) returns()
func (_AncestorMod *AncestorModTransactor) SetAncestorVersion(opts *bind.TransactOpts, value string) (*types.Transaction, error) {
	return _AncestorMod.contract.Transact(opts, "setAncestorVersion", value)
}

// SetAncestorVersion is a paid mutator transaction binding the contract method 0x95522e7e.
//
// Solidity: function setAncestorVersion(string value) returns()
func (_AncestorMod *AncestorModSession) SetAncestorVersion(value string) (*types.Transaction, *types.Receipt, error) {
	_AncestorMod.transactionSession.Lock()
	tx, err := _AncestorMod.Contract.SetAncestorVersion(_AncestorMod.transactionSession.TransactOpts, value)
	if err != nil {
		_AncestorMod.transactionSession.Unlock()
		return nil, nil, err
	}
	_AncestorMod.transactionSession.TransactOpts.Nonce.Add(_AncestorMod.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_AncestorMod.transactionSession.Unlock()
	receipt, err := _AncestorMod.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetAncestorVersion is a paid mutator transaction binding the contract method 0x95522e7e.
//
// Solidity: function setAncestorVersion(string value) returns()
func (_AncestorMod *AncestorModTransactorSession) SetAncestorVersion(value string) (*types.Transaction, error) {
	return _AncestorMod.Contract.SetAncestorVersion(&_AncestorMod.TransactOpts, value)
}

// AncestorModAncItem2SetIterator is returned from FilterAncItem2Set and is used to iterate over the raw logs and unpacked data for AncItem2Set events raised by the AncestorMod contract.
type AncestorModAncItem2SetIterator struct {
	Event *AncestorModAncItem2Set // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *AncestorModAncItem2SetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(AncestorModAncItem2Set)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(AncestorModAncItem2Set)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *AncestorModAncItem2SetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *AncestorModAncItem2SetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// AncestorModAncItem2Set represents a AncItem2Set event raised by the AncestorMod contract.
type AncestorModAncItem2Set struct {
	Key       [32]byte
	Value     [32]byte
	DataType  [32]byte
	DataDescr [32]byte
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterAncItem2Set is a free log retrieval operation binding the contract event 0x4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd.
//
// Solidity: event AncItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_AncestorMod *AncestorModFilterer) FilterAncItem2Set(opts *bind.FilterOpts) (*AncestorModAncItem2SetIterator, error) {

	logs, sub, err := _AncestorMod.contract.FilterLogs(opts, "AncItem2Set")
	if err != nil {
		return nil, err
	}
	return &AncestorModAncItem2SetIterator{contract: _AncestorMod.contract, event: "AncItem2Set", logs: logs, sub: sub}, nil
}

// WatchAncItem2Set is a free log subscription operation binding the contract event 0x4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd.
//
// Solidity: event AncItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_AncestorMod *AncestorModFilterer) WatchAncItem2Set(opts *bind.WatchOpts, sink chan<- *AncestorModAncItem2Set) (event.Subscription, error) {

	logs, sub, err := _AncestorMod.contract.WatchLogs(opts, "AncItem2Set")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(AncestorModAncItem2Set)
				if err := _AncestorMod.contract.UnpackLog(event, "AncItem2Set", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAncItem2Set is a log parse operation binding the contract event 0x4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd.
//
// Solidity: event AncItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_AncestorMod *AncestorModFilterer) ParseAncItem2Set(log types.Log) (*AncestorModAncItem2Set, error) {
	event := new(AncestorModAncItem2Set)
	if err := _AncestorMod.contract.UnpackLog(event, "AncItem2Set", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// AncestorModAncItemSetIterator is returned from FilterAncItemSet and is used to iterate over the raw logs and unpacked data for AncItemSet events raised by the AncestorMod contract.
type AncestorModAncItemSetIterator struct {
	Event *AncestorModAncItemSet // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *AncestorModAncItemSetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(AncestorModAncItemSet)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(AncestorModAncItemSet)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *AncestorModAncItemSetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *AncestorModAncItemSetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// AncestorModAncItemSet represents a AncItemSet event raised by the AncestorMod contract.
type AncestorModAncItemSet struct {
	Key       [32]byte
	Value     [32]byte
	DataType  [32]byte
	DataDescr [32]byte
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterAncItemSet is a free log retrieval operation binding the contract event 0x08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be3.
//
// Solidity: event AncItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_AncestorMod *AncestorModFilterer) FilterAncItemSet(opts *bind.FilterOpts) (*AncestorModAncItemSetIterator, error) {

	logs, sub, err := _AncestorMod.contract.FilterLogs(opts, "AncItemSet")
	if err != nil {
		return nil, err
	}
	return &AncestorModAncItemSetIterator{contract: _AncestorMod.contract, event: "AncItemSet", logs: logs, sub: sub}, nil
}

// WatchAncItemSet is a free log subscription operation binding the contract event 0x08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be3.
//
// Solidity: event AncItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_AncestorMod *AncestorModFilterer) WatchAncItemSet(opts *bind.WatchOpts, sink chan<- *AncestorModAncItemSet) (event.Subscription, error) {

	logs, sub, err := _AncestorMod.contract.WatchLogs(opts, "AncItemSet")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(AncestorModAncItemSet)
				if err := _AncestorMod.contract.UnpackLog(event, "AncItemSet", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAncItemSet is a log parse operation binding the contract event 0x08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be3.
//
// Solidity: event AncItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_AncestorMod *AncestorModFilterer) ParseAncItemSet(log types.Log) (*AncestorModAncItemSet, error) {
	event := new(AncestorModAncItemSet)
	if err := _AncestorMod.contract.UnpackLog(event, "AncItemSet", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// AncestorModAncModItem2SetIterator is returned from FilterAncModItem2Set and is used to iterate over the raw logs and unpacked data for AncModItem2Set events raised by the AncestorMod contract.
type AncestorModAncModItem2SetIterator struct {
	Event *AncestorModAncModItem2Set // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *AncestorModAncModItem2SetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(AncestorModAncModItem2Set)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(AncestorModAncModItem2Set)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *AncestorModAncModItem2SetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *AncestorModAncModItem2SetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// AncestorModAncModItem2Set represents a AncModItem2Set event raised by the AncestorMod contract.
type AncestorModAncModItem2Set struct {
	Key       [32]byte
	Value     [32]byte
	DataType  [32]byte
	DataDescr [32]byte
	SfTiny    uint16
	SfBool    bool
	SfInt     *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterAncModItem2Set is a free log retrieval operation binding the contract event 0x13a9d508c1d3ff570c3b81e3f41cf96794486343bf7ef78091df2531d949096a.
//
// Solidity: event AncModItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr, uint16 sfTiny, bool sfBool, uint256 sfInt)
func (_AncestorMod *AncestorModFilterer) FilterAncModItem2Set(opts *bind.FilterOpts) (*AncestorModAncModItem2SetIterator, error) {

	logs, sub, err := _AncestorMod.contract.FilterLogs(opts, "AncModItem2Set")
	if err != nil {
		return nil, err
	}
	return &AncestorModAncModItem2SetIterator{contract: _AncestorMod.contract, event: "AncModItem2Set", logs: logs, sub: sub}, nil
}

// WatchAncModItem2Set is a free log subscription operation binding the contract event 0x13a9d508c1d3ff570c3b81e3f41cf96794486343bf7ef78091df2531d949096a.
//
// Solidity: event AncModItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr, uint16 sfTiny, bool sfBool, uint256 sfInt)
func (_AncestorMod *AncestorModFilterer) WatchAncModItem2Set(opts *bind.WatchOpts, sink chan<- *AncestorModAncModItem2Set) (event.Subscription, error) {

	logs, sub, err := _AncestorMod.contract.WatchLogs(opts, "AncModItem2Set")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(AncestorModAncModItem2Set)
				if err := _AncestorMod.contract.UnpackLog(event, "AncModItem2Set", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAncModItem2Set is a log parse operation binding the contract event 0x13a9d508c1d3ff570c3b81e3f41cf96794486343bf7ef78091df2531d949096a.
//
// Solidity: event AncModItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr, uint16 sfTiny, bool sfBool, uint256 sfInt)
func (_AncestorMod *AncestorModFilterer) ParseAncModItem2Set(log types.Log) (*AncestorModAncModItem2Set, error) {
	event := new(AncestorModAncModItem2Set)
	if err := _AncestorMod.contract.UnpackLog(event, "AncModItem2Set", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// AncestorModAncModItemSetIterator is returned from FilterAncModItemSet and is used to iterate over the raw logs and unpacked data for AncModItemSet events raised by the AncestorMod contract.
type AncestorModAncModItemSetIterator struct {
	Event *AncestorModAncModItemSet // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *AncestorModAncModItemSetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(AncestorModAncModItemSet)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(AncestorModAncModItemSet)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *AncestorModAncModItemSetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *AncestorModAncModItemSetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// AncestorModAncModItemSet represents a AncModItemSet event raised by the AncestorMod contract.
type AncestorModAncModItemSet struct {
	Key       [32]byte
	Value     [32]byte
	DataType  [32]byte
	DataDescr [32]byte
	SfTiny    uint16
	SfBool    bool
	SfInt     *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterAncModItemSet is a free log retrieval operation binding the contract event 0x828cbc4adbdd4bdc2ed972ba7982fc8c0f4cc8893ca5fcbff26d78afe2db7df9.
//
// Solidity: event AncModItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr, uint16 sfTiny, bool sfBool, uint256 sfInt)
func (_AncestorMod *AncestorModFilterer) FilterAncModItemSet(opts *bind.FilterOpts) (*AncestorModAncModItemSetIterator, error) {

	logs, sub, err := _AncestorMod.contract.FilterLogs(opts, "AncModItemSet")
	if err != nil {
		return nil, err
	}
	return &AncestorModAncModItemSetIterator{contract: _AncestorMod.contract, event: "AncModItemSet", logs: logs, sub: sub}, nil
}

// WatchAncModItemSet is a free log subscription operation binding the contract event 0x828cbc4adbdd4bdc2ed972ba7982fc8c0f4cc8893ca5fcbff26d78afe2db7df9.
//
// Solidity: event AncModItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr, uint16 sfTiny, bool sfBool, uint256 sfInt)
func (_AncestorMod *AncestorModFilterer) WatchAncModItemSet(opts *bind.WatchOpts, sink chan<- *AncestorModAncModItemSet) (event.Subscription, error) {

	logs, sub, err := _AncestorMod.contract.WatchLogs(opts, "AncModItemSet")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(AncestorModAncModItemSet)
				if err := _AncestorMod.contract.UnpackLog(event, "AncModItemSet", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAncModItemSet is a log parse operation binding the contract event 0x828cbc4adbdd4bdc2ed972ba7982fc8c0f4cc8893ca5fcbff26d78afe2db7df9.
//
// Solidity: event AncModItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr, uint16 sfTiny, bool sfBool, uint256 sfInt)
func (_AncestorMod *AncestorModFilterer) ParseAncModItemSet(log types.Log) (*AncestorModAncModItemSet, error) {
	event := new(AncestorModAncModItemSet)
	if err := _AncestorMod.contract.UnpackLog(event, "AncModItemSet", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
