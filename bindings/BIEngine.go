// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package bindings

import (
	"errors"
	"math/big"
	"strings"

	"gitlab.com/aklin/linkch"
	"gitlab.com/aklin/linkch/accounts/abi"
	"gitlab.com/aklin/linkch/accounts/abi/bind"
	"gitlab.com/aklin/linkch/common"
	"gitlab.com/aklin/linkch/core/types"
	"gitlab.com/aklin/linkch/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
	_ = abi.ConvertType
)

// BIEngineMetaData contains all meta data concerning the BIEngine contract.
var BIEngineMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_MCATb\",\"type\":\"address\"},{\"internalType\":\"bytes32\",\"name\":\"_implKey\",\"type\":\"bytes32\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"stateMutability\":\"nonpayable\",\"type\":\"fallback\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"_key\",\"type\":\"bytes32\"}],\"name\":\"fabriceBI\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Bin: "0x608060405234801561001057600080fd5b5060405161091238038061091283398181016040528101906100329190610149565b61003a6100ec565b82816000019073ffffffffffffffffffffffffffffffffffffffff16908173ffffffffffffffffffffffffffffffffffffffff168152505081816020018181525050806000806001815260200190815260200160002060008201518160000160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550602082015181600101559050505050506101ef565b6040518060400160405280600073ffffffffffffffffffffffffffffffffffffffff168152602001600080191681525090565b60008151905061012e816101c1565b92915050565b600081519050610143816101d8565b92915050565b6000806040838503121561015c57600080fd5b600061016a8582860161011f565b925050602061017b85828601610134565b9150509250929050565b6000610190826101a1565b9050919050565b6000819050919050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b6101ca81610185565b81146101d557600080fd5b50565b6101e181610197565b81146101ec57600080fd5b50565b610714806101fe6000396000f3fe608060405234801561001057600080fd5b506004361061002f5760003560e01c806378f1e4981461003a57610030565b5b61003861006a565b005b610054600480360381019061004f9190610437565b61007c565b6040516100619190610592565b60405180910390f35b61007a610075610213565b6103be565b565b60008060606000806001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16846040516024016100df91906105ad565b6040516020818303038152906040527f78f1e498000000000000000000000000000000000000000000000000000000007bffffffffffffffffffffffffffffffffffffffffffffffffffffffff19166020820180517bffffffffffffffffffffffffffffffffffffffffffffffffffffffff8381831617835250505050604051610169919061057b565b6000604051808303816000865af19150503d80600081146101a6576040519150601f19603f3d011682016040523d82523d6000602084013e6101ab565b606091505b5091509150816101f0576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016101e7906105c8565b60405180910390fd5b600081806020019051810190610206919061040e565b9050809350505050919050565b60008060606000806001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16600080600181526020019081526020016000206001015460405160240161028c91906105ad565b6040516020818303038152906040527f8656fb06000000000000000000000000000000000000000000000000000000007bffffffffffffffffffffffffffffffffffffffffffffffffffffffff19166020820180517bffffffffffffffffffffffffffffffffffffffffffffffffffffffff8381831617835250505050604051610316919061057b565b6000604051808303816000865af19150503d8060008114610353576040519150601f19603f3d011682016040523d82523d6000602084013e610358565b606091505b50915091508161039d576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610394906105e8565b60405180910390fd5b6000818060200190518101906103b3919061040e565b905080935050505090565b3660008037600080366000845af43d6000803e80600081146103df573d6000f35b3d6000fd5b6000815190506103f3816106b0565b92915050565b600081359050610408816106c7565b92915050565b60006020828403121561042057600080fd5b600061042e848285016103e4565b91505092915050565b60006020828403121561044957600080fd5b6000610457848285016103f9565b91505092915050565b6104698161062f565b82525050565b61047881610653565b82525050565b600061048982610608565b6104938185610613565b93506104a381856020860161067d565b80840191505092915050565b60006104bc60288361061e565b91507f4d43415420636f6e74726163742066616272696365424920657865637574696f60008301527f6e204661696c65640000000000000000000000000000000000000000000000006020830152604082019050919050565b600061052260248361061e565b91507f4d43415420636f6e7472616374206765744d4320657865637574696f6e20466160008301527f696c6564000000000000000000000000000000000000000000000000000000006020830152604082019050919050565b6000610587828461047e565b915081905092915050565b60006020820190506105a76000830184610460565b92915050565b60006020820190506105c2600083018461046f565b92915050565b600060208201905081810360008301526105e1816104af565b9050919050565b6000602082019050818103600083015261060181610515565b9050919050565b600081519050919050565b600081905092915050565b600082825260208201905092915050565b600061063a8261065d565b9050919050565b600061064c8261065d565b9050919050565b6000819050919050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b60005b8381101561069b578082015181840152602081019050610680565b838111156106aa576000848401525b50505050565b6106b981610641565b81146106c457600080fd5b50565b6106d081610653565b81146106db57600080fd5b5056fea2646970667358221220db1aacf6f6d617740f7ab040bd2c07f22544e0ab2c7cbc91ebafb101bada952064736f6c63430006040033",
}

// BIEngineABI is the input ABI used to generate the binding from.
// Deprecated: Use BIEngineMetaData.ABI instead.
var BIEngineABI = BIEngineMetaData.ABI

// BIEngineBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use BIEngineMetaData.Bin instead.
var BIEngineBin = BIEngineMetaData.Bin

// DeployBIEngine deploys a new Ethereum contract, binding an instance of BIEngine to it.
func DeployBIEngine(auth *bind.TransactOpts, backend bind.ContractBackend, _MCATb common.Address, _implKey [32]byte) (common.Address, *types.Transaction, *BIEngine, error) {
	parsed, err := BIEngineMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(BIEngineBin), backend, _MCATb, _implKey)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &BIEngine{BIEngineCaller: BIEngineCaller{contract: contract}, BIEngineTransactor: BIEngineTransactor{contract: contract}, BIEngineFilterer: BIEngineFilterer{contract: contract}}, nil
}

// DeployBIEngineSync deploys a new Ethereum contract and waits for receipt, binding an instance of BIEngineSession to it.
func DeployBIEngineSync(session *bind.TransactSession, backend bind.ContractBackend, _MCATb common.Address, _implKey [32]byte) (*types.Transaction, *types.Receipt, *BIEngineSession, error) {
	parsed, err := abi.JSON(strings.NewReader(BIEngineABI))
	if err != nil {
		return nil, nil, nil, err
	}
	session.Lock()
	address, tx, _, err := bind.DeployContract(session.TransactOpts, parsed, common.FromHex(BIEngineBin), backend, _MCATb, _implKey)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	receipt, err := session.WaitTransaction(tx)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	session.TransactOpts.Nonce.Add(session.TransactOpts.Nonce, big.NewInt(1))
	session.Unlock()
	contractSession, err := NewBIEngineSession(address, backend, session)
	return tx, receipt, contractSession, err
}

// BIEngine is an auto generated Go binding around an Ethereum contract.
type BIEngine struct {
	BIEngineCaller     // Read-only binding to the contract
	BIEngineTransactor // Write-only binding to the contract
	BIEngineFilterer   // Log filterer for contract events
}

// BIEngineCaller is an auto generated read-only Go binding around an Ethereum contract.
type BIEngineCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// BIEngineTransactor is an auto generated write-only Go binding around an Ethereum contract.
type BIEngineTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// BIEngineFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type BIEngineFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// BIEngineSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type BIEngineSession struct {
	Contract           *BIEngine // Generic contract binding to set the session for
	transactionSession *bind.TransactSession
	Address            common.Address
}

// BIEngineCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type BIEngineCallerSession struct {
	Contract *BIEngineCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// BIEngineTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type BIEngineTransactorSession struct {
	Contract     *BIEngineTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// BIEngineRaw is an auto generated low-level Go binding around an Ethereum contract.
type BIEngineRaw struct {
	Contract *BIEngine // Generic contract binding to access the raw methods on
}

// BIEngineCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type BIEngineCallerRaw struct {
	Contract *BIEngineCaller // Generic read-only contract binding to access the raw methods on
}

// BIEngineTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type BIEngineTransactorRaw struct {
	Contract *BIEngineTransactor // Generic write-only contract binding to access the raw methods on
}

// NewBIEngine creates a new instance of BIEngine, bound to a specific deployed contract.
func NewBIEngine(address common.Address, backend bind.ContractBackend) (*BIEngine, error) {
	contract, err := bindBIEngine(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &BIEngine{BIEngineCaller: BIEngineCaller{contract: contract}, BIEngineTransactor: BIEngineTransactor{contract: contract}, BIEngineFilterer: BIEngineFilterer{contract: contract}}, nil
}

// NewBIEngineCaller creates a new read-only instance of BIEngine, bound to a specific deployed contract.
func NewBIEngineCaller(address common.Address, caller bind.ContractCaller) (*BIEngineCaller, error) {
	contract, err := bindBIEngine(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &BIEngineCaller{contract: contract}, nil
}

// NewBIEngineTransactor creates a new write-only instance of BIEngine, bound to a specific deployed contract.
func NewBIEngineTransactor(address common.Address, transactor bind.ContractTransactor) (*BIEngineTransactor, error) {
	contract, err := bindBIEngine(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &BIEngineTransactor{contract: contract}, nil
}

// NewBIEngineFilterer creates a new log filterer instance of BIEngine, bound to a specific deployed contract.
func NewBIEngineFilterer(address common.Address, filterer bind.ContractFilterer) (*BIEngineFilterer, error) {
	contract, err := bindBIEngine(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &BIEngineFilterer{contract: contract}, nil
}

func NewBIEngineSession(address common.Address, backend bind.ContractBackend, transactionSession *bind.TransactSession) (*BIEngineSession, error) {
	BIEngineInstance, err := NewBIEngine(address, backend)
	if err != nil {
		return nil, err
	}
	return &BIEngineSession{
		Contract:           BIEngineInstance,
		transactionSession: transactionSession,
		Address:            address,
	}, nil
}

// bindBIEngine binds a generic wrapper to an already deployed contract.
func bindBIEngine(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := BIEngineMetaData.GetAbi()
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, *parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_BIEngine *BIEngineRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _BIEngine.Contract.BIEngineCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_BIEngine *BIEngineRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _BIEngine.Contract.BIEngineTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_BIEngine *BIEngineRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _BIEngine.Contract.BIEngineTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_BIEngine *BIEngineCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _BIEngine.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_BIEngine *BIEngineTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _BIEngine.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_BIEngine *BIEngineTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _BIEngine.Contract.contract.Transact(opts, method, params...)
}

// FabriceBI is a paid mutator transaction binding the contract method 0x78f1e498.
//
// Solidity: function fabriceBI(bytes32 _key) returns(address)
func (_BIEngine *BIEngineTransactor) FabriceBI(opts *bind.TransactOpts, _key [32]byte) (*types.Transaction, error) {
	return _BIEngine.contract.Transact(opts, "fabriceBI", _key)
}

// FabriceBI is a paid mutator transaction binding the contract method 0x78f1e498.
//
// Solidity: function fabriceBI(bytes32 _key) returns(address)
func (_BIEngine *BIEngineSession) FabriceBI(_key [32]byte) (*types.Transaction, *types.Receipt, error) {
	_BIEngine.transactionSession.Lock()
	tx, err := _BIEngine.Contract.FabriceBI(_BIEngine.transactionSession.TransactOpts, _key)
	if err != nil {
		_BIEngine.transactionSession.Unlock()
		return nil, nil, err
	}
	_BIEngine.transactionSession.TransactOpts.Nonce.Add(_BIEngine.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_BIEngine.transactionSession.Unlock()
	receipt, err := _BIEngine.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// FabriceBI is a paid mutator transaction binding the contract method 0x78f1e498.
//
// Solidity: function fabriceBI(bytes32 _key) returns(address)
func (_BIEngine *BIEngineTransactorSession) FabriceBI(_key [32]byte) (*types.Transaction, error) {
	return _BIEngine.Contract.FabriceBI(&_BIEngine.TransactOpts, _key)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() returns()
func (_BIEngine *BIEngineTransactor) Fallback(opts *bind.TransactOpts, calldata []byte) (*types.Transaction, error) {
	return _BIEngine.contract.RawTransact(opts, calldata)
}

// Fallback is a paid mutator transaction binding the contract fallback function.
//
// Solidity: fallback() returns()
func (_BIEngine *BIEngineTransactorSession) Fallback(calldata []byte) (*types.Transaction, error) {
	return _BIEngine.Contract.Fallback(&_BIEngine.TransactOpts, calldata)
}
