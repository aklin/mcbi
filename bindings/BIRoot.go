// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package bindings

import (
	"errors"
	"math/big"
	"strings"

	"gitlab.com/aklin/linkch"
	"gitlab.com/aklin/linkch/accounts/abi"
	"gitlab.com/aklin/linkch/accounts/abi/bind"
	"gitlab.com/aklin/linkch/common"
	"gitlab.com/aklin/linkch/core/types"
	"gitlab.com/aklin/linkch/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
	_ = abi.ConvertType
)

// BIRootMetaData contains all meta data concerning the BIRoot contract.
var BIRootMetaData = &bind.MetaData{
	ABI: "[]",
	Bin: "0x6080604052348015600f57600080fd5b50603f80601d6000396000f3fe6080604052600080fdfea2646970667358221220159d2e51ac8717a9012eba603009f7584c82d001c480b4d53df4dd534d7700ac64736f6c63430006040033",
}

// BIRootABI is the input ABI used to generate the binding from.
// Deprecated: Use BIRootMetaData.ABI instead.
var BIRootABI = BIRootMetaData.ABI

// BIRootBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use BIRootMetaData.Bin instead.
var BIRootBin = BIRootMetaData.Bin

// DeployBIRoot deploys a new Ethereum contract, binding an instance of BIRoot to it.
func DeployBIRoot(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *BIRoot, error) {
	parsed, err := BIRootMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(BIRootBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &BIRoot{BIRootCaller: BIRootCaller{contract: contract}, BIRootTransactor: BIRootTransactor{contract: contract}, BIRootFilterer: BIRootFilterer{contract: contract}}, nil
}

// DeployBIRootSync deploys a new Ethereum contract and waits for receipt, binding an instance of BIRootSession to it.
func DeployBIRootSync(session *bind.TransactSession, backend bind.ContractBackend) (*types.Transaction, *types.Receipt, *BIRootSession, error) {
	parsed, err := abi.JSON(strings.NewReader(BIRootABI))
	if err != nil {
		return nil, nil, nil, err
	}
	session.Lock()
	address, tx, _, err := bind.DeployContract(session.TransactOpts, parsed, common.FromHex(BIRootBin), backend)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	receipt, err := session.WaitTransaction(tx)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	session.TransactOpts.Nonce.Add(session.TransactOpts.Nonce, big.NewInt(1))
	session.Unlock()
	contractSession, err := NewBIRootSession(address, backend, session)
	return tx, receipt, contractSession, err
}

// BIRoot is an auto generated Go binding around an Ethereum contract.
type BIRoot struct {
	BIRootCaller     // Read-only binding to the contract
	BIRootTransactor // Write-only binding to the contract
	BIRootFilterer   // Log filterer for contract events
}

// BIRootCaller is an auto generated read-only Go binding around an Ethereum contract.
type BIRootCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// BIRootTransactor is an auto generated write-only Go binding around an Ethereum contract.
type BIRootTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// BIRootFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type BIRootFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// BIRootSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type BIRootSession struct {
	Contract           *BIRoot // Generic contract binding to set the session for
	transactionSession *bind.TransactSession
	Address            common.Address
}

// BIRootCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type BIRootCallerSession struct {
	Contract *BIRootCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// BIRootTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type BIRootTransactorSession struct {
	Contract     *BIRootTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// BIRootRaw is an auto generated low-level Go binding around an Ethereum contract.
type BIRootRaw struct {
	Contract *BIRoot // Generic contract binding to access the raw methods on
}

// BIRootCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type BIRootCallerRaw struct {
	Contract *BIRootCaller // Generic read-only contract binding to access the raw methods on
}

// BIRootTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type BIRootTransactorRaw struct {
	Contract *BIRootTransactor // Generic write-only contract binding to access the raw methods on
}

// NewBIRoot creates a new instance of BIRoot, bound to a specific deployed contract.
func NewBIRoot(address common.Address, backend bind.ContractBackend) (*BIRoot, error) {
	contract, err := bindBIRoot(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &BIRoot{BIRootCaller: BIRootCaller{contract: contract}, BIRootTransactor: BIRootTransactor{contract: contract}, BIRootFilterer: BIRootFilterer{contract: contract}}, nil
}

// NewBIRootCaller creates a new read-only instance of BIRoot, bound to a specific deployed contract.
func NewBIRootCaller(address common.Address, caller bind.ContractCaller) (*BIRootCaller, error) {
	contract, err := bindBIRoot(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &BIRootCaller{contract: contract}, nil
}

// NewBIRootTransactor creates a new write-only instance of BIRoot, bound to a specific deployed contract.
func NewBIRootTransactor(address common.Address, transactor bind.ContractTransactor) (*BIRootTransactor, error) {
	contract, err := bindBIRoot(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &BIRootTransactor{contract: contract}, nil
}

// NewBIRootFilterer creates a new log filterer instance of BIRoot, bound to a specific deployed contract.
func NewBIRootFilterer(address common.Address, filterer bind.ContractFilterer) (*BIRootFilterer, error) {
	contract, err := bindBIRoot(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &BIRootFilterer{contract: contract}, nil
}

func NewBIRootSession(address common.Address, backend bind.ContractBackend, transactionSession *bind.TransactSession) (*BIRootSession, error) {
	BIRootInstance, err := NewBIRoot(address, backend)
	if err != nil {
		return nil, err
	}
	return &BIRootSession{
		Contract:           BIRootInstance,
		transactionSession: transactionSession,
		Address:            address,
	}, nil
}

// bindBIRoot binds a generic wrapper to an already deployed contract.
func bindBIRoot(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := BIRootMetaData.GetAbi()
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, *parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_BIRoot *BIRootRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _BIRoot.Contract.BIRootCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_BIRoot *BIRootRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _BIRoot.Contract.BIRootTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_BIRoot *BIRootRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _BIRoot.Contract.BIRootTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_BIRoot *BIRootCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _BIRoot.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_BIRoot *BIRootTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _BIRoot.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_BIRoot *BIRootTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _BIRoot.Contract.contract.Transact(opts, method, params...)
}
