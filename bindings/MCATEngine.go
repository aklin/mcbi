// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package bindings

import (
	"errors"
	"math/big"
	"strings"

	"gitlab.com/aklin/linkch"
	"gitlab.com/aklin/linkch/accounts/abi"
	"gitlab.com/aklin/linkch/accounts/abi/bind"
	"gitlab.com/aklin/linkch/common"
	"gitlab.com/aklin/linkch/core/types"
	"gitlab.com/aklin/linkch/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
	_ = abi.ConvertType
)

// MCATEngineMetaData contains all meta data concerning the MCATEngine contract.
var MCATEngineMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"_key\",\"type\":\"bytes32\"}],\"name\":\"fabriceBI\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"_key\",\"type\":\"bytes32\"}],\"name\":\"getMC\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"_key\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"_impl\",\"type\":\"address\"}],\"name\":\"setMC\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Bin: "0x608060405234801561001057600080fd5b50336000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550610e72806100606000396000f3fe608060405234801561001057600080fd5b50600436106100415760003560e01c806378f1e4981461004657806380977753146100765780638656fb0614610092575b600080fd5b610060600480360381019061005b9190610302565b6100c2565b60405161006d919061042b565b60405180910390f35b610090600480360381019061008b919061032b565b6101a9565b005b6100ac60048036038101906100a79190610302565b61028e565b6040516100b9919061042b565b60405180910390f35b60008073ffffffffffffffffffffffffffffffffffffffff166001600084815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff161415610166576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161015d9061048f565b60405180910390fd5b60003083604051610176906102cb565b610181929190610446565b604051809103906000f08015801561019d573d6000803e3d6000fd5b50905080915050919050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610238576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161022f9061046f565b60405180910390fd5b806001600084815260200190815260200160002060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055505050565b60006001600083815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff169050919050565b6109128061052b83390190565b6000813590506102e7816104fc565b92915050565b6000813590506102fc81610513565b92915050565b60006020828403121561031457600080fd5b6000610322848285016102ed565b91505092915050565b6000806040838503121561033e57600080fd5b600061034c858286016102ed565b925050602061035d858286016102d8565b9150509250929050565b610370816104c0565b82525050565b61037f816104d2565b82525050565b60006103926025836104af565b91507f53657474696e6720696d706c656d656e746174696f6e2063616e206f6e6c792060008301527f6f776e65720000000000000000000000000000000000000000000000000000006020830152604082019050919050565b60006103f86012836104af565b91507f4d432061646472657373206973207a65726f00000000000000000000000000006000830152602082019050919050565b60006020820190506104406000830184610367565b92915050565b600060408201905061045b6000830185610367565b6104686020830184610376565b9392505050565b6000602082019050818103600083015261048881610385565b9050919050565b600060208201905081810360008301526104a8816103eb565b9050919050565b600082825260208201905092915050565b60006104cb826104dc565b9050919050565b6000819050919050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b610505816104c0565b811461051057600080fd5b50565b61051c816104d2565b811461052757600080fd5b5056fe608060405234801561001057600080fd5b5060405161091238038061091283398181016040528101906100329190610149565b61003a6100ec565b82816000019073ffffffffffffffffffffffffffffffffffffffff16908173ffffffffffffffffffffffffffffffffffffffff168152505081816020018181525050806000806001815260200190815260200160002060008201518160000160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550602082015181600101559050505050506101ef565b6040518060400160405280600073ffffffffffffffffffffffffffffffffffffffff168152602001600080191681525090565b60008151905061012e816101c1565b92915050565b600081519050610143816101d8565b92915050565b6000806040838503121561015c57600080fd5b600061016a8582860161011f565b925050602061017b85828601610134565b9150509250929050565b6000610190826101a1565b9050919050565b6000819050919050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b6101ca81610185565b81146101d557600080fd5b50565b6101e181610197565b81146101ec57600080fd5b50565b610714806101fe6000396000f3fe608060405234801561001057600080fd5b506004361061002f5760003560e01c806378f1e4981461003a57610030565b5b61003861006a565b005b610054600480360381019061004f9190610437565b61007c565b6040516100619190610592565b60405180910390f35b61007a610075610213565b6103be565b565b60008060606000806001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16846040516024016100df91906105ad565b6040516020818303038152906040527f78f1e498000000000000000000000000000000000000000000000000000000007bffffffffffffffffffffffffffffffffffffffffffffffffffffffff19166020820180517bffffffffffffffffffffffffffffffffffffffffffffffffffffffff8381831617835250505050604051610169919061057b565b6000604051808303816000865af19150503d80600081146101a6576040519150601f19603f3d011682016040523d82523d6000602084013e6101ab565b606091505b5091509150816101f0576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016101e7906105c8565b60405180910390fd5b600081806020019051810190610206919061040e565b9050809350505050919050565b60008060606000806001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16600080600181526020019081526020016000206001015460405160240161028c91906105ad565b6040516020818303038152906040527f8656fb06000000000000000000000000000000000000000000000000000000007bffffffffffffffffffffffffffffffffffffffffffffffffffffffff19166020820180517bffffffffffffffffffffffffffffffffffffffffffffffffffffffff8381831617835250505050604051610316919061057b565b6000604051808303816000865af19150503d8060008114610353576040519150601f19603f3d011682016040523d82523d6000602084013e610358565b606091505b50915091508161039d576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610394906105e8565b60405180910390fd5b6000818060200190518101906103b3919061040e565b905080935050505090565b3660008037600080366000845af43d6000803e80600081146103df573d6000f35b3d6000fd5b6000815190506103f3816106b0565b92915050565b600081359050610408816106c7565b92915050565b60006020828403121561042057600080fd5b600061042e848285016103e4565b91505092915050565b60006020828403121561044957600080fd5b6000610457848285016103f9565b91505092915050565b6104698161062f565b82525050565b61047881610653565b82525050565b600061048982610608565b6104938185610613565b93506104a381856020860161067d565b80840191505092915050565b60006104bc60288361061e565b91507f4d43415420636f6e74726163742066616272696365424920657865637574696f60008301527f6e204661696c65640000000000000000000000000000000000000000000000006020830152604082019050919050565b600061052260248361061e565b91507f4d43415420636f6e7472616374206765744d4320657865637574696f6e20466160008301527f696c6564000000000000000000000000000000000000000000000000000000006020830152604082019050919050565b6000610587828461047e565b915081905092915050565b60006020820190506105a76000830184610460565b92915050565b60006020820190506105c2600083018461046f565b92915050565b600060208201905081810360008301526105e1816104af565b9050919050565b6000602082019050818103600083015261060181610515565b9050919050565b600081519050919050565b600081905092915050565b600082825260208201905092915050565b600061063a8261065d565b9050919050565b600061064c8261065d565b9050919050565b6000819050919050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b60005b8381101561069b578082015181840152602081019050610680565b838111156106aa576000848401525b50505050565b6106b981610641565b81146106c457600080fd5b50565b6106d081610653565b81146106db57600080fd5b5056fea2646970667358221220db1aacf6f6d617740f7ab040bd2c07f22544e0ab2c7cbc91ebafb101bada952064736f6c63430006040033a2646970667358221220176c963d947f109422b662c120dd222b965102fa25efe7682591f3f1f1df737f64736f6c63430006040033",
}

// MCATEngineABI is the input ABI used to generate the binding from.
// Deprecated: Use MCATEngineMetaData.ABI instead.
var MCATEngineABI = MCATEngineMetaData.ABI

// MCATEngineBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use MCATEngineMetaData.Bin instead.
var MCATEngineBin = MCATEngineMetaData.Bin

// DeployMCATEngine deploys a new Ethereum contract, binding an instance of MCATEngine to it.
func DeployMCATEngine(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *MCATEngine, error) {
	parsed, err := MCATEngineMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(MCATEngineBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &MCATEngine{MCATEngineCaller: MCATEngineCaller{contract: contract}, MCATEngineTransactor: MCATEngineTransactor{contract: contract}, MCATEngineFilterer: MCATEngineFilterer{contract: contract}}, nil
}

// DeployMCATEngineSync deploys a new Ethereum contract and waits for receipt, binding an instance of MCATEngineSession to it.
func DeployMCATEngineSync(session *bind.TransactSession, backend bind.ContractBackend) (*types.Transaction, *types.Receipt, *MCATEngineSession, error) {
	parsed, err := abi.JSON(strings.NewReader(MCATEngineABI))
	if err != nil {
		return nil, nil, nil, err
	}
	session.Lock()
	address, tx, _, err := bind.DeployContract(session.TransactOpts, parsed, common.FromHex(MCATEngineBin), backend)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	receipt, err := session.WaitTransaction(tx)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	session.TransactOpts.Nonce.Add(session.TransactOpts.Nonce, big.NewInt(1))
	session.Unlock()
	contractSession, err := NewMCATEngineSession(address, backend, session)
	return tx, receipt, contractSession, err
}

// MCATEngine is an auto generated Go binding around an Ethereum contract.
type MCATEngine struct {
	MCATEngineCaller     // Read-only binding to the contract
	MCATEngineTransactor // Write-only binding to the contract
	MCATEngineFilterer   // Log filterer for contract events
}

// MCATEngineCaller is an auto generated read-only Go binding around an Ethereum contract.
type MCATEngineCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// MCATEngineTransactor is an auto generated write-only Go binding around an Ethereum contract.
type MCATEngineTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// MCATEngineFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type MCATEngineFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// MCATEngineSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type MCATEngineSession struct {
	Contract           *MCATEngine // Generic contract binding to set the session for
	transactionSession *bind.TransactSession
	Address            common.Address
}

// MCATEngineCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type MCATEngineCallerSession struct {
	Contract *MCATEngineCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts     // Call options to use throughout this session
}

// MCATEngineTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type MCATEngineTransactorSession struct {
	Contract     *MCATEngineTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts     // Transaction auth options to use throughout this session
}

// MCATEngineRaw is an auto generated low-level Go binding around an Ethereum contract.
type MCATEngineRaw struct {
	Contract *MCATEngine // Generic contract binding to access the raw methods on
}

// MCATEngineCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type MCATEngineCallerRaw struct {
	Contract *MCATEngineCaller // Generic read-only contract binding to access the raw methods on
}

// MCATEngineTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type MCATEngineTransactorRaw struct {
	Contract *MCATEngineTransactor // Generic write-only contract binding to access the raw methods on
}

// NewMCATEngine creates a new instance of MCATEngine, bound to a specific deployed contract.
func NewMCATEngine(address common.Address, backend bind.ContractBackend) (*MCATEngine, error) {
	contract, err := bindMCATEngine(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &MCATEngine{MCATEngineCaller: MCATEngineCaller{contract: contract}, MCATEngineTransactor: MCATEngineTransactor{contract: contract}, MCATEngineFilterer: MCATEngineFilterer{contract: contract}}, nil
}

// NewMCATEngineCaller creates a new read-only instance of MCATEngine, bound to a specific deployed contract.
func NewMCATEngineCaller(address common.Address, caller bind.ContractCaller) (*MCATEngineCaller, error) {
	contract, err := bindMCATEngine(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &MCATEngineCaller{contract: contract}, nil
}

// NewMCATEngineTransactor creates a new write-only instance of MCATEngine, bound to a specific deployed contract.
func NewMCATEngineTransactor(address common.Address, transactor bind.ContractTransactor) (*MCATEngineTransactor, error) {
	contract, err := bindMCATEngine(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &MCATEngineTransactor{contract: contract}, nil
}

// NewMCATEngineFilterer creates a new log filterer instance of MCATEngine, bound to a specific deployed contract.
func NewMCATEngineFilterer(address common.Address, filterer bind.ContractFilterer) (*MCATEngineFilterer, error) {
	contract, err := bindMCATEngine(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &MCATEngineFilterer{contract: contract}, nil
}

func NewMCATEngineSession(address common.Address, backend bind.ContractBackend, transactionSession *bind.TransactSession) (*MCATEngineSession, error) {
	MCATEngineInstance, err := NewMCATEngine(address, backend)
	if err != nil {
		return nil, err
	}
	return &MCATEngineSession{
		Contract:           MCATEngineInstance,
		transactionSession: transactionSession,
		Address:            address,
	}, nil
}

// bindMCATEngine binds a generic wrapper to an already deployed contract.
func bindMCATEngine(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := MCATEngineMetaData.GetAbi()
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, *parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_MCATEngine *MCATEngineRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _MCATEngine.Contract.MCATEngineCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_MCATEngine *MCATEngineRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _MCATEngine.Contract.MCATEngineTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_MCATEngine *MCATEngineRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _MCATEngine.Contract.MCATEngineTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_MCATEngine *MCATEngineCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _MCATEngine.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_MCATEngine *MCATEngineTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _MCATEngine.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_MCATEngine *MCATEngineTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _MCATEngine.Contract.contract.Transact(opts, method, params...)
}

// GetMC is a free data retrieval call binding the contract method 0x8656fb06.
//
// Solidity: function getMC(bytes32 _key) view returns(address)
func (_MCATEngine *MCATEngineCaller) GetMC(opts *bind.CallOpts, _key [32]byte) (common.Address, error) {
	var out []interface{}
	err := _MCATEngine.contract.Call(opts, &out, "getMC", _key)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetMC is a free data retrieval call binding the contract method 0x8656fb06.
//
// Solidity: function getMC(bytes32 _key) view returns(address)
func (_MCATEngine *MCATEngineSession) GetMC(_key [32]byte) (common.Address, error) {
	return _MCATEngine.Contract.GetMC(_MCATEngine.transactionSession.CallOpts, _key)
}

// GetMC is a free data retrieval call binding the contract method 0x8656fb06.
//
// Solidity: function getMC(bytes32 _key) view returns(address)
func (_MCATEngine *MCATEngineCallerSession) GetMC(_key [32]byte) (common.Address, error) {
	return _MCATEngine.Contract.GetMC(&_MCATEngine.CallOpts, _key)
}

// FabriceBI is a paid mutator transaction binding the contract method 0x78f1e498.
//
// Solidity: function fabriceBI(bytes32 _key) returns(address)
func (_MCATEngine *MCATEngineTransactor) FabriceBI(opts *bind.TransactOpts, _key [32]byte) (*types.Transaction, error) {
	return _MCATEngine.contract.Transact(opts, "fabriceBI", _key)
}

// FabriceBI is a paid mutator transaction binding the contract method 0x78f1e498.
//
// Solidity: function fabriceBI(bytes32 _key) returns(address)
func (_MCATEngine *MCATEngineSession) FabriceBI(_key [32]byte) (*types.Transaction, *types.Receipt, error) {
	_MCATEngine.transactionSession.Lock()
	tx, err := _MCATEngine.Contract.FabriceBI(_MCATEngine.transactionSession.TransactOpts, _key)
	if err != nil {
		_MCATEngine.transactionSession.Unlock()
		return nil, nil, err
	}
	_MCATEngine.transactionSession.TransactOpts.Nonce.Add(_MCATEngine.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_MCATEngine.transactionSession.Unlock()
	receipt, err := _MCATEngine.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// FabriceBI is a paid mutator transaction binding the contract method 0x78f1e498.
//
// Solidity: function fabriceBI(bytes32 _key) returns(address)
func (_MCATEngine *MCATEngineTransactorSession) FabriceBI(_key [32]byte) (*types.Transaction, error) {
	return _MCATEngine.Contract.FabriceBI(&_MCATEngine.TransactOpts, _key)
}

// SetMC is a paid mutator transaction binding the contract method 0x80977753.
//
// Solidity: function setMC(bytes32 _key, address _impl) returns()
func (_MCATEngine *MCATEngineTransactor) SetMC(opts *bind.TransactOpts, _key [32]byte, _impl common.Address) (*types.Transaction, error) {
	return _MCATEngine.contract.Transact(opts, "setMC", _key, _impl)
}

// SetMC is a paid mutator transaction binding the contract method 0x80977753.
//
// Solidity: function setMC(bytes32 _key, address _impl) returns()
func (_MCATEngine *MCATEngineSession) SetMC(_key [32]byte, _impl common.Address) (*types.Transaction, *types.Receipt, error) {
	_MCATEngine.transactionSession.Lock()
	tx, err := _MCATEngine.Contract.SetMC(_MCATEngine.transactionSession.TransactOpts, _key, _impl)
	if err != nil {
		_MCATEngine.transactionSession.Unlock()
		return nil, nil, err
	}
	_MCATEngine.transactionSession.TransactOpts.Nonce.Add(_MCATEngine.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_MCATEngine.transactionSession.Unlock()
	receipt, err := _MCATEngine.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetMC is a paid mutator transaction binding the contract method 0x80977753.
//
// Solidity: function setMC(bytes32 _key, address _impl) returns()
func (_MCATEngine *MCATEngineTransactorSession) SetMC(_key [32]byte, _impl common.Address) (*types.Transaction, error) {
	return _MCATEngine.Contract.SetMC(&_MCATEngine.TransactOpts, _key, _impl)
}
