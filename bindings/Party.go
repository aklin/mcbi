// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package bindings

import (
	"errors"
	"math/big"
	"strings"

	"gitlab.com/aklin/linkch"
	"gitlab.com/aklin/linkch/accounts/abi"
	"gitlab.com/aklin/linkch/accounts/abi/bind"
	"gitlab.com/aklin/linkch/common"
	"gitlab.com/aklin/linkch/core/types"
	"gitlab.com/aklin/linkch/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
	_ = abi.ConvertType
)

// PartyMetaData contains all meta data concerning the Party contract.
var PartyMetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"AncItem2Set\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"AncItemSet\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"Item2Set\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"ItemSet\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"ancestorPublicData\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"version\",\"type\":\"string\"},{\"internalType\":\"uint256\",\"name\":\"i\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"f1\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"}],\"name\":\"getAncestorItem\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"}],\"name\":\"getAncestorItem2\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getAncestorVersion\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"ret\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"}],\"name\":\"getPartyItem\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"}],\"name\":\"getPartyItem2\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getPartyVersion\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"ret\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_owner\",\"type\":\"address\"}],\"name\":\"init\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"partyPublicData\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"version\",\"type\":\"string\"},{\"internalType\":\"uint256\",\"name\":\"i\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"setAncestorItem\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"setAncestorItem2\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"value\",\"type\":\"string\"}],\"name\":\"setAncestorVersion\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"setPartyItem\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"setPartyItem2\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"value\",\"type\":\"string\"}],\"name\":\"setPartyVersion\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"z1\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
	Bin: "0x608060405234801561001057600080fd5b50611932806100206000396000f3fe608060405234801561001057600080fd5b506004361061010b5760003560e01c806378127ba7116100a2578063a911ed9411610071578063a911ed94146102bc578063c27fc305146102ee578063dc9a052b1461030c578063ec7503b014610328578063eca61ba9146103445761010b565b806378127ba71461023557806395522e7e146102665780639a4dd7b3146102825780639cbbd15b1461029e5761010b565b806324e0466a116100de57806324e0466a146101ad5780632ebc462f146101cb578063468d5ac2146101e757806376d695a1146102035761010b565b80630569191f14610110578063059ff0041461014157806319ab453c1461015f5780631b0695d11461017b575b600080fd5b61012a60048036038101906101259190611516565b610376565b604051610138929190611713565b60405180910390f35b610149610432565b60405161015691906116f1565b60405180910390f35b61017960048036038101906101749190611420565b610502565b005b61019560048036038101906101909190611449565b61061b565b6040516101a493929190611675565b60405180910390f35b6101b5610698565b6040516101c291906116f1565b60405180910390f35b6101e560048036038101906101e091906114d5565b610752565b005b61020160048036038101906101fc9190611472565b610826565b005b61021d60048036038101906102189190611449565b610a1f565b60405161022c93929190611675565b60405180910390f35b61024f600480360381019061024a9190611516565b610ab2565b60405161025d929190611713565b60405180910390f35b610280600480360381019061027b91906114d5565b610b6e565b005b61029c60048036038101906102979190611472565b610c42565b005b6102a6610e3b565b6040516102b39190611783565b60405180910390f35b6102d660048036038101906102d19190611449565b610e41565b6040516102e593929190611675565b60405180910390f35b6102f6610ed4565b6040516103039190611783565b60405180910390f35b61032660048036038101906103219190611472565b610eda565b005b610342600480360381019061033d9190611472565b61103f565b005b61035e60048036038101906103599190611449565b6111a4565b60405161036d93929190611675565b60405180910390f35b600b602052806000526040600020600091509050806000018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156104225780601f106103f757610100808354040283529160200191610422565b820191906000526020600020905b81548152906001019060200180831161040557829003601f168201915b5050505050908060010154905082565b60606004600060026000600181526020019081526020016000206000015481526020019081526020016000206000018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156104f55780601f106104ca576101008083540402835291602001916104f5565b820191906000526020600020905b8154815290600101906020018083116104d857829003601f168201915b5050505050905080905090565b600180600060018152602001908152602001600020600001600082815260200190815260200160002060009054906101000a900460ff1615610579576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161057090611763565b60405180910390fd5b610581611221565b81600360006001815260200190815260200160002060000160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550600180600060018152602001908152602001600020600001600083815260200190815260200160002060006101000a81548160ff0219169083151502179055505050565b6000806000610628611240565b600a600060018152602001908152602001600020600001600086815260200190815260200160002060405180606001604052908160008201548152602001600182015481526020016002820154815250509050806000015181602001518260400151935093509350509193909250565b6060600b6000600181526020019081526020016000206000018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156107455780601f1061071a57610100808354040283529160200191610745565b820191906000526020600020905b81548152906001019060200180831161072857829003601f168201915b5050505050905080905090565b600360006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16146107f7576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016107ee90611743565b60405180910390fd5b80600b600060018152602001908152602001600020600001908051906020019061082292919061126a565b5050565b600360006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16146108cb576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016108c290611743565b60405180910390fd5b8260036000600181526020019081526020016000206001016000868152602001908152602001600020600001819055508160036000600181526020019081526020016000206001016000868152602001908152602001600020600101819055508060036000600181526020019081526020016000206001016000868152602001908152602001600020600201819055507f08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be384600360006001815260200190815260200160002060010160008781526020019081526020016000206000015460036000600181526020019081526020016000206001016000888152602001908152602001600020600101546003600060018152602001908152602001600020600101600089815260200190815260200160002060020154604051610a1194939291906116ac565b60405180910390a150505050565b6000806000610a2c6112ea565b600360006002600060018152602001908152602001600020600001548152602001908152602001600020600101600086815260200190815260200160002060405180606001604052908160008201548152602001600182015481526020016002820154815250509050806000015181602001518260400151935093509350509193909250565b6004602052806000526040600020600091509050806000018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610b5e5780601f10610b3357610100808354040283529160200191610b5e565b820191906000526020600020905b815481529060010190602001808311610b4157829003601f168201915b5050505050908060010154905082565b600360006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610c13576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610c0a90611743565b60405180910390fd5b8060046000600181526020019081526020016000206000019080519060200190610c3e92919061126a565b5050565b600360006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610ce7576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610cde90611743565b60405180910390fd5b82600a60006001815260200190815260200160002060000160008681526020019081526020016000206000018190555081600a60006001815260200190815260200160002060000160008681526020019081526020016000206001018190555080600a6000600181526020019081526020016000206000016000868152602001908152602001600020600201819055507f4cdf50941fcfa155d7889dac17014ea0ad02c9996b5832c894ad301cd43b7fff84600a600060018152602001908152602001600020600001600087815260200190815260200160002060000154600a600060018152602001908152602001600020600001600088815260200190815260200160002060010154600a600060018152602001908152602001600020600001600089815260200190815260200160002060020154604051610e2d94939291906116ac565b60405180910390a150505050565b600f5481565b6000806000610e4e611314565b600360006002600060018152602001908152602001600020600001548152602001908152602001600020600201600086815260200190815260200160002060405180606001604052908160008201548152602001600182015481526020016002820154815250509050806000015181602001518260400151935093509350509193909250565b60085481565b600360006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610f7f576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610f7690611743565b60405180910390fd5b610f87611314565b83816000018181525050828160200181815250508181604001818152505080600360006001815260200190815260200160002060020160008781526020019081526020016000206000820151816000015560208201518160010155604082015181600201559050507f4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd8582600001518360200151846040015160405161103094939291906116ac565b60405180910390a15050505050565b600360006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16146110e4576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016110db90611743565b60405180910390fd5b6110ec61133e565b83816000018181525050828160200181815250508181604001818152505080600a60006001815260200190815260200160002060010160008781526020019081526020016000206000820151816000015560208201518160010155604082015181600201559050507fd487098619215fac3606de4bd35ef942edeca77d659758fc9d202b1ba0be9e398582600001518360200151846040015160405161119594939291906116ac565b60405180910390a15050505050565b60008060006111b161133e565b600a600060018152602001908152602001600020600101600086815260200190815260200160002060405180606001604052908160008201548152602001600182015481526020016002820154815250509050806000015181602001518260400151935093509350509193909250565b6001600260006001815260200190815260200160002060000181905550565b60405180606001604052806000801916815260200160008019168152602001600080191681525090565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106112ab57805160ff19168380011785556112d9565b828001600101855582156112d9579182015b828111156112d85782518255916020019190600101906112bd565b5b5090506112e69190611368565b5090565b60405180606001604052806000801916815260200160008019168152602001600080191681525090565b60405180606001604052806000801916815260200160008019168152602001600080191681525090565b60405180606001604052806000801916815260200160008019168152602001600080191681525090565b61138a91905b8082111561138657600081600090555060010161136e565b5090565b90565b60008135905061139c816118b7565b92915050565b6000813590506113b1816118ce565b92915050565b600082601f8301126113c857600080fd5b81356113db6113d6826117cb565b61179e565b915080825260208301602083018583830111156113f757600080fd5b611402838284611864565b50505092915050565b60008135905061141a816118e5565b92915050565b60006020828403121561143257600080fd5b60006114408482850161138d565b91505092915050565b60006020828403121561145b57600080fd5b6000611469848285016113a2565b91505092915050565b6000806000806080858703121561148857600080fd5b6000611496878288016113a2565b94505060206114a7878288016113a2565b93505060406114b8878288016113a2565b92505060606114c9878288016113a2565b91505092959194509250565b6000602082840312156114e757600080fd5b600082013567ffffffffffffffff81111561150157600080fd5b61150d848285016113b7565b91505092915050565b60006020828403121561152857600080fd5b60006115368482850161140b565b91505092915050565b61154881611830565b82525050565b600061155982611802565b611563818561180d565b9350611573818560208601611873565b61157c816118a6565b840191505092915050565b6000611592826117f7565b61159c818561180d565b93506115ac818560208601611873565b6115b5816118a6565b840191505092915050565b60006115cd601c8361180d565b91507f4f6e6c79206f776e65722063616e20657865632066756e6374696f6e000000006000830152602082019050919050565b600061160d60278361180d565b91507f436f6e747261637473206461746120696e2063656c6c206a75737420696e697460008301527f69616c697a6564000000000000000000000000000000000000000000000000006020830152604082019050919050565b61166f8161185a565b82525050565b600060608201905061168a600083018661153f565b611697602083018561153f565b6116a4604083018461153f565b949350505050565b60006080820190506116c1600083018761153f565b6116ce602083018661153f565b6116db604083018561153f565b6116e8606083018461153f565b95945050505050565b6000602082019050818103600083015261170b818461154e565b905092915050565b6000604082019050818103600083015261172d8185611587565b905061173c6020830184611666565b9392505050565b6000602082019050818103600083015261175c816115c0565b9050919050565b6000602082019050818103600083015261177c81611600565b9050919050565b60006020820190506117986000830184611666565b92915050565b6000604051905081810181811067ffffffffffffffff821117156117c157600080fd5b8060405250919050565b600067ffffffffffffffff8211156117e257600080fd5b601f19601f8301169050602081019050919050565b600081519050919050565b600081519050919050565b600082825260208201905092915050565b60006118298261183a565b9050919050565b6000819050919050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b6000819050919050565b82818337600083830152505050565b60005b83811015611891578082015181840152602081019050611876565b838111156118a0576000848401525b50505050565b6000601f19601f8301169050919050565b6118c08161181e565b81146118cb57600080fd5b50565b6118d781611830565b81146118e257600080fd5b50565b6118ee8161185a565b81146118f957600080fd5b5056fea264697066735822122029c519778bc172aaa97b1bc03ecee26341876074870d6846a28d4e05e51ad5cd64736f6c63430006040033",
}

// PartyABI is the input ABI used to generate the binding from.
// Deprecated: Use PartyMetaData.ABI instead.
var PartyABI = PartyMetaData.ABI

// PartyBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use PartyMetaData.Bin instead.
var PartyBin = PartyMetaData.Bin

// DeployParty deploys a new Ethereum contract, binding an instance of Party to it.
func DeployParty(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *Party, error) {
	parsed, err := PartyMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(PartyBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Party{PartyCaller: PartyCaller{contract: contract}, PartyTransactor: PartyTransactor{contract: contract}, PartyFilterer: PartyFilterer{contract: contract}}, nil
}

// DeployPartySync deploys a new Ethereum contract and waits for receipt, binding an instance of PartySession to it.
func DeployPartySync(session *bind.TransactSession, backend bind.ContractBackend) (*types.Transaction, *types.Receipt, *PartySession, error) {
	parsed, err := abi.JSON(strings.NewReader(PartyABI))
	if err != nil {
		return nil, nil, nil, err
	}
	session.Lock()
	address, tx, _, err := bind.DeployContract(session.TransactOpts, parsed, common.FromHex(PartyBin), backend)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	receipt, err := session.WaitTransaction(tx)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	session.TransactOpts.Nonce.Add(session.TransactOpts.Nonce, big.NewInt(1))
	session.Unlock()
	contractSession, err := NewPartySession(address, backend, session)
	return tx, receipt, contractSession, err
}

// Party is an auto generated Go binding around an Ethereum contract.
type Party struct {
	PartyCaller     // Read-only binding to the contract
	PartyTransactor // Write-only binding to the contract
	PartyFilterer   // Log filterer for contract events
}

// PartyCaller is an auto generated read-only Go binding around an Ethereum contract.
type PartyCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// PartyTransactor is an auto generated write-only Go binding around an Ethereum contract.
type PartyTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// PartyFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type PartyFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// PartySession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type PartySession struct {
	Contract           *Party // Generic contract binding to set the session for
	transactionSession *bind.TransactSession
	Address            common.Address
}

// PartyCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type PartyCallerSession struct {
	Contract *PartyCaller  // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// PartyTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type PartyTransactorSession struct {
	Contract     *PartyTransactor  // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// PartyRaw is an auto generated low-level Go binding around an Ethereum contract.
type PartyRaw struct {
	Contract *Party // Generic contract binding to access the raw methods on
}

// PartyCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type PartyCallerRaw struct {
	Contract *PartyCaller // Generic read-only contract binding to access the raw methods on
}

// PartyTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type PartyTransactorRaw struct {
	Contract *PartyTransactor // Generic write-only contract binding to access the raw methods on
}

// NewParty creates a new instance of Party, bound to a specific deployed contract.
func NewParty(address common.Address, backend bind.ContractBackend) (*Party, error) {
	contract, err := bindParty(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Party{PartyCaller: PartyCaller{contract: contract}, PartyTransactor: PartyTransactor{contract: contract}, PartyFilterer: PartyFilterer{contract: contract}}, nil
}

// NewPartyCaller creates a new read-only instance of Party, bound to a specific deployed contract.
func NewPartyCaller(address common.Address, caller bind.ContractCaller) (*PartyCaller, error) {
	contract, err := bindParty(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &PartyCaller{contract: contract}, nil
}

// NewPartyTransactor creates a new write-only instance of Party, bound to a specific deployed contract.
func NewPartyTransactor(address common.Address, transactor bind.ContractTransactor) (*PartyTransactor, error) {
	contract, err := bindParty(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &PartyTransactor{contract: contract}, nil
}

// NewPartyFilterer creates a new log filterer instance of Party, bound to a specific deployed contract.
func NewPartyFilterer(address common.Address, filterer bind.ContractFilterer) (*PartyFilterer, error) {
	contract, err := bindParty(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &PartyFilterer{contract: contract}, nil
}

func NewPartySession(address common.Address, backend bind.ContractBackend, transactionSession *bind.TransactSession) (*PartySession, error) {
	PartyInstance, err := NewParty(address, backend)
	if err != nil {
		return nil, err
	}
	return &PartySession{
		Contract:           PartyInstance,
		transactionSession: transactionSession,
		Address:            address,
	}, nil
}

// bindParty binds a generic wrapper to an already deployed contract.
func bindParty(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := PartyMetaData.GetAbi()
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, *parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Party *PartyRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Party.Contract.PartyCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Party *PartyRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Party.Contract.PartyTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Party *PartyRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Party.Contract.PartyTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Party *PartyCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Party.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Party *PartyTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Party.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Party *PartyTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Party.Contract.contract.Transact(opts, method, params...)
}

// AncestorPublicData is a free data retrieval call binding the contract method 0x78127ba7.
//
// Solidity: function ancestorPublicData(uint256 ) view returns(string version, uint256 i)
func (_Party *PartyCaller) AncestorPublicData(opts *bind.CallOpts, arg0 *big.Int) (struct {
	Version string
	I       *big.Int
}, error) {
	var out []interface{}
	err := _Party.contract.Call(opts, &out, "ancestorPublicData", arg0)

	outstruct := new(struct {
		Version string
		I       *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Version = *abi.ConvertType(out[0], new(string)).(*string)
	outstruct.I = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// AncestorPublicData is a free data retrieval call binding the contract method 0x78127ba7.
//
// Solidity: function ancestorPublicData(uint256 ) view returns(string version, uint256 i)
func (_Party *PartySession) AncestorPublicData(arg0 *big.Int) (struct {
	Version string
	I       *big.Int
}, error) {
	return _Party.Contract.AncestorPublicData(_Party.transactionSession.CallOpts, arg0)
}

// AncestorPublicData is a free data retrieval call binding the contract method 0x78127ba7.
//
// Solidity: function ancestorPublicData(uint256 ) view returns(string version, uint256 i)
func (_Party *PartyCallerSession) AncestorPublicData(arg0 *big.Int) (struct {
	Version string
	I       *big.Int
}, error) {
	return _Party.Contract.AncestorPublicData(&_Party.CallOpts, arg0)
}

// F1 is a free data retrieval call binding the contract method 0xc27fc305.
//
// Solidity: function f1() view returns(uint256)
func (_Party *PartyCaller) F1(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Party.contract.Call(opts, &out, "f1")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// F1 is a free data retrieval call binding the contract method 0xc27fc305.
//
// Solidity: function f1() view returns(uint256)
func (_Party *PartySession) F1() (*big.Int, error) {
	return _Party.Contract.F1(_Party.transactionSession.CallOpts)
}

// F1 is a free data retrieval call binding the contract method 0xc27fc305.
//
// Solidity: function f1() view returns(uint256)
func (_Party *PartyCallerSession) F1() (*big.Int, error) {
	return _Party.Contract.F1(&_Party.CallOpts)
}

// GetAncestorItem is a free data retrieval call binding the contract method 0x76d695a1.
//
// Solidity: function getAncestorItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Party *PartyCaller) GetAncestorItem(opts *bind.CallOpts, key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	var out []interface{}
	err := _Party.contract.Call(opts, &out, "getAncestorItem", key)

	if err != nil {
		return *new([32]byte), *new([32]byte), *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)
	out1 := *abi.ConvertType(out[1], new([32]byte)).(*[32]byte)
	out2 := *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)

	return out0, out1, out2, err

}

// GetAncestorItem is a free data retrieval call binding the contract method 0x76d695a1.
//
// Solidity: function getAncestorItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Party *PartySession) GetAncestorItem(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Party.Contract.GetAncestorItem(_Party.transactionSession.CallOpts, key)
}

// GetAncestorItem is a free data retrieval call binding the contract method 0x76d695a1.
//
// Solidity: function getAncestorItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Party *PartyCallerSession) GetAncestorItem(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Party.Contract.GetAncestorItem(&_Party.CallOpts, key)
}

// GetAncestorItem2 is a free data retrieval call binding the contract method 0xa911ed94.
//
// Solidity: function getAncestorItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Party *PartyCaller) GetAncestorItem2(opts *bind.CallOpts, key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	var out []interface{}
	err := _Party.contract.Call(opts, &out, "getAncestorItem2", key)

	if err != nil {
		return *new([32]byte), *new([32]byte), *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)
	out1 := *abi.ConvertType(out[1], new([32]byte)).(*[32]byte)
	out2 := *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)

	return out0, out1, out2, err

}

// GetAncestorItem2 is a free data retrieval call binding the contract method 0xa911ed94.
//
// Solidity: function getAncestorItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Party *PartySession) GetAncestorItem2(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Party.Contract.GetAncestorItem2(_Party.transactionSession.CallOpts, key)
}

// GetAncestorItem2 is a free data retrieval call binding the contract method 0xa911ed94.
//
// Solidity: function getAncestorItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Party *PartyCallerSession) GetAncestorItem2(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Party.Contract.GetAncestorItem2(&_Party.CallOpts, key)
}

// GetAncestorVersion is a free data retrieval call binding the contract method 0x059ff004.
//
// Solidity: function getAncestorVersion() view returns(string ret)
func (_Party *PartyCaller) GetAncestorVersion(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Party.contract.Call(opts, &out, "getAncestorVersion")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// GetAncestorVersion is a free data retrieval call binding the contract method 0x059ff004.
//
// Solidity: function getAncestorVersion() view returns(string ret)
func (_Party *PartySession) GetAncestorVersion() (string, error) {
	return _Party.Contract.GetAncestorVersion(_Party.transactionSession.CallOpts)
}

// GetAncestorVersion is a free data retrieval call binding the contract method 0x059ff004.
//
// Solidity: function getAncestorVersion() view returns(string ret)
func (_Party *PartyCallerSession) GetAncestorVersion() (string, error) {
	return _Party.Contract.GetAncestorVersion(&_Party.CallOpts)
}

// GetPartyItem is a free data retrieval call binding the contract method 0x1b0695d1.
//
// Solidity: function getPartyItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Party *PartyCaller) GetPartyItem(opts *bind.CallOpts, key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	var out []interface{}
	err := _Party.contract.Call(opts, &out, "getPartyItem", key)

	if err != nil {
		return *new([32]byte), *new([32]byte), *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)
	out1 := *abi.ConvertType(out[1], new([32]byte)).(*[32]byte)
	out2 := *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)

	return out0, out1, out2, err

}

// GetPartyItem is a free data retrieval call binding the contract method 0x1b0695d1.
//
// Solidity: function getPartyItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Party *PartySession) GetPartyItem(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Party.Contract.GetPartyItem(_Party.transactionSession.CallOpts, key)
}

// GetPartyItem is a free data retrieval call binding the contract method 0x1b0695d1.
//
// Solidity: function getPartyItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Party *PartyCallerSession) GetPartyItem(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Party.Contract.GetPartyItem(&_Party.CallOpts, key)
}

// GetPartyItem2 is a free data retrieval call binding the contract method 0xeca61ba9.
//
// Solidity: function getPartyItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Party *PartyCaller) GetPartyItem2(opts *bind.CallOpts, key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	var out []interface{}
	err := _Party.contract.Call(opts, &out, "getPartyItem2", key)

	if err != nil {
		return *new([32]byte), *new([32]byte), *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)
	out1 := *abi.ConvertType(out[1], new([32]byte)).(*[32]byte)
	out2 := *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)

	return out0, out1, out2, err

}

// GetPartyItem2 is a free data retrieval call binding the contract method 0xeca61ba9.
//
// Solidity: function getPartyItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Party *PartySession) GetPartyItem2(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Party.Contract.GetPartyItem2(_Party.transactionSession.CallOpts, key)
}

// GetPartyItem2 is a free data retrieval call binding the contract method 0xeca61ba9.
//
// Solidity: function getPartyItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Party *PartyCallerSession) GetPartyItem2(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Party.Contract.GetPartyItem2(&_Party.CallOpts, key)
}

// GetPartyVersion is a free data retrieval call binding the contract method 0x24e0466a.
//
// Solidity: function getPartyVersion() view returns(string ret)
func (_Party *PartyCaller) GetPartyVersion(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Party.contract.Call(opts, &out, "getPartyVersion")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// GetPartyVersion is a free data retrieval call binding the contract method 0x24e0466a.
//
// Solidity: function getPartyVersion() view returns(string ret)
func (_Party *PartySession) GetPartyVersion() (string, error) {
	return _Party.Contract.GetPartyVersion(_Party.transactionSession.CallOpts)
}

// GetPartyVersion is a free data retrieval call binding the contract method 0x24e0466a.
//
// Solidity: function getPartyVersion() view returns(string ret)
func (_Party *PartyCallerSession) GetPartyVersion() (string, error) {
	return _Party.Contract.GetPartyVersion(&_Party.CallOpts)
}

// PartyPublicData is a free data retrieval call binding the contract method 0x0569191f.
//
// Solidity: function partyPublicData(uint256 ) view returns(string version, uint256 i)
func (_Party *PartyCaller) PartyPublicData(opts *bind.CallOpts, arg0 *big.Int) (struct {
	Version string
	I       *big.Int
}, error) {
	var out []interface{}
	err := _Party.contract.Call(opts, &out, "partyPublicData", arg0)

	outstruct := new(struct {
		Version string
		I       *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Version = *abi.ConvertType(out[0], new(string)).(*string)
	outstruct.I = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// PartyPublicData is a free data retrieval call binding the contract method 0x0569191f.
//
// Solidity: function partyPublicData(uint256 ) view returns(string version, uint256 i)
func (_Party *PartySession) PartyPublicData(arg0 *big.Int) (struct {
	Version string
	I       *big.Int
}, error) {
	return _Party.Contract.PartyPublicData(_Party.transactionSession.CallOpts, arg0)
}

// PartyPublicData is a free data retrieval call binding the contract method 0x0569191f.
//
// Solidity: function partyPublicData(uint256 ) view returns(string version, uint256 i)
func (_Party *PartyCallerSession) PartyPublicData(arg0 *big.Int) (struct {
	Version string
	I       *big.Int
}, error) {
	return _Party.Contract.PartyPublicData(&_Party.CallOpts, arg0)
}

// Z1 is a free data retrieval call binding the contract method 0x9cbbd15b.
//
// Solidity: function z1() view returns(uint256)
func (_Party *PartyCaller) Z1(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Party.contract.Call(opts, &out, "z1")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Z1 is a free data retrieval call binding the contract method 0x9cbbd15b.
//
// Solidity: function z1() view returns(uint256)
func (_Party *PartySession) Z1() (*big.Int, error) {
	return _Party.Contract.Z1(_Party.transactionSession.CallOpts)
}

// Z1 is a free data retrieval call binding the contract method 0x9cbbd15b.
//
// Solidity: function z1() view returns(uint256)
func (_Party *PartyCallerSession) Z1() (*big.Int, error) {
	return _Party.Contract.Z1(&_Party.CallOpts)
}

// Init is a paid mutator transaction binding the contract method 0x19ab453c.
//
// Solidity: function init(address _owner) returns()
func (_Party *PartyTransactor) Init(opts *bind.TransactOpts, _owner common.Address) (*types.Transaction, error) {
	return _Party.contract.Transact(opts, "init", _owner)
}

// Init is a paid mutator transaction binding the contract method 0x19ab453c.
//
// Solidity: function init(address _owner) returns()
func (_Party *PartySession) Init(_owner common.Address) (*types.Transaction, *types.Receipt, error) {
	_Party.transactionSession.Lock()
	tx, err := _Party.Contract.Init(_Party.transactionSession.TransactOpts, _owner)
	if err != nil {
		_Party.transactionSession.Unlock()
		return nil, nil, err
	}
	_Party.transactionSession.TransactOpts.Nonce.Add(_Party.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Party.transactionSession.Unlock()
	receipt, err := _Party.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// Init is a paid mutator transaction binding the contract method 0x19ab453c.
//
// Solidity: function init(address _owner) returns()
func (_Party *PartyTransactorSession) Init(_owner common.Address) (*types.Transaction, error) {
	return _Party.Contract.Init(&_Party.TransactOpts, _owner)
}

// SetAncestorItem is a paid mutator transaction binding the contract method 0x468d5ac2.
//
// Solidity: function setAncestorItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Party *PartyTransactor) SetAncestorItem(opts *bind.TransactOpts, key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Party.contract.Transact(opts, "setAncestorItem", key, value, dataType, dataDescr)
}

// SetAncestorItem is a paid mutator transaction binding the contract method 0x468d5ac2.
//
// Solidity: function setAncestorItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Party *PartySession) SetAncestorItem(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, *types.Receipt, error) {
	_Party.transactionSession.Lock()
	tx, err := _Party.Contract.SetAncestorItem(_Party.transactionSession.TransactOpts, key, value, dataType, dataDescr)
	if err != nil {
		_Party.transactionSession.Unlock()
		return nil, nil, err
	}
	_Party.transactionSession.TransactOpts.Nonce.Add(_Party.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Party.transactionSession.Unlock()
	receipt, err := _Party.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetAncestorItem is a paid mutator transaction binding the contract method 0x468d5ac2.
//
// Solidity: function setAncestorItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Party *PartyTransactorSession) SetAncestorItem(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Party.Contract.SetAncestorItem(&_Party.TransactOpts, key, value, dataType, dataDescr)
}

// SetAncestorItem2 is a paid mutator transaction binding the contract method 0xdc9a052b.
//
// Solidity: function setAncestorItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Party *PartyTransactor) SetAncestorItem2(opts *bind.TransactOpts, key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Party.contract.Transact(opts, "setAncestorItem2", key, value, dataType, dataDescr)
}

// SetAncestorItem2 is a paid mutator transaction binding the contract method 0xdc9a052b.
//
// Solidity: function setAncestorItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Party *PartySession) SetAncestorItem2(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, *types.Receipt, error) {
	_Party.transactionSession.Lock()
	tx, err := _Party.Contract.SetAncestorItem2(_Party.transactionSession.TransactOpts, key, value, dataType, dataDescr)
	if err != nil {
		_Party.transactionSession.Unlock()
		return nil, nil, err
	}
	_Party.transactionSession.TransactOpts.Nonce.Add(_Party.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Party.transactionSession.Unlock()
	receipt, err := _Party.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetAncestorItem2 is a paid mutator transaction binding the contract method 0xdc9a052b.
//
// Solidity: function setAncestorItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Party *PartyTransactorSession) SetAncestorItem2(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Party.Contract.SetAncestorItem2(&_Party.TransactOpts, key, value, dataType, dataDescr)
}

// SetAncestorVersion is a paid mutator transaction binding the contract method 0x95522e7e.
//
// Solidity: function setAncestorVersion(string value) returns()
func (_Party *PartyTransactor) SetAncestorVersion(opts *bind.TransactOpts, value string) (*types.Transaction, error) {
	return _Party.contract.Transact(opts, "setAncestorVersion", value)
}

// SetAncestorVersion is a paid mutator transaction binding the contract method 0x95522e7e.
//
// Solidity: function setAncestorVersion(string value) returns()
func (_Party *PartySession) SetAncestorVersion(value string) (*types.Transaction, *types.Receipt, error) {
	_Party.transactionSession.Lock()
	tx, err := _Party.Contract.SetAncestorVersion(_Party.transactionSession.TransactOpts, value)
	if err != nil {
		_Party.transactionSession.Unlock()
		return nil, nil, err
	}
	_Party.transactionSession.TransactOpts.Nonce.Add(_Party.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Party.transactionSession.Unlock()
	receipt, err := _Party.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetAncestorVersion is a paid mutator transaction binding the contract method 0x95522e7e.
//
// Solidity: function setAncestorVersion(string value) returns()
func (_Party *PartyTransactorSession) SetAncestorVersion(value string) (*types.Transaction, error) {
	return _Party.Contract.SetAncestorVersion(&_Party.TransactOpts, value)
}

// SetPartyItem is a paid mutator transaction binding the contract method 0x9a4dd7b3.
//
// Solidity: function setPartyItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Party *PartyTransactor) SetPartyItem(opts *bind.TransactOpts, key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Party.contract.Transact(opts, "setPartyItem", key, value, dataType, dataDescr)
}

// SetPartyItem is a paid mutator transaction binding the contract method 0x9a4dd7b3.
//
// Solidity: function setPartyItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Party *PartySession) SetPartyItem(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, *types.Receipt, error) {
	_Party.transactionSession.Lock()
	tx, err := _Party.Contract.SetPartyItem(_Party.transactionSession.TransactOpts, key, value, dataType, dataDescr)
	if err != nil {
		_Party.transactionSession.Unlock()
		return nil, nil, err
	}
	_Party.transactionSession.TransactOpts.Nonce.Add(_Party.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Party.transactionSession.Unlock()
	receipt, err := _Party.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetPartyItem is a paid mutator transaction binding the contract method 0x9a4dd7b3.
//
// Solidity: function setPartyItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Party *PartyTransactorSession) SetPartyItem(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Party.Contract.SetPartyItem(&_Party.TransactOpts, key, value, dataType, dataDescr)
}

// SetPartyItem2 is a paid mutator transaction binding the contract method 0xec7503b0.
//
// Solidity: function setPartyItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Party *PartyTransactor) SetPartyItem2(opts *bind.TransactOpts, key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Party.contract.Transact(opts, "setPartyItem2", key, value, dataType, dataDescr)
}

// SetPartyItem2 is a paid mutator transaction binding the contract method 0xec7503b0.
//
// Solidity: function setPartyItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Party *PartySession) SetPartyItem2(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, *types.Receipt, error) {
	_Party.transactionSession.Lock()
	tx, err := _Party.Contract.SetPartyItem2(_Party.transactionSession.TransactOpts, key, value, dataType, dataDescr)
	if err != nil {
		_Party.transactionSession.Unlock()
		return nil, nil, err
	}
	_Party.transactionSession.TransactOpts.Nonce.Add(_Party.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Party.transactionSession.Unlock()
	receipt, err := _Party.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetPartyItem2 is a paid mutator transaction binding the contract method 0xec7503b0.
//
// Solidity: function setPartyItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Party *PartyTransactorSession) SetPartyItem2(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Party.Contract.SetPartyItem2(&_Party.TransactOpts, key, value, dataType, dataDescr)
}

// SetPartyVersion is a paid mutator transaction binding the contract method 0x2ebc462f.
//
// Solidity: function setPartyVersion(string value) returns()
func (_Party *PartyTransactor) SetPartyVersion(opts *bind.TransactOpts, value string) (*types.Transaction, error) {
	return _Party.contract.Transact(opts, "setPartyVersion", value)
}

// SetPartyVersion is a paid mutator transaction binding the contract method 0x2ebc462f.
//
// Solidity: function setPartyVersion(string value) returns()
func (_Party *PartySession) SetPartyVersion(value string) (*types.Transaction, *types.Receipt, error) {
	_Party.transactionSession.Lock()
	tx, err := _Party.Contract.SetPartyVersion(_Party.transactionSession.TransactOpts, value)
	if err != nil {
		_Party.transactionSession.Unlock()
		return nil, nil, err
	}
	_Party.transactionSession.TransactOpts.Nonce.Add(_Party.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Party.transactionSession.Unlock()
	receipt, err := _Party.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetPartyVersion is a paid mutator transaction binding the contract method 0x2ebc462f.
//
// Solidity: function setPartyVersion(string value) returns()
func (_Party *PartyTransactorSession) SetPartyVersion(value string) (*types.Transaction, error) {
	return _Party.Contract.SetPartyVersion(&_Party.TransactOpts, value)
}

// PartyAncItem2SetIterator is returned from FilterAncItem2Set and is used to iterate over the raw logs and unpacked data for AncItem2Set events raised by the Party contract.
type PartyAncItem2SetIterator struct {
	Event *PartyAncItem2Set // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *PartyAncItem2SetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(PartyAncItem2Set)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(PartyAncItem2Set)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *PartyAncItem2SetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *PartyAncItem2SetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// PartyAncItem2Set represents a AncItem2Set event raised by the Party contract.
type PartyAncItem2Set struct {
	Key       [32]byte
	Value     [32]byte
	DataType  [32]byte
	DataDescr [32]byte
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterAncItem2Set is a free log retrieval operation binding the contract event 0x4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd.
//
// Solidity: event AncItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Party *PartyFilterer) FilterAncItem2Set(opts *bind.FilterOpts) (*PartyAncItem2SetIterator, error) {

	logs, sub, err := _Party.contract.FilterLogs(opts, "AncItem2Set")
	if err != nil {
		return nil, err
	}
	return &PartyAncItem2SetIterator{contract: _Party.contract, event: "AncItem2Set", logs: logs, sub: sub}, nil
}

// WatchAncItem2Set is a free log subscription operation binding the contract event 0x4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd.
//
// Solidity: event AncItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Party *PartyFilterer) WatchAncItem2Set(opts *bind.WatchOpts, sink chan<- *PartyAncItem2Set) (event.Subscription, error) {

	logs, sub, err := _Party.contract.WatchLogs(opts, "AncItem2Set")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(PartyAncItem2Set)
				if err := _Party.contract.UnpackLog(event, "AncItem2Set", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAncItem2Set is a log parse operation binding the contract event 0x4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd.
//
// Solidity: event AncItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Party *PartyFilterer) ParseAncItem2Set(log types.Log) (*PartyAncItem2Set, error) {
	event := new(PartyAncItem2Set)
	if err := _Party.contract.UnpackLog(event, "AncItem2Set", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// PartyAncItemSetIterator is returned from FilterAncItemSet and is used to iterate over the raw logs and unpacked data for AncItemSet events raised by the Party contract.
type PartyAncItemSetIterator struct {
	Event *PartyAncItemSet // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *PartyAncItemSetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(PartyAncItemSet)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(PartyAncItemSet)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *PartyAncItemSetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *PartyAncItemSetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// PartyAncItemSet represents a AncItemSet event raised by the Party contract.
type PartyAncItemSet struct {
	Key       [32]byte
	Value     [32]byte
	DataType  [32]byte
	DataDescr [32]byte
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterAncItemSet is a free log retrieval operation binding the contract event 0x08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be3.
//
// Solidity: event AncItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Party *PartyFilterer) FilterAncItemSet(opts *bind.FilterOpts) (*PartyAncItemSetIterator, error) {

	logs, sub, err := _Party.contract.FilterLogs(opts, "AncItemSet")
	if err != nil {
		return nil, err
	}
	return &PartyAncItemSetIterator{contract: _Party.contract, event: "AncItemSet", logs: logs, sub: sub}, nil
}

// WatchAncItemSet is a free log subscription operation binding the contract event 0x08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be3.
//
// Solidity: event AncItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Party *PartyFilterer) WatchAncItemSet(opts *bind.WatchOpts, sink chan<- *PartyAncItemSet) (event.Subscription, error) {

	logs, sub, err := _Party.contract.WatchLogs(opts, "AncItemSet")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(PartyAncItemSet)
				if err := _Party.contract.UnpackLog(event, "AncItemSet", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAncItemSet is a log parse operation binding the contract event 0x08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be3.
//
// Solidity: event AncItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Party *PartyFilterer) ParseAncItemSet(log types.Log) (*PartyAncItemSet, error) {
	event := new(PartyAncItemSet)
	if err := _Party.contract.UnpackLog(event, "AncItemSet", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// PartyItem2SetIterator is returned from FilterItem2Set and is used to iterate over the raw logs and unpacked data for Item2Set events raised by the Party contract.
type PartyItem2SetIterator struct {
	Event *PartyItem2Set // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *PartyItem2SetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(PartyItem2Set)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(PartyItem2Set)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *PartyItem2SetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *PartyItem2SetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// PartyItem2Set represents a Item2Set event raised by the Party contract.
type PartyItem2Set struct {
	Key       [32]byte
	Value     [32]byte
	DataType  [32]byte
	DataDescr [32]byte
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterItem2Set is a free log retrieval operation binding the contract event 0xd487098619215fac3606de4bd35ef942edeca77d659758fc9d202b1ba0be9e39.
//
// Solidity: event Item2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Party *PartyFilterer) FilterItem2Set(opts *bind.FilterOpts) (*PartyItem2SetIterator, error) {

	logs, sub, err := _Party.contract.FilterLogs(opts, "Item2Set")
	if err != nil {
		return nil, err
	}
	return &PartyItem2SetIterator{contract: _Party.contract, event: "Item2Set", logs: logs, sub: sub}, nil
}

// WatchItem2Set is a free log subscription operation binding the contract event 0xd487098619215fac3606de4bd35ef942edeca77d659758fc9d202b1ba0be9e39.
//
// Solidity: event Item2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Party *PartyFilterer) WatchItem2Set(opts *bind.WatchOpts, sink chan<- *PartyItem2Set) (event.Subscription, error) {

	logs, sub, err := _Party.contract.WatchLogs(opts, "Item2Set")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(PartyItem2Set)
				if err := _Party.contract.UnpackLog(event, "Item2Set", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseItem2Set is a log parse operation binding the contract event 0xd487098619215fac3606de4bd35ef942edeca77d659758fc9d202b1ba0be9e39.
//
// Solidity: event Item2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Party *PartyFilterer) ParseItem2Set(log types.Log) (*PartyItem2Set, error) {
	event := new(PartyItem2Set)
	if err := _Party.contract.UnpackLog(event, "Item2Set", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// PartyItemSetIterator is returned from FilterItemSet and is used to iterate over the raw logs and unpacked data for ItemSet events raised by the Party contract.
type PartyItemSetIterator struct {
	Event *PartyItemSet // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *PartyItemSetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(PartyItemSet)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(PartyItemSet)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *PartyItemSetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *PartyItemSetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// PartyItemSet represents a ItemSet event raised by the Party contract.
type PartyItemSet struct {
	Key       [32]byte
	Value     [32]byte
	DataType  [32]byte
	DataDescr [32]byte
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterItemSet is a free log retrieval operation binding the contract event 0x4cdf50941fcfa155d7889dac17014ea0ad02c9996b5832c894ad301cd43b7fff.
//
// Solidity: event ItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Party *PartyFilterer) FilterItemSet(opts *bind.FilterOpts) (*PartyItemSetIterator, error) {

	logs, sub, err := _Party.contract.FilterLogs(opts, "ItemSet")
	if err != nil {
		return nil, err
	}
	return &PartyItemSetIterator{contract: _Party.contract, event: "ItemSet", logs: logs, sub: sub}, nil
}

// WatchItemSet is a free log subscription operation binding the contract event 0x4cdf50941fcfa155d7889dac17014ea0ad02c9996b5832c894ad301cd43b7fff.
//
// Solidity: event ItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Party *PartyFilterer) WatchItemSet(opts *bind.WatchOpts, sink chan<- *PartyItemSet) (event.Subscription, error) {

	logs, sub, err := _Party.contract.WatchLogs(opts, "ItemSet")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(PartyItemSet)
				if err := _Party.contract.UnpackLog(event, "ItemSet", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseItemSet is a log parse operation binding the contract event 0x4cdf50941fcfa155d7889dac17014ea0ad02c9996b5832c894ad301cd43b7fff.
//
// Solidity: event ItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Party *PartyFilterer) ParseItemSet(log types.Log) (*PartyItemSet, error) {
	event := new(PartyItemSet)
	if err := _Party.contract.UnpackLog(event, "ItemSet", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
