// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package bindings

import (
	"errors"
	"math/big"
	"strings"

	"gitlab.com/aklin/linkch"
	"gitlab.com/aklin/linkch/accounts/abi"
	"gitlab.com/aklin/linkch/accounts/abi/bind"
	"gitlab.com/aklin/linkch/common"
	"gitlab.com/aklin/linkch/core/types"
	"gitlab.com/aklin/linkch/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
	_ = abi.ConvertType
)

// ProtoMetaData contains all meta data concerning the Proto contract.
var ProtoMetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"AncItem2Set\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"AncItemSet\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"Item2Set\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"ItemSet\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"ancestorPublicData\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"version\",\"type\":\"string\"},{\"internalType\":\"uint256\",\"name\":\"i\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"f1\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"}],\"name\":\"getAncestorItem\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"}],\"name\":\"getAncestorItem2\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getAncestorVersion\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"ret\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getParty\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getParty2\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"}],\"name\":\"getProtoItem\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"}],\"name\":\"getProtoItem2\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getProtoVersion\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"ret\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_owner\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_party\",\"type\":\"address\"}],\"name\":\"init\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"protoPublicData\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"version\",\"type\":\"string\"},{\"internalType\":\"uint256\",\"name\":\"i\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"setAncestorItem\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"setAncestorItem2\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"value\",\"type\":\"string\"}],\"name\":\"setAncestorVersion\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"setProtoItem\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataType\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"dataDescr\",\"type\":\"bytes32\"}],\"name\":\"setProtoItem2\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"value\",\"type\":\"string\"}],\"name\":\"setProtoVersion\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"z1\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
	Bin: "0x608060405234801561001057600080fd5b50611e36806100206000396000f3fe608060405234801561001057600080fd5b50600436106101215760003560e01c80637f890baa116100ad578063b4caa93311610071578063b4caa93314610338578063c27fc30514610356578063dc9a052b14610374578063f047ac9414610390578063f09a4016146103ac57610121565b80637f890baa1461029257806390736301146102ae57806395522e7e146102cc5780639cbbd15b146102e8578063a911ed941461030657610121565b806347037fdc116100f457806347037fdc146101c35780635fdae263146101df5780636c012ade146101fd57806376d695a11461022f57806378127ba71461026157610121565b8063059ff004146101265780631b8efb83146101445780632e695d1014610176578063468d5ac2146101a7575b600080fd5b61012e6103c8565b60405161013b9190611bdc565b60405180910390f35b61015e600480360381019061015991906118e4565b610498565b60405161016d93929190611b60565b60405180910390f35b610190600480360381019061018b91906119b1565b610515565b60405161019e929190611bfe565b60405180910390f35b6101c160048036038101906101bc919061190d565b6105d1565b005b6101dd60048036038101906101d89190611970565b6107ca565b005b6101e761089e565b6040516101f49190611bdc565b60405180910390f35b610217600480360381019061021291906118e4565b610958565b60405161022693929190611b60565b60405180910390f35b610249600480360381019061024491906118e4565b6109d5565b60405161025893929190611b60565b60405180910390f35b61027b600480360381019061027691906119b1565b610a68565b604051610289929190611bfe565b60405180910390f35b6102ac60048036038101906102a7919061190d565b610b24565b005b6102b6610dc5565b6040516102c39190611b45565b60405180910390f35b6102e660048036038101906102e19190611970565b610e04565b005b6102f0610ed8565b6040516102fd9190611c87565b60405180910390f35b610320600480360381019061031b91906118e4565b610ede565b60405161032f93929190611b60565b60405180910390f35b610340610f71565b60405161034d9190611b45565b60405180910390f35b61035e610fb0565b60405161036b9190611c87565b60405180910390f35b61038e6004803603810190610389919061190d565b610fb6565b005b6103aa60048036038101906103a5919061190d565b61111b565b005b6103c660048036038101906103c191906118a8565b611328565b005b60606004600060026000600181526020019081526020016000206000015481526020019081526020016000206000018054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561048b5780601f106104605761010080835404028352916020019161048b565b820191906000526020600020905b81548152906001019060200180831161046e57829003601f168201915b5050505050905080905090565b60008060006104a561168a565b600a600060018152602001908152602001600020600001600086815260200190815260200160002060405180606001604052908160008201548152602001600182015481526020016002820154815250509050806000015181602001518260400151935093509350509193909250565b600b602052806000526040600020600091509050806000018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156105c15780601f10610596576101008083540402835291602001916105c1565b820191906000526020600020905b8154815290600101906020018083116105a457829003601f168201915b5050505050908060010154905082565b600360006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610676576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161066d90611c47565b60405180910390fd5b8260036000600181526020019081526020016000206001016000868152602001908152602001600020600001819055508160036000600181526020019081526020016000206001016000868152602001908152602001600020600101819055508060036000600181526020019081526020016000206001016000868152602001908152602001600020600201819055507f08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be3846003600060018152602001908152602001600020600101600087815260200190815260200160002060000154600360006001815260200190815260200160002060010160008881526020019081526020016000206001015460036000600181526020019081526020016000206001016000898152602001908152602001600020600201546040516107bc9493929190611b97565b60405180910390a150505050565b600360006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161461086f576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161086690611c47565b60405180910390fd5b80600b600060018152602001908152602001600020600001908051906020019061089a9291906116b4565b5050565b6060600b6000600181526020019081526020016000206000018054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561094b5780601f106109205761010080835404028352916020019161094b565b820191906000526020600020905b81548152906001019060200180831161092e57829003601f168201915b5050505050905080905090565b6000806000610965611734565b600a600060018152602001908152602001600020600101600086815260200190815260200160002060405180606001604052908160008201548152602001600182015481526020016002820154815250509050806000015181602001518260400151935093509350509193909250565b60008060006109e261175e565b600360006002600060018152602001908152602001600020600001548152602001908152602001600020600101600086815260200190815260200160002060405180606001604052908160008201548152602001600182015481526020016002820154815250509050806000015181602001518260400151935093509350509193909250565b6004602052806000526040600020600091509050806000018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610b145780601f10610ae957610100808354040283529160200191610b14565b820191906000526020600020905b815481529060010190602001808311610af757829003601f168201915b5050505050908060010154905082565b600360006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610bc9576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610bc090611c47565b60405180910390fd5b600960006001815260200190815260200160002060010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16639a4dd7b3858585856040518563ffffffff1660e01b8152600401610c3f9493929190611b97565b600060405180830381600087803b158015610c5957600080fd5b505af1158015610c6d573d6000803e3d6000fd5b5050505082600a60006001815260200190815260200160002060000160008681526020019081526020016000206000018190555081600a60006001815260200190815260200160002060000160008681526020019081526020016000206001018190555080600a6000600181526020019081526020016000206000016000868152602001908152602001600020600201819055507f4cdf50941fcfa155d7889dac17014ea0ad02c9996b5832c894ad301cd43b7fff84600a600060018152602001908152602001600020600001600087815260200190815260200160002060000154600a600060018152602001908152602001600020600001600088815260200190815260200160002060010154600a600060018152602001908152602001600020600001600089815260200190815260200160002060020154604051610db79493929190611b97565b60405180910390a150505050565b6000600960006001815260200190815260200160002060020160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16905090565b600360006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610ea9576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610ea090611c47565b60405180910390fd5b8060046000600181526020019081526020016000206000019080519060200190610ed49291906116b4565b5050565b600f5481565b6000806000610eeb611788565b600360006002600060018152602001908152602001600020600001548152602001908152602001600020600201600086815260200190815260200160002060405180606001604052908160008201548152602001600182015481526020016002820154815250509050806000015181602001518260400151935093509350509193909250565b6000600960006001815260200190815260200160002060010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16905090565b60085481565b600360006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161461105b576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161105290611c47565b60405180910390fd5b611063611788565b83816000018181525050828160200181815250508181604001818152505080600360006001815260200190815260200160002060020160008781526020019081526020016000206000820151816000015560208201518160010155604082015181600201559050507f4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd8582600001518360200151846040015160405161110c9493929190611b97565b60405180910390a15050505050565b600360006001815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16146111c0576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016111b790611c47565b60405180910390fd5b600960006001815260200190815260200160002060020160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1663ec7503b0858585856040518563ffffffff1660e01b81526004016112369493929190611b97565b600060405180830381600087803b15801561125057600080fd5b505af1158015611264573d6000803e3d6000fd5b50505050611270611734565b83816000018181525050828160200181815250508181604001818152505080600a60006001815260200190815260200160002060010160008781526020019081526020016000206000820151816000015560208201518160010155604082015181600201559050507fd487098619215fac3606de4bd35ef942edeca77d659758fc9d202b1ba0be9e39858260000151836020015184604001516040516113199493929190611b97565b60405180910390a15050505050565b600180600060018152602001908152602001600020600001600082815260200190815260200160002060009054906101000a900460ff161561139f576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161139690611c67565b60405180910390fd5b6113a7611453565b6113b082611472565b6113b86114cb565b82600360006001815260200190815260200160002060000160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550600180600060018152602001908152602001600020600001600083815260200190815260200160002060006101000a81548160ff021916908315150217905550505050565b6001600260006001815260200190815260200160002060000181905550565b80600960006001815260200190815260200160002060010160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b6000600960006001815260200190815260200160002060010160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1690508073ffffffffffffffffffffffffffffffffffffffff166378f1e4986040518163ffffffff1660e01b815260040161153e90611c2e565b602060405180830381600087803b15801561155857600080fd5b505af115801561156c573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190611590919061187f565b600960006001815260200190815260200160002060020160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550600960006001815260200190815260200160002060020160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166319ab453c306040518263ffffffff1660e01b81526004016116559190611b45565b600060405180830381600087803b15801561166f57600080fd5b505af1158015611683573d6000803e3d6000fd5b5050505050565b60405180606001604052806000801916815260200160008019168152602001600080191681525090565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106116f557805160ff1916838001178555611723565b82800160010185558215611723579182015b82811115611722578251825591602001919060010190611707565b5b50905061173091906117b2565b5090565b60405180606001604052806000801916815260200160008019168152602001600080191681525090565b60405180606001604052806000801916815260200160008019168152602001600080191681525090565b60405180606001604052806000801916815260200160008019168152602001600080191681525090565b6117d491905b808211156117d05760008160009055506001016117b8565b5090565b90565b6000813590506117e681611dbb565b92915050565b6000815190506117fb81611dbb565b92915050565b60008135905061181081611dd2565b92915050565b600082601f83011261182757600080fd5b813561183a61183582611ccf565b611ca2565b9150808252602083016020830185838301111561185657600080fd5b611861838284611d68565b50505092915050565b60008135905061187981611de9565b92915050565b60006020828403121561189157600080fd5b600061189f848285016117ec565b91505092915050565b600080604083850312156118bb57600080fd5b60006118c9858286016117d7565b92505060206118da858286016117d7565b9150509250929050565b6000602082840312156118f657600080fd5b600061190484828501611801565b91505092915050565b6000806000806080858703121561192357600080fd5b600061193187828801611801565b945050602061194287828801611801565b935050604061195387828801611801565b925050606061196487828801611801565b91505092959194509250565b60006020828403121561198257600080fd5b600082013567ffffffffffffffff81111561199c57600080fd5b6119a884828501611816565b91505092915050565b6000602082840312156119c357600080fd5b60006119d18482850161186a565b91505092915050565b6119e381611d22565b82525050565b6119f281611d34565b82525050565b6000611a0382611d06565b611a0d8185611d11565b9350611a1d818560208601611d77565b611a2681611daa565b840191505092915050565b6000611a3c82611cfb565b611a468185611d11565b9350611a56818560208601611d77565b611a5f81611daa565b840191505092915050565b7f7061727479000000000000000000000000000000000000000000000000000000815250565b6000611a9d601c83611d11565b91507f4f6e6c79206f776e65722063616e20657865632066756e6374696f6e000000006000830152602082019050919050565b6000611add602783611d11565b91507f436f6e747261637473206461746120696e2063656c6c206a75737420696e697460008301527f69616c697a6564000000000000000000000000000000000000000000000000006020830152604082019050919050565b611b3f81611d5e565b82525050565b6000602082019050611b5a60008301846119da565b92915050565b6000606082019050611b7560008301866119e9565b611b8260208301856119e9565b611b8f60408301846119e9565b949350505050565b6000608082019050611bac60008301876119e9565b611bb960208301866119e9565b611bc660408301856119e9565b611bd360608301846119e9565b95945050505050565b60006020820190508181036000830152611bf681846119f8565b905092915050565b60006040820190508181036000830152611c188185611a31565b9050611c276020830184611b36565b9392505050565b6000602082019050611c4260008301611a6a565b919050565b60006020820190508181036000830152611c6081611a90565b9050919050565b60006020820190508181036000830152611c8081611ad0565b9050919050565b6000602082019050611c9c6000830184611b36565b92915050565b6000604051905081810181811067ffffffffffffffff82111715611cc557600080fd5b8060405250919050565b600067ffffffffffffffff821115611ce657600080fd5b601f19601f8301169050602081019050919050565b600081519050919050565b600081519050919050565b600082825260208201905092915050565b6000611d2d82611d3e565b9050919050565b6000819050919050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b6000819050919050565b82818337600083830152505050565b60005b83811015611d95578082015181840152602081019050611d7a565b83811115611da4576000848401525b50505050565b6000601f19601f8301169050919050565b611dc481611d22565b8114611dcf57600080fd5b50565b611ddb81611d34565b8114611de657600080fd5b50565b611df281611d5e565b8114611dfd57600080fd5b5056fea2646970667358221220953c7ae1ca6d4e73c903060b322fe340c0c6ec47fc943064812f1772fead854464736f6c63430006040033",
}

// ProtoABI is the input ABI used to generate the binding from.
// Deprecated: Use ProtoMetaData.ABI instead.
var ProtoABI = ProtoMetaData.ABI

// ProtoBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use ProtoMetaData.Bin instead.
var ProtoBin = ProtoMetaData.Bin

// DeployProto deploys a new Ethereum contract, binding an instance of Proto to it.
func DeployProto(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *Proto, error) {
	parsed, err := ProtoMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(ProtoBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Proto{ProtoCaller: ProtoCaller{contract: contract}, ProtoTransactor: ProtoTransactor{contract: contract}, ProtoFilterer: ProtoFilterer{contract: contract}}, nil
}

// DeployProtoSync deploys a new Ethereum contract and waits for receipt, binding an instance of ProtoSession to it.
func DeployProtoSync(session *bind.TransactSession, backend bind.ContractBackend) (*types.Transaction, *types.Receipt, *ProtoSession, error) {
	parsed, err := abi.JSON(strings.NewReader(ProtoABI))
	if err != nil {
		return nil, nil, nil, err
	}
	session.Lock()
	address, tx, _, err := bind.DeployContract(session.TransactOpts, parsed, common.FromHex(ProtoBin), backend)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	receipt, err := session.WaitTransaction(tx)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	session.TransactOpts.Nonce.Add(session.TransactOpts.Nonce, big.NewInt(1))
	session.Unlock()
	contractSession, err := NewProtoSession(address, backend, session)
	return tx, receipt, contractSession, err
}

// Proto is an auto generated Go binding around an Ethereum contract.
type Proto struct {
	ProtoCaller     // Read-only binding to the contract
	ProtoTransactor // Write-only binding to the contract
	ProtoFilterer   // Log filterer for contract events
}

// ProtoCaller is an auto generated read-only Go binding around an Ethereum contract.
type ProtoCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ProtoTransactor is an auto generated write-only Go binding around an Ethereum contract.
type ProtoTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ProtoFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type ProtoFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ProtoSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type ProtoSession struct {
	Contract           *Proto // Generic contract binding to set the session for
	transactionSession *bind.TransactSession
	Address            common.Address
}

// ProtoCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type ProtoCallerSession struct {
	Contract *ProtoCaller  // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// ProtoTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type ProtoTransactorSession struct {
	Contract     *ProtoTransactor  // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ProtoRaw is an auto generated low-level Go binding around an Ethereum contract.
type ProtoRaw struct {
	Contract *Proto // Generic contract binding to access the raw methods on
}

// ProtoCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type ProtoCallerRaw struct {
	Contract *ProtoCaller // Generic read-only contract binding to access the raw methods on
}

// ProtoTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type ProtoTransactorRaw struct {
	Contract *ProtoTransactor // Generic write-only contract binding to access the raw methods on
}

// NewProto creates a new instance of Proto, bound to a specific deployed contract.
func NewProto(address common.Address, backend bind.ContractBackend) (*Proto, error) {
	contract, err := bindProto(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Proto{ProtoCaller: ProtoCaller{contract: contract}, ProtoTransactor: ProtoTransactor{contract: contract}, ProtoFilterer: ProtoFilterer{contract: contract}}, nil
}

// NewProtoCaller creates a new read-only instance of Proto, bound to a specific deployed contract.
func NewProtoCaller(address common.Address, caller bind.ContractCaller) (*ProtoCaller, error) {
	contract, err := bindProto(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &ProtoCaller{contract: contract}, nil
}

// NewProtoTransactor creates a new write-only instance of Proto, bound to a specific deployed contract.
func NewProtoTransactor(address common.Address, transactor bind.ContractTransactor) (*ProtoTransactor, error) {
	contract, err := bindProto(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &ProtoTransactor{contract: contract}, nil
}

// NewProtoFilterer creates a new log filterer instance of Proto, bound to a specific deployed contract.
func NewProtoFilterer(address common.Address, filterer bind.ContractFilterer) (*ProtoFilterer, error) {
	contract, err := bindProto(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &ProtoFilterer{contract: contract}, nil
}

func NewProtoSession(address common.Address, backend bind.ContractBackend, transactionSession *bind.TransactSession) (*ProtoSession, error) {
	ProtoInstance, err := NewProto(address, backend)
	if err != nil {
		return nil, err
	}
	return &ProtoSession{
		Contract:           ProtoInstance,
		transactionSession: transactionSession,
		Address:            address,
	}, nil
}

// bindProto binds a generic wrapper to an already deployed contract.
func bindProto(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := ProtoMetaData.GetAbi()
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, *parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Proto *ProtoRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Proto.Contract.ProtoCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Proto *ProtoRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Proto.Contract.ProtoTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Proto *ProtoRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Proto.Contract.ProtoTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Proto *ProtoCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Proto.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Proto *ProtoTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Proto.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Proto *ProtoTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Proto.Contract.contract.Transact(opts, method, params...)
}

// AncestorPublicData is a free data retrieval call binding the contract method 0x78127ba7.
//
// Solidity: function ancestorPublicData(uint256 ) view returns(string version, uint256 i)
func (_Proto *ProtoCaller) AncestorPublicData(opts *bind.CallOpts, arg0 *big.Int) (struct {
	Version string
	I       *big.Int
}, error) {
	var out []interface{}
	err := _Proto.contract.Call(opts, &out, "ancestorPublicData", arg0)

	outstruct := new(struct {
		Version string
		I       *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Version = *abi.ConvertType(out[0], new(string)).(*string)
	outstruct.I = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// AncestorPublicData is a free data retrieval call binding the contract method 0x78127ba7.
//
// Solidity: function ancestorPublicData(uint256 ) view returns(string version, uint256 i)
func (_Proto *ProtoSession) AncestorPublicData(arg0 *big.Int) (struct {
	Version string
	I       *big.Int
}, error) {
	return _Proto.Contract.AncestorPublicData(_Proto.transactionSession.CallOpts, arg0)
}

// AncestorPublicData is a free data retrieval call binding the contract method 0x78127ba7.
//
// Solidity: function ancestorPublicData(uint256 ) view returns(string version, uint256 i)
func (_Proto *ProtoCallerSession) AncestorPublicData(arg0 *big.Int) (struct {
	Version string
	I       *big.Int
}, error) {
	return _Proto.Contract.AncestorPublicData(&_Proto.CallOpts, arg0)
}

// F1 is a free data retrieval call binding the contract method 0xc27fc305.
//
// Solidity: function f1() view returns(uint256)
func (_Proto *ProtoCaller) F1(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Proto.contract.Call(opts, &out, "f1")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// F1 is a free data retrieval call binding the contract method 0xc27fc305.
//
// Solidity: function f1() view returns(uint256)
func (_Proto *ProtoSession) F1() (*big.Int, error) {
	return _Proto.Contract.F1(_Proto.transactionSession.CallOpts)
}

// F1 is a free data retrieval call binding the contract method 0xc27fc305.
//
// Solidity: function f1() view returns(uint256)
func (_Proto *ProtoCallerSession) F1() (*big.Int, error) {
	return _Proto.Contract.F1(&_Proto.CallOpts)
}

// GetAncestorItem is a free data retrieval call binding the contract method 0x76d695a1.
//
// Solidity: function getAncestorItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Proto *ProtoCaller) GetAncestorItem(opts *bind.CallOpts, key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	var out []interface{}
	err := _Proto.contract.Call(opts, &out, "getAncestorItem", key)

	if err != nil {
		return *new([32]byte), *new([32]byte), *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)
	out1 := *abi.ConvertType(out[1], new([32]byte)).(*[32]byte)
	out2 := *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)

	return out0, out1, out2, err

}

// GetAncestorItem is a free data retrieval call binding the contract method 0x76d695a1.
//
// Solidity: function getAncestorItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Proto *ProtoSession) GetAncestorItem(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Proto.Contract.GetAncestorItem(_Proto.transactionSession.CallOpts, key)
}

// GetAncestorItem is a free data retrieval call binding the contract method 0x76d695a1.
//
// Solidity: function getAncestorItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Proto *ProtoCallerSession) GetAncestorItem(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Proto.Contract.GetAncestorItem(&_Proto.CallOpts, key)
}

// GetAncestorItem2 is a free data retrieval call binding the contract method 0xa911ed94.
//
// Solidity: function getAncestorItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Proto *ProtoCaller) GetAncestorItem2(opts *bind.CallOpts, key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	var out []interface{}
	err := _Proto.contract.Call(opts, &out, "getAncestorItem2", key)

	if err != nil {
		return *new([32]byte), *new([32]byte), *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)
	out1 := *abi.ConvertType(out[1], new([32]byte)).(*[32]byte)
	out2 := *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)

	return out0, out1, out2, err

}

// GetAncestorItem2 is a free data retrieval call binding the contract method 0xa911ed94.
//
// Solidity: function getAncestorItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Proto *ProtoSession) GetAncestorItem2(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Proto.Contract.GetAncestorItem2(_Proto.transactionSession.CallOpts, key)
}

// GetAncestorItem2 is a free data retrieval call binding the contract method 0xa911ed94.
//
// Solidity: function getAncestorItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Proto *ProtoCallerSession) GetAncestorItem2(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Proto.Contract.GetAncestorItem2(&_Proto.CallOpts, key)
}

// GetAncestorVersion is a free data retrieval call binding the contract method 0x059ff004.
//
// Solidity: function getAncestorVersion() view returns(string ret)
func (_Proto *ProtoCaller) GetAncestorVersion(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Proto.contract.Call(opts, &out, "getAncestorVersion")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// GetAncestorVersion is a free data retrieval call binding the contract method 0x059ff004.
//
// Solidity: function getAncestorVersion() view returns(string ret)
func (_Proto *ProtoSession) GetAncestorVersion() (string, error) {
	return _Proto.Contract.GetAncestorVersion(_Proto.transactionSession.CallOpts)
}

// GetAncestorVersion is a free data retrieval call binding the contract method 0x059ff004.
//
// Solidity: function getAncestorVersion() view returns(string ret)
func (_Proto *ProtoCallerSession) GetAncestorVersion() (string, error) {
	return _Proto.Contract.GetAncestorVersion(&_Proto.CallOpts)
}

// GetParty is a free data retrieval call binding the contract method 0xb4caa933.
//
// Solidity: function getParty() view returns(address)
func (_Proto *ProtoCaller) GetParty(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Proto.contract.Call(opts, &out, "getParty")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetParty is a free data retrieval call binding the contract method 0xb4caa933.
//
// Solidity: function getParty() view returns(address)
func (_Proto *ProtoSession) GetParty() (common.Address, error) {
	return _Proto.Contract.GetParty(_Proto.transactionSession.CallOpts)
}

// GetParty is a free data retrieval call binding the contract method 0xb4caa933.
//
// Solidity: function getParty() view returns(address)
func (_Proto *ProtoCallerSession) GetParty() (common.Address, error) {
	return _Proto.Contract.GetParty(&_Proto.CallOpts)
}

// GetParty2 is a free data retrieval call binding the contract method 0x90736301.
//
// Solidity: function getParty2() view returns(address)
func (_Proto *ProtoCaller) GetParty2(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Proto.contract.Call(opts, &out, "getParty2")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetParty2 is a free data retrieval call binding the contract method 0x90736301.
//
// Solidity: function getParty2() view returns(address)
func (_Proto *ProtoSession) GetParty2() (common.Address, error) {
	return _Proto.Contract.GetParty2(_Proto.transactionSession.CallOpts)
}

// GetParty2 is a free data retrieval call binding the contract method 0x90736301.
//
// Solidity: function getParty2() view returns(address)
func (_Proto *ProtoCallerSession) GetParty2() (common.Address, error) {
	return _Proto.Contract.GetParty2(&_Proto.CallOpts)
}

// GetProtoItem is a free data retrieval call binding the contract method 0x1b8efb83.
//
// Solidity: function getProtoItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Proto *ProtoCaller) GetProtoItem(opts *bind.CallOpts, key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	var out []interface{}
	err := _Proto.contract.Call(opts, &out, "getProtoItem", key)

	if err != nil {
		return *new([32]byte), *new([32]byte), *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)
	out1 := *abi.ConvertType(out[1], new([32]byte)).(*[32]byte)
	out2 := *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)

	return out0, out1, out2, err

}

// GetProtoItem is a free data retrieval call binding the contract method 0x1b8efb83.
//
// Solidity: function getProtoItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Proto *ProtoSession) GetProtoItem(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Proto.Contract.GetProtoItem(_Proto.transactionSession.CallOpts, key)
}

// GetProtoItem is a free data retrieval call binding the contract method 0x1b8efb83.
//
// Solidity: function getProtoItem(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Proto *ProtoCallerSession) GetProtoItem(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Proto.Contract.GetProtoItem(&_Proto.CallOpts, key)
}

// GetProtoItem2 is a free data retrieval call binding the contract method 0x6c012ade.
//
// Solidity: function getProtoItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Proto *ProtoCaller) GetProtoItem2(opts *bind.CallOpts, key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	var out []interface{}
	err := _Proto.contract.Call(opts, &out, "getProtoItem2", key)

	if err != nil {
		return *new([32]byte), *new([32]byte), *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)
	out1 := *abi.ConvertType(out[1], new([32]byte)).(*[32]byte)
	out2 := *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)

	return out0, out1, out2, err

}

// GetProtoItem2 is a free data retrieval call binding the contract method 0x6c012ade.
//
// Solidity: function getProtoItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Proto *ProtoSession) GetProtoItem2(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Proto.Contract.GetProtoItem2(_Proto.transactionSession.CallOpts, key)
}

// GetProtoItem2 is a free data retrieval call binding the contract method 0x6c012ade.
//
// Solidity: function getProtoItem2(bytes32 key) view returns(bytes32, bytes32, bytes32)
func (_Proto *ProtoCallerSession) GetProtoItem2(key [32]byte) ([32]byte, [32]byte, [32]byte, error) {
	return _Proto.Contract.GetProtoItem2(&_Proto.CallOpts, key)
}

// GetProtoVersion is a free data retrieval call binding the contract method 0x5fdae263.
//
// Solidity: function getProtoVersion() view returns(string ret)
func (_Proto *ProtoCaller) GetProtoVersion(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Proto.contract.Call(opts, &out, "getProtoVersion")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// GetProtoVersion is a free data retrieval call binding the contract method 0x5fdae263.
//
// Solidity: function getProtoVersion() view returns(string ret)
func (_Proto *ProtoSession) GetProtoVersion() (string, error) {
	return _Proto.Contract.GetProtoVersion(_Proto.transactionSession.CallOpts)
}

// GetProtoVersion is a free data retrieval call binding the contract method 0x5fdae263.
//
// Solidity: function getProtoVersion() view returns(string ret)
func (_Proto *ProtoCallerSession) GetProtoVersion() (string, error) {
	return _Proto.Contract.GetProtoVersion(&_Proto.CallOpts)
}

// ProtoPublicData is a free data retrieval call binding the contract method 0x2e695d10.
//
// Solidity: function protoPublicData(uint256 ) view returns(string version, uint256 i)
func (_Proto *ProtoCaller) ProtoPublicData(opts *bind.CallOpts, arg0 *big.Int) (struct {
	Version string
	I       *big.Int
}, error) {
	var out []interface{}
	err := _Proto.contract.Call(opts, &out, "protoPublicData", arg0)

	outstruct := new(struct {
		Version string
		I       *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Version = *abi.ConvertType(out[0], new(string)).(*string)
	outstruct.I = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// ProtoPublicData is a free data retrieval call binding the contract method 0x2e695d10.
//
// Solidity: function protoPublicData(uint256 ) view returns(string version, uint256 i)
func (_Proto *ProtoSession) ProtoPublicData(arg0 *big.Int) (struct {
	Version string
	I       *big.Int
}, error) {
	return _Proto.Contract.ProtoPublicData(_Proto.transactionSession.CallOpts, arg0)
}

// ProtoPublicData is a free data retrieval call binding the contract method 0x2e695d10.
//
// Solidity: function protoPublicData(uint256 ) view returns(string version, uint256 i)
func (_Proto *ProtoCallerSession) ProtoPublicData(arg0 *big.Int) (struct {
	Version string
	I       *big.Int
}, error) {
	return _Proto.Contract.ProtoPublicData(&_Proto.CallOpts, arg0)
}

// Z1 is a free data retrieval call binding the contract method 0x9cbbd15b.
//
// Solidity: function z1() view returns(uint256)
func (_Proto *ProtoCaller) Z1(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Proto.contract.Call(opts, &out, "z1")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Z1 is a free data retrieval call binding the contract method 0x9cbbd15b.
//
// Solidity: function z1() view returns(uint256)
func (_Proto *ProtoSession) Z1() (*big.Int, error) {
	return _Proto.Contract.Z1(_Proto.transactionSession.CallOpts)
}

// Z1 is a free data retrieval call binding the contract method 0x9cbbd15b.
//
// Solidity: function z1() view returns(uint256)
func (_Proto *ProtoCallerSession) Z1() (*big.Int, error) {
	return _Proto.Contract.Z1(&_Proto.CallOpts)
}

// Init is a paid mutator transaction binding the contract method 0xf09a4016.
//
// Solidity: function init(address _owner, address _party) returns()
func (_Proto *ProtoTransactor) Init(opts *bind.TransactOpts, _owner common.Address, _party common.Address) (*types.Transaction, error) {
	return _Proto.contract.Transact(opts, "init", _owner, _party)
}

// Init is a paid mutator transaction binding the contract method 0xf09a4016.
//
// Solidity: function init(address _owner, address _party) returns()
func (_Proto *ProtoSession) Init(_owner common.Address, _party common.Address) (*types.Transaction, *types.Receipt, error) {
	_Proto.transactionSession.Lock()
	tx, err := _Proto.Contract.Init(_Proto.transactionSession.TransactOpts, _owner, _party)
	if err != nil {
		_Proto.transactionSession.Unlock()
		return nil, nil, err
	}
	_Proto.transactionSession.TransactOpts.Nonce.Add(_Proto.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Proto.transactionSession.Unlock()
	receipt, err := _Proto.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// Init is a paid mutator transaction binding the contract method 0xf09a4016.
//
// Solidity: function init(address _owner, address _party) returns()
func (_Proto *ProtoTransactorSession) Init(_owner common.Address, _party common.Address) (*types.Transaction, error) {
	return _Proto.Contract.Init(&_Proto.TransactOpts, _owner, _party)
}

// SetAncestorItem is a paid mutator transaction binding the contract method 0x468d5ac2.
//
// Solidity: function setAncestorItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Proto *ProtoTransactor) SetAncestorItem(opts *bind.TransactOpts, key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Proto.contract.Transact(opts, "setAncestorItem", key, value, dataType, dataDescr)
}

// SetAncestorItem is a paid mutator transaction binding the contract method 0x468d5ac2.
//
// Solidity: function setAncestorItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Proto *ProtoSession) SetAncestorItem(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, *types.Receipt, error) {
	_Proto.transactionSession.Lock()
	tx, err := _Proto.Contract.SetAncestorItem(_Proto.transactionSession.TransactOpts, key, value, dataType, dataDescr)
	if err != nil {
		_Proto.transactionSession.Unlock()
		return nil, nil, err
	}
	_Proto.transactionSession.TransactOpts.Nonce.Add(_Proto.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Proto.transactionSession.Unlock()
	receipt, err := _Proto.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetAncestorItem is a paid mutator transaction binding the contract method 0x468d5ac2.
//
// Solidity: function setAncestorItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Proto *ProtoTransactorSession) SetAncestorItem(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Proto.Contract.SetAncestorItem(&_Proto.TransactOpts, key, value, dataType, dataDescr)
}

// SetAncestorItem2 is a paid mutator transaction binding the contract method 0xdc9a052b.
//
// Solidity: function setAncestorItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Proto *ProtoTransactor) SetAncestorItem2(opts *bind.TransactOpts, key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Proto.contract.Transact(opts, "setAncestorItem2", key, value, dataType, dataDescr)
}

// SetAncestorItem2 is a paid mutator transaction binding the contract method 0xdc9a052b.
//
// Solidity: function setAncestorItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Proto *ProtoSession) SetAncestorItem2(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, *types.Receipt, error) {
	_Proto.transactionSession.Lock()
	tx, err := _Proto.Contract.SetAncestorItem2(_Proto.transactionSession.TransactOpts, key, value, dataType, dataDescr)
	if err != nil {
		_Proto.transactionSession.Unlock()
		return nil, nil, err
	}
	_Proto.transactionSession.TransactOpts.Nonce.Add(_Proto.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Proto.transactionSession.Unlock()
	receipt, err := _Proto.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetAncestorItem2 is a paid mutator transaction binding the contract method 0xdc9a052b.
//
// Solidity: function setAncestorItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Proto *ProtoTransactorSession) SetAncestorItem2(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Proto.Contract.SetAncestorItem2(&_Proto.TransactOpts, key, value, dataType, dataDescr)
}

// SetAncestorVersion is a paid mutator transaction binding the contract method 0x95522e7e.
//
// Solidity: function setAncestorVersion(string value) returns()
func (_Proto *ProtoTransactor) SetAncestorVersion(opts *bind.TransactOpts, value string) (*types.Transaction, error) {
	return _Proto.contract.Transact(opts, "setAncestorVersion", value)
}

// SetAncestorVersion is a paid mutator transaction binding the contract method 0x95522e7e.
//
// Solidity: function setAncestorVersion(string value) returns()
func (_Proto *ProtoSession) SetAncestorVersion(value string) (*types.Transaction, *types.Receipt, error) {
	_Proto.transactionSession.Lock()
	tx, err := _Proto.Contract.SetAncestorVersion(_Proto.transactionSession.TransactOpts, value)
	if err != nil {
		_Proto.transactionSession.Unlock()
		return nil, nil, err
	}
	_Proto.transactionSession.TransactOpts.Nonce.Add(_Proto.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Proto.transactionSession.Unlock()
	receipt, err := _Proto.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetAncestorVersion is a paid mutator transaction binding the contract method 0x95522e7e.
//
// Solidity: function setAncestorVersion(string value) returns()
func (_Proto *ProtoTransactorSession) SetAncestorVersion(value string) (*types.Transaction, error) {
	return _Proto.Contract.SetAncestorVersion(&_Proto.TransactOpts, value)
}

// SetProtoItem is a paid mutator transaction binding the contract method 0x7f890baa.
//
// Solidity: function setProtoItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Proto *ProtoTransactor) SetProtoItem(opts *bind.TransactOpts, key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Proto.contract.Transact(opts, "setProtoItem", key, value, dataType, dataDescr)
}

// SetProtoItem is a paid mutator transaction binding the contract method 0x7f890baa.
//
// Solidity: function setProtoItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Proto *ProtoSession) SetProtoItem(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, *types.Receipt, error) {
	_Proto.transactionSession.Lock()
	tx, err := _Proto.Contract.SetProtoItem(_Proto.transactionSession.TransactOpts, key, value, dataType, dataDescr)
	if err != nil {
		_Proto.transactionSession.Unlock()
		return nil, nil, err
	}
	_Proto.transactionSession.TransactOpts.Nonce.Add(_Proto.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Proto.transactionSession.Unlock()
	receipt, err := _Proto.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetProtoItem is a paid mutator transaction binding the contract method 0x7f890baa.
//
// Solidity: function setProtoItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Proto *ProtoTransactorSession) SetProtoItem(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Proto.Contract.SetProtoItem(&_Proto.TransactOpts, key, value, dataType, dataDescr)
}

// SetProtoItem2 is a paid mutator transaction binding the contract method 0xf047ac94.
//
// Solidity: function setProtoItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Proto *ProtoTransactor) SetProtoItem2(opts *bind.TransactOpts, key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Proto.contract.Transact(opts, "setProtoItem2", key, value, dataType, dataDescr)
}

// SetProtoItem2 is a paid mutator transaction binding the contract method 0xf047ac94.
//
// Solidity: function setProtoItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Proto *ProtoSession) SetProtoItem2(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, *types.Receipt, error) {
	_Proto.transactionSession.Lock()
	tx, err := _Proto.Contract.SetProtoItem2(_Proto.transactionSession.TransactOpts, key, value, dataType, dataDescr)
	if err != nil {
		_Proto.transactionSession.Unlock()
		return nil, nil, err
	}
	_Proto.transactionSession.TransactOpts.Nonce.Add(_Proto.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Proto.transactionSession.Unlock()
	receipt, err := _Proto.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetProtoItem2 is a paid mutator transaction binding the contract method 0xf047ac94.
//
// Solidity: function setProtoItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) returns()
func (_Proto *ProtoTransactorSession) SetProtoItem2(key [32]byte, value [32]byte, dataType [32]byte, dataDescr [32]byte) (*types.Transaction, error) {
	return _Proto.Contract.SetProtoItem2(&_Proto.TransactOpts, key, value, dataType, dataDescr)
}

// SetProtoVersion is a paid mutator transaction binding the contract method 0x47037fdc.
//
// Solidity: function setProtoVersion(string value) returns()
func (_Proto *ProtoTransactor) SetProtoVersion(opts *bind.TransactOpts, value string) (*types.Transaction, error) {
	return _Proto.contract.Transact(opts, "setProtoVersion", value)
}

// SetProtoVersion is a paid mutator transaction binding the contract method 0x47037fdc.
//
// Solidity: function setProtoVersion(string value) returns()
func (_Proto *ProtoSession) SetProtoVersion(value string) (*types.Transaction, *types.Receipt, error) {
	_Proto.transactionSession.Lock()
	tx, err := _Proto.Contract.SetProtoVersion(_Proto.transactionSession.TransactOpts, value)
	if err != nil {
		_Proto.transactionSession.Unlock()
		return nil, nil, err
	}
	_Proto.transactionSession.TransactOpts.Nonce.Add(_Proto.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Proto.transactionSession.Unlock()
	receipt, err := _Proto.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetProtoVersion is a paid mutator transaction binding the contract method 0x47037fdc.
//
// Solidity: function setProtoVersion(string value) returns()
func (_Proto *ProtoTransactorSession) SetProtoVersion(value string) (*types.Transaction, error) {
	return _Proto.Contract.SetProtoVersion(&_Proto.TransactOpts, value)
}

// ProtoAncItem2SetIterator is returned from FilterAncItem2Set and is used to iterate over the raw logs and unpacked data for AncItem2Set events raised by the Proto contract.
type ProtoAncItem2SetIterator struct {
	Event *ProtoAncItem2Set // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *ProtoAncItem2SetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(ProtoAncItem2Set)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(ProtoAncItem2Set)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *ProtoAncItem2SetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *ProtoAncItem2SetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// ProtoAncItem2Set represents a AncItem2Set event raised by the Proto contract.
type ProtoAncItem2Set struct {
	Key       [32]byte
	Value     [32]byte
	DataType  [32]byte
	DataDescr [32]byte
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterAncItem2Set is a free log retrieval operation binding the contract event 0x4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd.
//
// Solidity: event AncItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Proto *ProtoFilterer) FilterAncItem2Set(opts *bind.FilterOpts) (*ProtoAncItem2SetIterator, error) {

	logs, sub, err := _Proto.contract.FilterLogs(opts, "AncItem2Set")
	if err != nil {
		return nil, err
	}
	return &ProtoAncItem2SetIterator{contract: _Proto.contract, event: "AncItem2Set", logs: logs, sub: sub}, nil
}

// WatchAncItem2Set is a free log subscription operation binding the contract event 0x4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd.
//
// Solidity: event AncItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Proto *ProtoFilterer) WatchAncItem2Set(opts *bind.WatchOpts, sink chan<- *ProtoAncItem2Set) (event.Subscription, error) {

	logs, sub, err := _Proto.contract.WatchLogs(opts, "AncItem2Set")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(ProtoAncItem2Set)
				if err := _Proto.contract.UnpackLog(event, "AncItem2Set", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAncItem2Set is a log parse operation binding the contract event 0x4db9fa11d5f00d18505240a201a0671200b9516d868806c2b3d4724d66877dbd.
//
// Solidity: event AncItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Proto *ProtoFilterer) ParseAncItem2Set(log types.Log) (*ProtoAncItem2Set, error) {
	event := new(ProtoAncItem2Set)
	if err := _Proto.contract.UnpackLog(event, "AncItem2Set", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// ProtoAncItemSetIterator is returned from FilterAncItemSet and is used to iterate over the raw logs and unpacked data for AncItemSet events raised by the Proto contract.
type ProtoAncItemSetIterator struct {
	Event *ProtoAncItemSet // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *ProtoAncItemSetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(ProtoAncItemSet)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(ProtoAncItemSet)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *ProtoAncItemSetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *ProtoAncItemSetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// ProtoAncItemSet represents a AncItemSet event raised by the Proto contract.
type ProtoAncItemSet struct {
	Key       [32]byte
	Value     [32]byte
	DataType  [32]byte
	DataDescr [32]byte
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterAncItemSet is a free log retrieval operation binding the contract event 0x08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be3.
//
// Solidity: event AncItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Proto *ProtoFilterer) FilterAncItemSet(opts *bind.FilterOpts) (*ProtoAncItemSetIterator, error) {

	logs, sub, err := _Proto.contract.FilterLogs(opts, "AncItemSet")
	if err != nil {
		return nil, err
	}
	return &ProtoAncItemSetIterator{contract: _Proto.contract, event: "AncItemSet", logs: logs, sub: sub}, nil
}

// WatchAncItemSet is a free log subscription operation binding the contract event 0x08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be3.
//
// Solidity: event AncItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Proto *ProtoFilterer) WatchAncItemSet(opts *bind.WatchOpts, sink chan<- *ProtoAncItemSet) (event.Subscription, error) {

	logs, sub, err := _Proto.contract.WatchLogs(opts, "AncItemSet")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(ProtoAncItemSet)
				if err := _Proto.contract.UnpackLog(event, "AncItemSet", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseAncItemSet is a log parse operation binding the contract event 0x08a5ed56c248a1597d6437cf87fa70303b22368c3898e9cdefadb9b24e4c6be3.
//
// Solidity: event AncItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Proto *ProtoFilterer) ParseAncItemSet(log types.Log) (*ProtoAncItemSet, error) {
	event := new(ProtoAncItemSet)
	if err := _Proto.contract.UnpackLog(event, "AncItemSet", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// ProtoItem2SetIterator is returned from FilterItem2Set and is used to iterate over the raw logs and unpacked data for Item2Set events raised by the Proto contract.
type ProtoItem2SetIterator struct {
	Event *ProtoItem2Set // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *ProtoItem2SetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(ProtoItem2Set)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(ProtoItem2Set)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *ProtoItem2SetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *ProtoItem2SetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// ProtoItem2Set represents a Item2Set event raised by the Proto contract.
type ProtoItem2Set struct {
	Key       [32]byte
	Value     [32]byte
	DataType  [32]byte
	DataDescr [32]byte
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterItem2Set is a free log retrieval operation binding the contract event 0xd487098619215fac3606de4bd35ef942edeca77d659758fc9d202b1ba0be9e39.
//
// Solidity: event Item2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Proto *ProtoFilterer) FilterItem2Set(opts *bind.FilterOpts) (*ProtoItem2SetIterator, error) {

	logs, sub, err := _Proto.contract.FilterLogs(opts, "Item2Set")
	if err != nil {
		return nil, err
	}
	return &ProtoItem2SetIterator{contract: _Proto.contract, event: "Item2Set", logs: logs, sub: sub}, nil
}

// WatchItem2Set is a free log subscription operation binding the contract event 0xd487098619215fac3606de4bd35ef942edeca77d659758fc9d202b1ba0be9e39.
//
// Solidity: event Item2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Proto *ProtoFilterer) WatchItem2Set(opts *bind.WatchOpts, sink chan<- *ProtoItem2Set) (event.Subscription, error) {

	logs, sub, err := _Proto.contract.WatchLogs(opts, "Item2Set")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(ProtoItem2Set)
				if err := _Proto.contract.UnpackLog(event, "Item2Set", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseItem2Set is a log parse operation binding the contract event 0xd487098619215fac3606de4bd35ef942edeca77d659758fc9d202b1ba0be9e39.
//
// Solidity: event Item2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Proto *ProtoFilterer) ParseItem2Set(log types.Log) (*ProtoItem2Set, error) {
	event := new(ProtoItem2Set)
	if err := _Proto.contract.UnpackLog(event, "Item2Set", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// ProtoItemSetIterator is returned from FilterItemSet and is used to iterate over the raw logs and unpacked data for ItemSet events raised by the Proto contract.
type ProtoItemSetIterator struct {
	Event *ProtoItemSet // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *ProtoItemSetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(ProtoItemSet)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(ProtoItemSet)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *ProtoItemSetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *ProtoItemSetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// ProtoItemSet represents a ItemSet event raised by the Proto contract.
type ProtoItemSet struct {
	Key       [32]byte
	Value     [32]byte
	DataType  [32]byte
	DataDescr [32]byte
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterItemSet is a free log retrieval operation binding the contract event 0x4cdf50941fcfa155d7889dac17014ea0ad02c9996b5832c894ad301cd43b7fff.
//
// Solidity: event ItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Proto *ProtoFilterer) FilterItemSet(opts *bind.FilterOpts) (*ProtoItemSetIterator, error) {

	logs, sub, err := _Proto.contract.FilterLogs(opts, "ItemSet")
	if err != nil {
		return nil, err
	}
	return &ProtoItemSetIterator{contract: _Proto.contract, event: "ItemSet", logs: logs, sub: sub}, nil
}

// WatchItemSet is a free log subscription operation binding the contract event 0x4cdf50941fcfa155d7889dac17014ea0ad02c9996b5832c894ad301cd43b7fff.
//
// Solidity: event ItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Proto *ProtoFilterer) WatchItemSet(opts *bind.WatchOpts, sink chan<- *ProtoItemSet) (event.Subscription, error) {

	logs, sub, err := _Proto.contract.WatchLogs(opts, "ItemSet")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(ProtoItemSet)
				if err := _Proto.contract.UnpackLog(event, "ItemSet", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseItemSet is a log parse operation binding the contract event 0x4cdf50941fcfa155d7889dac17014ea0ad02c9996b5832c894ad301cd43b7fff.
//
// Solidity: event ItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr)
func (_Proto *ProtoFilterer) ParseItemSet(log types.Log) (*ProtoItemSet, error) {
	event := new(ProtoItemSet)
	if err := _Proto.contract.UnpackLog(event, "ItemSet", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
