package base_tests

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aklin/linkch/accounts/abi/bind"
	"gitlab.com/aklin/linkch/accounts/abi/bind/backends"
	"gitlab.com/aklin/linkch/common"
	"gitlab.com/aklin/linkch/core"
	"gitlab.com/aklin/linkch/crypto"
	"gitlab.com/aklin/mcbi/bindings"
	infr "gitlab.com/aklin/mcbi/cmd/dao-start/infrastructure"
	"math/big"
	"os"
	"testing"
)

// Можно запустить из папки dao-start (но только здесь, в терминале, из-под среды):
// clear && go test ./dao/base_tests/ -v -timeout 20m
// А можно запускать из RUN

func simTestBackend(testAddr common.Address) *backends.SimulatedBackend {
	return backends.NewSimulatedBackend(
		core.GenesisAlloc{
			testAddr: {Balance: big.NewInt(10000000000000000)},
		}, 10000000,
	)
}

type MainHelper struct {
	suite.Suite
	sim         *backends.SimulatedBackend
	rootAuth    *bind.TransactOpts
	mcatAddr    common.Address
	protoBIAddr common.Address
}

func TestMain(m *testing.M) {
	// Здесь можно сделать инициализацию
	// Запуск тестов и выход
	os.Exit(m.Run())
}

func Test_MCBI_Proto(t *testing.T) {
	s := MainHelper{}
	suite.Run(t, &s)
	s.sim.Close()
	fmt.Println("\n***** Конец тестов")
}

func (s *MainHelper) Test_00_Create_McBi() {
	testKey, err := crypto.GenerateKey()
	s.Empty(err)
	rootAddr := crypto.PubkeyToAddress(testKey.PublicKey)
	s.sim = simTestBackend(rootAddr)
	bgCtx := context.Background()
	code, err := s.sim.CodeAt(bgCtx, rootAddr, nil)
	s.Empty(err)
	s.Empty(code)

	s.rootAuth, err = bind.NewKeyedTransactorWithChainID(testKey, big.NewInt(1337))
	s.Empty(err)

	// ---
	// --- Создание MCAT -----
	// ---
	fmt.Println()
	fmt.Println()
	fmt.Println("-----------------------------")
	fmt.Println("--- Создание MCAT -----------")
	fmt.Println("-----------------------------")
	fmt.Println()

	// Деплой MCAT контракта
	MCATAddr, tx, MCAT, err := bindings.DeployMCATEngine(s.rootAuth, s.sim)
	s.Empty(err)
	code, err = s.sim.PendingCodeAt(bgCtx, MCATAddr)
	s.Empty(err)
	s.NotEmpty(code)
	s.sim.Commit()
	fmt.Println("Создание таблицы адресов MC-контрактов DeployMCATEngine: Потрачено газа", infr.WeiToGwei(tx.Cost()))
	s.mcatAddr = MCATAddr

	// ---
	// --- Заполнение MCAT -----
	// ---
	fmt.Println("\n--- Заполнение MCAT -----")
	fmt.Println()

	// --- Proto -----
	fmt.Println("--- Proto -----")

	// Деплой Proto MC-контракта
	protoMCAddr, tx, _, err := bindings.DeployProto(s.rootAuth, s.sim)
	s.Empty(err)
	code, err = s.sim.PendingCodeAt(bgCtx, protoMCAddr)
	s.Empty(err)
	s.NotEmpty(code)
	s.sim.Commit()
	fmt.Println("Создание Proto MC-контракта DeployProto: Потрачено газа", infr.WeiToGwei(tx.Cost()))

	// Запись Proto MC-контракта в MCAT
	mcProtoKey, err := infr.ToBytes32("proto")
	s.Empty(err)
	tx, err = MCAT.SetMC(s.rootAuth, mcProtoKey, protoMCAddr)
	s.Empty(err)
	s.sim.Commit()
	fmt.Println("Запись адреса Proto MC-контракта в таблицу адресов MC-контрактов MCAT.SetMC('proto',protoMCAddr): Потрачено газа", infr.WeiToGwei(tx.Cost()))

	// --- Party -----
	fmt.Println("--- Party -----")

	// Деплой Party MC-контракта
	partyMCAddr, tx, _, err := bindings.DeployParty(s.rootAuth, s.sim)
	s.Empty(err)
	code, err = s.sim.PendingCodeAt(bgCtx, partyMCAddr)
	s.Empty(err)
	s.NotEmpty(code)
	s.sim.Commit()
	fmt.Println("Создание Party MC-контракта DeployParty: Потрачено газа", infr.WeiToGwei(tx.Cost()))

	// Запись Party MC-контракта в MCAT
	mcPartyKey, err := infr.ToBytes32("party")
	s.Empty(err)
	tx, err = MCAT.SetMC(s.rootAuth, mcPartyKey, partyMCAddr)
	s.Empty(err)
	s.sim.Commit()
	fmt.Println("Запись адреса Party MC-контракта в таблицу адресов MC-контрактов MCAT.SetMC('party',partyMCAddr): Потрачено газа", infr.WeiToGwei(tx.Cost()))

	// ---
	// --- BI Деплои -----
	// ---
	fmt.Println("\n--- BI Деплои -----")
	fmt.Println()

	// --- Proto -----
	fmt.Println("--- Proto -----")

	// Деплой Proto BI-контракта
	s.protoBIAddr, tx, _, err = bindings.DeployBIEngine(s.rootAuth, s.sim, MCATAddr, mcProtoKey)
	s.Empty(err)
	code, err = s.sim.PendingCodeAt(bgCtx, s.protoBIAddr)
	s.Empty(err)
	s.NotEmpty(code)
	s.sim.Commit()
	fmt.Println("Создание контракта-заимствования Proto для Proto MC-контракта DeployBIEngine('proto'): Потрачено газа", infr.WeiToGwei(tx.Cost()))

	// --- Party -----
	fmt.Println("--- Party -----")

	// Деплой Party BI-контракта
	partyBIAddr, tx, _, err := bindings.DeployBIEngine(s.rootAuth, s.sim, MCATAddr, mcPartyKey)
	s.Empty(err)
	code, err = s.sim.PendingCodeAt(bgCtx, partyBIAddr)
	s.Empty(err)
	s.NotEmpty(code)
	s.sim.Commit()
	fmt.Println("Создание контракта-заимствования Party для Party MC-контракта DeployBIEngine('party'): Потрачено газа", infr.WeiToGwei(tx.Cost()))

	// Приведение контракта BI к контракту его заимствования Party
	party, err := bindings.NewParty(partyBIAddr, s.sim)
	s.Empty(err)

	// Инициализация Party
	tx, err = party.Init(s.rootAuth, s.protoBIAddr)
	s.Empty(err)
	s.sim.Commit()
	fmt.Println("Вызов инициализатора Party Init(): Потрачено газа", infr.WeiToGwei(tx.Cost()))

	// Повторный вызов инициализации Party
	tx, err = party.Init(s.rootAuth, common.Address{})
	s.NotEmpty(err)
	fmt.Println("Повторный вызов инициализатора Party Init(): Ошибка", err)

	// --------
	// ----- Инициализация Proto ----
	// --------
	fmt.Println("\n--- Инициализация Proto -----")

	// Приведение контракта BI к контракту его заимствования Proto
	proto, err := bindings.NewProto(s.protoBIAddr, s.sim)
	s.Empty(err)

	// Инициализация Proto
	tx, err = proto.Init(s.rootAuth, s.rootAuth.From, partyBIAddr)
	s.Empty(err)
	s.sim.Commit()
	fmt.Println("Вызов инициализатора Proto Init(): Потрачено газа", infr.WeiToGwei(tx.Cost()))

	// Повторный вызов инициализации Proto
	tx, err = proto.Init(s.rootAuth, common.Address{}, common.Address{})
	s.NotEmpty(err)
	fmt.Println("Повторный вызов инициализатора Proto Init(): Ошибка", err)

	// Получение адреса контракта Party2 из Proto
	party2Addr, err := proto.GetParty2(&bind.CallOpts{From: s.rootAuth.From})
	s.Empty(err)
	party2, err := bindings.NewParty(party2Addr, s.sim)
	s.Empty(err)

	// Повторный вызов (так как первичный был вызван в proto.Init()) инициализации Party
	tx, err = party2.Init(s.rootAuth, common.Address{})
	s.NotEmpty(err)
	fmt.Println("Повторный вызов (так как первичный был вызван в proto.Init()) инициализатора Party2 Init(): Ошибка", err)

	fmt.Println()
	fmt.Println("--- ПРОВЕРКИ -----")
	fmt.Println()
}

func (s *MainHelper) Test_01_Ansestor_Data_Set_And_Check() {
	fmt.Println("--- Ancestor <- Proto: Запись данных, их последующее извлечение и проверка -----")
	s.ansestorDataSetAndCheck("baseAnc")
}

func (s *MainHelper) Test_02_Proto_Data_Set_And_Check() {
	fmt.Println("--- Proto: Запись данных, их последующее извлечение и проверка -----")
	s.protoDataSetAndCheck("baseProto")
}

func (s *MainHelper) Test_03_ReMake_MCAT() {
	// ---
	// --- Перезаполнение MCAT -----
	// ---
	fmt.Println()
	fmt.Println()
	fmt.Println("-----------------------------")
	fmt.Println("--- Перезаполнение MCAT -----")
	fmt.Println("-----------------------------")
	fmt.Println()
	bgCtx := context.Background()
	// Привязка к контракту MCAT
	MCAT, err := bindings.NewMCATEngine(s.mcatAddr, s.sim)
	s.Empty(err)

	// --- Proto -----
	fmt.Println("--- Proto -----")

	// Деплой ProtoMod MC-контракта
	protoModMCAddr, tx, _, err := bindings.DeployProtoMod(s.rootAuth, s.sim)
	s.Empty(err)
	code, err := s.sim.PendingCodeAt(bgCtx, protoModMCAddr)
	s.Empty(err)
	s.NotEmpty(code)
	s.sim.Commit()
	fmt.Println("Создание ProtoMod MC-контракта DeployProtoMod: Потрачено газа", infr.WeiToGwei(tx.Cost()))

	// Запись ProtoMod MC-контракта в MCAT
	mcProtoKey, err := infr.ToBytes32("proto")
	s.Empty(err)
	tx, err = MCAT.SetMC(s.rootAuth, mcProtoKey, protoModMCAddr)
	s.Empty(err)
	s.sim.Commit()
	fmt.Println("Запись адреса ProtoMod MC-контракта в таблицу адресов MC-контрактов MCAT.SetMC('proto',protoModMCAddr): Потрачено газа", infr.WeiToGwei(tx.Cost()))

	// --- Party -----
	fmt.Println("--- Party -----")

	// Деплой PartyMod MC-контракта
	partyModMCAddr, tx, _, err := bindings.DeployPartyMod(s.rootAuth, s.sim)
	s.Empty(err)
	code, err = s.sim.PendingCodeAt(bgCtx, partyModMCAddr)
	s.Empty(err)
	s.NotEmpty(code)
	s.sim.Commit()
	fmt.Println("Создание PartyMod MC-контракта DeployPartyMod: Потрачено газа", infr.WeiToGwei(tx.Cost()))

	// Запись PartyMod MC-контракта в MCAT
	mcPartyKey, err := infr.ToBytes32("party")
	s.Empty(err)
	tx, err = MCAT.SetMC(s.rootAuth, mcPartyKey, partyModMCAddr)
	s.Empty(err)
	s.sim.Commit()
	fmt.Println("Запись адреса PartyMOd MC-контракта в таблицу адресов MC-контрактов MCAT.SetMC('party',partyModMCAddr): Потрачено газа", infr.WeiToGwei(tx.Cost()))

	fmt.Println()
	fmt.Println("--- ПРОВЕРКИ -----")
	fmt.Println()
}

func (s *MainHelper) Test_04_Ansestor_Data_Check_Only() {
	fmt.Println("--- (СТАРАЯ МОДЕЛЬ) --- Ancestor <- Proto: Извлечение и проверка данных -----")
	s.ansestorDataCheckOnly("baseAnc")
}

func (s *MainHelper) Test_05_Proto_Data_Check_Only() {
	fmt.Println("--- (СТАРАЯ МОДЕЛЬ) --- Proto: Извлечение и проверка данных -----")
	s.protoDataCheckOnly("baseProto")
}

func (s *MainHelper) Test_06_Ansestor_Data_Set_And_Check() {
	fmt.Println("--- (СТАРАЯ МОДЕЛЬ) --- Ancestor <- Proto: Запись данных, их последующее извлечение и проверка -----")
	s.ansestorDataSetAndCheck("newBaseAnc")
}

func (s *MainHelper) Test_07_Proto_Data_Set_And_Check() {
	fmt.Println("--- (СТАРАЯ МОДЕЛЬ) --- Proto: Запись данных, их последующее извлечение и проверка -----")
	s.protoDataSetAndCheck("newBaseProto")
}

func (s *MainHelper) Test_08_Ansestor_Mod_Data_Check_Only() {
	fmt.Println("--- (НОВАЯ МОДЕЛЬ) --- Ancestor <- Proto: Извлечение и проверка данных -----")
	s.ansestorModDataCheckOnly("baseAnc")
}

func (s *MainHelper) Test_09_Ansestor_Mod_Data_Check_Only() {
	fmt.Println("--- (НОВАЯ МОДЕЛЬ) --- Ancestor <- Proto: Извлечение и проверка данных -----")
	s.ansestorModDataCheckOnly("newBaseAnc")
}

func (s *MainHelper) Test_10_Ansestor_Data_Set_And_Check() {
	fmt.Println("--- (НОВАЯ МОДЕЛЬ) --- Ancestor <- Proto: Запись данных, их последующее извлечение и проверка -----")
	s.ansestorModDataSetAndCheck("modAnc")
}

func (s *MainHelper) Test_11_Proto_Mod_Data_Check_Only() {
	fmt.Println("--- (НОВАЯ МОДЕЛЬ) --- Proto: Извлечение и проверка данных -----")
	s.protoModDataCheckOnly("baseProto")
}

func (s *MainHelper) Test_12_Proto_Mod_Data_Check_Only() {
	fmt.Println("--- (НОВАЯ МОДЕЛЬ) --- Proto: Извлечение и проверка данных -----")
	s.protoModDataCheckOnly("newBaseProto")
}

func (s *MainHelper) Test_13_Proto_Data_Set_And_Check() {
	fmt.Println("--- (НОВАЯ МОДЕЛЬ) --- Proto: Запись данных, их последующее извлечение и проверка -----")
	s.protoModDataSetAndCheck("modProto")
}

func (s *MainHelper) ansestorDataSetAndCheck(key string) {
	s.ansestorData(true, key)
	fmt.Println()
}

func (s *MainHelper) ansestorDataCheckOnly(key string) {
	s.ansestorData(false, key)
	fmt.Println()
}

func (s *MainHelper) ansestorModDataSetAndCheck(key string) {
	s.ansestorModData(true, false, key)
	fmt.Println()
}

func (s *MainHelper) ansestorModDataCheckOnly(key string) {
	s.ansestorModData(false, true, key)
	fmt.Println()
}

func (s *MainHelper) protoDataSetAndCheck(key string) {
	s.protoData(true, key)
	fmt.Println()
}

func (s *MainHelper) protoDataCheckOnly(key string) {
	s.protoData(false, key)
	fmt.Println()
}

func (s *MainHelper) protoModDataSetAndCheck(key string) {
	s.protoModData(true, false, key)
	fmt.Println()
}

func (s *MainHelper) protoModDataCheckOnly(key string) {
	s.protoModData(false, true, key)
	fmt.Println()
}

func (s *MainHelper) ansestorData(withSet bool, key string) {
	// Приведение контракта BI к контракту его заимствования Proto
	proto, err := bindings.NewProto(s.protoBIAddr, s.sim)
	s.Empty(err)

	channel := make(chan *bindings.ProtoAncItemSet, 1)
	channel2 := make(chan *bindings.ProtoAncItem2Set, 1)
	sub, err := proto.WatchAncItemSet(&bind.WatchOpts{}, channel)
	s.Empty(err)
	sub2, err := proto.WatchAncItem2Set(&bind.WatchOpts{}, channel2)
	s.Empty(err)
	defer sub.Unsubscribe()
	defer sub2.Unsubscribe()

	expectedVersion := "1.1"

	if withSet {
		tx, err := proto.SetAncestorVersion(s.rootAuth, expectedVersion)
		s.Empty(err)
		s.sim.Commit()
		fmt.Println("SetAncestorVersion: Потрачено газа", infr.WeiToGwei(tx.Cost()))
	}

	pubData, err := proto.AncestorPublicData(&bind.CallOpts{From: s.rootAuth.From}, big.NewInt(1))
	s.Empty(err)
	s.Equal(expectedVersion, pubData.Version)

	keyItem := key + "_01"
	key1 := infr.B32(keyItem)
	value1 := infr.B32(keyItem + ": Value")
	type1 := infr.B32(keyItem + ": Type")
	descr1 := infr.B32(keyItem + ": Descr")

	if withSet {
		tx, err := proto.SetAncestorItem(s.rootAuth, key1, value1, type1, descr1)
		s.Empty(err)
		s.sim.Commit()
		fmt.Println("SetAncestorItem: Потрачено газа", infr.WeiToGwei(tx.Cost()))

		event := <-channel
		s.Equal(key1, event.Key)
		s.Equal(value1, event.Value)
		s.Equal(type1, event.DataType)
		s.Equal(descr1, event.DataDescr)
	}

	resultValue01, resultDataType01, resultDataDescr01, err := proto.GetAncestorItem(&bind.CallOpts{From: s.rootAuth.From}, key1)
	s.Equal(value1, resultValue01)
	s.Equal(type1, resultDataType01)
	s.Equal(descr1, resultDataDescr01)

	keyItem2 := key + "_02"
	key2 := infr.B32(keyItem2)
	value2 := infr.B32(keyItem2 + ": Value")
	type2 := infr.B32(keyItem2 + ": Type")
	descr2 := infr.B32(keyItem2 + ": Descr")

	if withSet {
		tx, err := proto.SetAncestorItem2(s.rootAuth, key2, value2, type2, descr2)
		s.Empty(err)
		s.sim.Commit()
		fmt.Println("SetAncestorItem2: Потрачено газа", infr.WeiToGwei(tx.Cost()))

		event := <-channel2
		s.Equal(key2, event.Key)
		s.Equal(value2, event.Value)
		s.Equal(type2, event.DataType)
		s.Equal(descr2, event.DataDescr)
	}

	resultValue02, resultDataType02, resultDataDescr02, err := proto.GetAncestorItem2(&bind.CallOpts{From: s.rootAuth.From}, key2)
	s.Equal(value2, resultValue02)
	s.Equal(type2, resultDataType02)
	s.Equal(descr2, resultDataDescr02)
}

func (s *MainHelper) protoData(withSet bool, key string) {
	// Приведение контракта Proto BI к контракту его заимствования Proto
	proto, err := bindings.NewProto(s.protoBIAddr, s.sim)
	s.Empty(err)
	// Получение адреса контракта Party из Proto
	partyAddr, err := proto.GetParty(&bind.CallOpts{From: s.rootAuth.From})
	s.Empty(err)
	party, err := bindings.NewParty(partyAddr, s.sim)
	s.Empty(err)
	// Получение адреса контракта Party2 из Proto
	party2Addr, err := proto.GetParty2(&bind.CallOpts{From: s.rootAuth.From})
	s.Empty(err)
	party2, err := bindings.NewParty(party2Addr, s.sim)
	s.Empty(err)

	channel := make(chan *bindings.ProtoItemSet, 1)
	channel2 := make(chan *bindings.ProtoItem2Set, 1)
	channelParty := make(chan *bindings.PartyItemSet, 1)
	channelParty2 := make(chan *bindings.PartyItem2Set, 1)
	sub, err := proto.WatchItemSet(&bind.WatchOpts{}, channel)
	s.Empty(err)
	sub2, err := proto.WatchItem2Set(&bind.WatchOpts{}, channel2)
	s.Empty(err)
	subParty, err := party.WatchItemSet(&bind.WatchOpts{}, channelParty)
	s.Empty(err)
	subParty2, err := party2.WatchItem2Set(&bind.WatchOpts{}, channelParty2)
	s.Empty(err)
	defer sub.Unsubscribe()
	defer sub2.Unsubscribe()
	defer subParty.Unsubscribe()
	defer subParty2.Unsubscribe()

	expectedVersion := "1.2"

	if withSet {
		tx, err := proto.SetProtoVersion(s.rootAuth, expectedVersion)
		s.Empty(err)
		s.sim.Commit()
		fmt.Println("SetProtoVersion: Потрачено газа", infr.WeiToGwei(tx.Cost()))
	}

	pubData, err := proto.ProtoPublicData(&bind.CallOpts{From: s.rootAuth.From}, big.NewInt(1))
	s.Empty(err)
	s.Equal(expectedVersion, pubData.Version)

	keyItem := key + "_01"
	key1 := infr.B32(keyItem)
	value1 := infr.B32(keyItem + ": Value")
	type1 := infr.B32(keyItem + ": Type")
	descr1 := infr.B32(keyItem + ": Descr")

	if withSet {
		tx, err := proto.SetProtoItem(s.rootAuth, key1, value1, type1, descr1)
		s.Empty(err)
		s.sim.Commit()
		fmt.Println("SetProtoItem: Потрачено газа", infr.WeiToGwei(tx.Cost()))

		event := <-channel
		s.Equal(key1, event.Key)
		s.Equal(value1, event.Value)
		s.Equal(type1, event.DataType)
		s.Equal(descr1, event.DataDescr)

		eventParty := <-channelParty
		s.Equal(key1, eventParty.Key)
		s.Equal(value1, eventParty.Value)
		s.Equal(type1, eventParty.DataType)
		s.Equal(descr1, eventParty.DataDescr)

		tx, err = party.SetPartyItem(s.rootAuth, key1, value1, type1, descr1)
		s.NotEmpty(err)
		fmt.Println("Party SetPartyItem(): Ошибка", err)
	}

	resultValue11, resultType11, resultDescr11, err := proto.GetProtoItem(&bind.CallOpts{From: s.rootAuth.From}, key1)
	s.Empty(err)
	s.Equal(value1, resultValue11)
	s.Equal(type1, resultType11)
	s.Equal(descr1, resultDescr11)

	resultPartyValue11, resultPartyType11, resultPartyDescr11, err := party.GetPartyItem(&bind.CallOpts{From: s.rootAuth.From}, key1)
	s.Empty(err)
	s.Equal(value1, resultPartyValue11)
	s.Equal(type1, resultPartyType11)
	s.Equal(descr1, resultPartyDescr11)

	keyItem2 := key + "_02"
	key2 := infr.B32(keyItem2)
	value2 := infr.B32(keyItem2 + ": Value")
	type2 := infr.B32(keyItem2 + ": Type")
	descr2 := infr.B32(keyItem2 + ": Descr")

	if withSet {
		tx, err := proto.SetProtoItem2(s.rootAuth, key2, value2, type2, descr2)
		s.Empty(err)
		s.sim.Commit()
		fmt.Println("SetProtoItem2: Потрачено газа", infr.WeiToGwei(tx.Cost()))

		event := <-channel2
		s.Equal(key2, event.Key)
		s.Equal(value2, event.Value)
		s.Equal(type2, event.DataType)
		s.Equal(descr2, event.DataDescr)

		eventParty2 := <-channelParty2
		s.Equal(key2, eventParty2.Key)
		s.Equal(value2, eventParty2.Value)
		s.Equal(type2, eventParty2.DataType)
		s.Equal(descr2, eventParty2.DataDescr)

		tx, err = party2.SetPartyItem2(s.rootAuth, key2, value2, type2, descr2)
		s.NotEmpty(err)
		fmt.Println("Party2 SetPartyItem2(): Ошибка", err)
	}

	resultValue12, resultDataType12, resultDataDescr12, err := proto.GetProtoItem2(&bind.CallOpts{From: s.rootAuth.From}, key2)
	s.Equal(value2, resultValue12)
	s.Equal(type2, resultDataType12)
	s.Equal(descr2, resultDataDescr12)

	resultPartyValue12, resultPartyType12, resultPartyDescr12, err := party2.GetPartyItem2(&bind.CallOpts{From: s.rootAuth.From}, key2)
	s.Empty(err)
	s.Equal(value2, resultPartyValue12)
	s.Equal(type2, resultPartyType12)
	s.Equal(descr2, resultPartyDescr12)
}

func (s *MainHelper) ansestorModData(withSet, oldVersion bool, key string) {
	// Приведение контракта BI к контракту его заимствования Proto
	proto, err := bindings.NewProtoMod(s.protoBIAddr, s.sim)
	s.Empty(err)

	channel := make(chan *bindings.ProtoModAncModItemSet, 1)
	channel2 := make(chan *bindings.ProtoModAncModItem2Set, 1)
	sub, err := proto.WatchAncModItemSet(&bind.WatchOpts{}, channel)
	s.Empty(err)
	sub2, err := proto.WatchAncModItem2Set(&bind.WatchOpts{}, channel2)
	s.Empty(err)
	defer sub.Unsubscribe()
	defer sub2.Unsubscribe()

	expectedVersion := "2.1"
	if oldVersion {
		expectedVersion = "1.1"
	}

	if withSet {
		tx, err := proto.SetAncestorVersion(s.rootAuth, expectedVersion)
		s.Empty(err)
		s.sim.Commit()
		fmt.Println("SetAncestorVersion: Потрачено газа", infr.WeiToGwei(tx.Cost()))
	}

	pubData, err := proto.AncestorPublicData(&bind.CallOpts{From: s.rootAuth.From}, big.NewInt(1))
	s.Empty(err)
	s.Equal(expectedVersion, pubData.Version)

	keyItem := key + "_01"
	key1 := infr.B32(keyItem)
	value1 := infr.B32(keyItem + ": Value")
	type1 := infr.B32(keyItem + ": Type")
	descr1 := infr.B32(keyItem + ": Descr")
	// --- Добавленные поля ---
	sfTiny1 := uint16(123)
	sfBool1 := true
	sfInt1 := big.NewInt(7654)

	if withSet {
		tx, err := proto.SetAncestorModItem(s.rootAuth, key1, value1, type1, descr1, sfTiny1, sfBool1, sfInt1)
		s.Empty(err)
		s.sim.Commit()
		fmt.Println("SetAncestorItem: Потрачено газа", infr.WeiToGwei(tx.Cost()))

		event := <-channel
		s.Equal(key1, event.Key)
		s.Equal(value1, event.Value)
		s.Equal(type1, event.DataType)
		s.Equal(descr1, event.DataDescr)
		// --- Добавленные поля ---
		s.Equal(sfTiny1, event.SfTiny)
		s.Equal(sfBool1, event.SfBool)
		s.Equal(sfInt1, event.SfInt)
	}

	resultValue1, resultDataType1, resultDataDescr1,
		resultSfTiny1, resultSfBool1, resultSfInt1, err := proto.GetAncestorModItem(&bind.CallOpts{From: s.rootAuth.From}, key1)
	s.Equal(value1, resultValue1)
	s.Equal(type1, resultDataType1)
	s.Equal(descr1, resultDataDescr1)
	// --- Добавленные поля ---
	if oldVersion {
		s.Equal(uint16(0), resultSfTiny1)
		s.Equal(false, resultSfBool1)
		s.Equal(int64(0), resultSfInt1.Int64())
	} else {
		s.Equal(sfTiny1, resultSfTiny1)
		s.Equal(sfBool1, resultSfBool1)
		s.Equal(sfInt1, resultSfInt1)
	}

	keyItem2 := key + "_02"
	key2 := infr.B32(keyItem2)
	value2 := infr.B32(keyItem2 + ": Value")
	type2 := infr.B32(keyItem2 + ": Type")
	descr2 := infr.B32(keyItem2 + ": Descr")
	// --- Добавленные поля ---
	sfTiny2 := uint16(321)
	sfBool2 := true
	sfInt2 := big.NewInt(65865)

	if withSet {
		tx, err := proto.SetAncestorModItem2(s.rootAuth, key2, value2, type2, descr2, sfTiny2, sfBool2, sfInt2)
		s.Empty(err)
		s.sim.Commit()
		fmt.Println("SetAncestorItem2: Потрачено газа", infr.WeiToGwei(tx.Cost()))

		event := <-channel2
		s.Equal(key2, event.Key)
		s.Equal(value2, event.Value)
		s.Equal(type2, event.DataType)
		s.Equal(descr2, event.DataDescr)
		// --- Добавленные поля ---
		s.Equal(sfTiny2, event.SfTiny)
		s.Equal(sfBool2, event.SfBool)
		s.Equal(sfInt2, event.SfInt)
	}

	resultValue2, resultDataType2, resultDataDescr2,
		resultSfTiny2, resultSfBool2, resultSfInt2, err := proto.GetAncestorModItem2(&bind.CallOpts{From: s.rootAuth.From}, key2)
	s.Equal(value2, resultValue2)
	s.Equal(type2, resultDataType2)
	s.Equal(descr2, resultDataDescr2)
	// --- Добавленные поля ---
	if oldVersion {
		s.Equal(uint16(0), resultSfTiny2)
		s.Equal(false, resultSfBool2)
		s.Equal(int64(0), resultSfInt2.Int64())
	} else {
		s.Equal(sfTiny2, resultSfTiny2)
		s.Equal(sfBool2, resultSfBool2)
		s.Equal(sfInt2, resultSfInt2)
	}
}

func (s *MainHelper) protoModData(withSet, oldVersion bool, key string) {
	// Приведение контракта Proto BI к контракту его заимствования Proto
	proto, err := bindings.NewProtoMod(s.protoBIAddr, s.sim)
	s.Empty(err)
	// Получение адреса контракта Party из Proto
	partyAddr, err := proto.GetParty(&bind.CallOpts{From: s.rootAuth.From})
	s.Empty(err)
	party, err := bindings.NewPartyMod(partyAddr, s.sim)
	s.Empty(err)
	// Получение адреса контракта Party2 из Proto
	party2Addr, err := proto.GetParty2(&bind.CallOpts{From: s.rootAuth.From})
	s.Empty(err)
	party2, err := bindings.NewPartyMod(party2Addr, s.sim)
	s.Empty(err)

	channel := make(chan *bindings.ProtoModModItemSet, 1)
	channel2 := make(chan *bindings.ProtoModModItem2Set, 1)
	channelParty := make(chan *bindings.PartyModModItemSet, 1)
	channelParty2 := make(chan *bindings.PartyModModItem2Set, 1)
	sub, err := proto.WatchModItemSet(&bind.WatchOpts{}, channel)
	s.Empty(err)
	sub2, err := proto.WatchModItem2Set(&bind.WatchOpts{}, channel2)
	s.Empty(err)
	subParty, err := party.WatchModItemSet(&bind.WatchOpts{}, channelParty)
	s.Empty(err)
	subParty2, err := party2.WatchModItem2Set(&bind.WatchOpts{}, channelParty2)
	s.Empty(err)
	defer sub.Unsubscribe()
	defer sub2.Unsubscribe()
	defer subParty.Unsubscribe()
	defer subParty2.Unsubscribe()

	expectedVersion := "2.2"
	if oldVersion {
		expectedVersion = "1.2"
	}

	if withSet {
		tx, err := proto.SetProtoVersion(s.rootAuth, expectedVersion)
		s.Empty(err)
		s.sim.Commit()
		fmt.Println("SetProtoVersion: Потрачено газа", infr.WeiToGwei(tx.Cost()))
	}

	pubData, err := proto.ProtoPublicData(&bind.CallOpts{From: s.rootAuth.From}, big.NewInt(1))
	s.Empty(err)
	s.Equal(expectedVersion, pubData.Version)

	keyItem := key + "_01"
	key1 := infr.B32(keyItem)
	value1 := infr.B32(keyItem + ": Value")
	type1 := infr.B32(keyItem + ": Type")
	descr1 := infr.B32(keyItem + ": Descr")
	// --- Добавленные поля ---
	sfTiny1 := uint16(789)
	sfBool1 := true
	sfInt1 := big.NewInt(3467)

	if withSet {
		tx, err := proto.SetProtoModItem(s.rootAuth, key1, value1, type1, descr1, sfTiny1, sfBool1, sfInt1)
		s.Empty(err)
		s.sim.Commit()
		fmt.Println("SetProtoItem: Потрачено газа", infr.WeiToGwei(tx.Cost()))

		event := <-channel
		s.Equal(key1, event.Key)
		s.Equal(value1, event.Value)
		s.Equal(type1, event.DataType)
		s.Equal(descr1, event.DataDescr)
		// --- Добавленные поля ---
		s.Equal(sfTiny1, event.SfTiny)
		s.Equal(sfBool1, event.SfBool)
		s.Equal(sfInt1, event.SfInt)

		eventParty := <-channelParty
		s.Equal(key1, eventParty.Key)
		s.Equal(value1, eventParty.Value)
		s.Equal(type1, eventParty.DataType)
		s.Equal(descr1, eventParty.DataDescr)
		// --- Добавленные поля ---
		s.Equal(sfTiny1, eventParty.SfTiny)
		s.Equal(sfBool1, eventParty.SfBool)
		s.Equal(sfInt1, eventParty.SfInt)

		tx, err = party.SetPartyModItem(s.rootAuth, key1, value1, type1, descr1, sfTiny1, sfBool1, sfInt1)
		s.NotEmpty(err)
		fmt.Println("Party SetPartyItem(): Ошибка", err)
	}

	resultValue1, resultType1, resultDescr1,
		resultSfTiny1, resultSfBool1, resultSfInt1, err := proto.GetProtoModItem(&bind.CallOpts{From: s.rootAuth.From}, key1)
	s.Empty(err)
	s.Equal(value1, resultValue1)
	s.Equal(type1, resultType1)
	s.Equal(descr1, resultDescr1)
	// --- Добавленные поля ---
	if oldVersion {
		s.Equal(uint16(0), resultSfTiny1)
		s.Equal(false, resultSfBool1)
		s.Equal(int64(0), resultSfInt1.Int64())
	} else {
		s.Equal(sfTiny1, resultSfTiny1)
		s.Equal(sfBool1, resultSfBool1)
		s.Equal(sfInt1, resultSfInt1)
	}

	resultPartyValue1, resultPartyType1, resultPartyDescr1,
		resultPartySfTiny1, resultPartySfBool1, resultPartySfInt1, err := party.GetPartyModItem(&bind.CallOpts{From: s.rootAuth.From}, key1)
	s.Empty(err)
	s.Equal(value1, resultPartyValue1)
	s.Equal(type1, resultPartyType1)
	s.Equal(descr1, resultPartyDescr1)
	// --- Добавленные поля ---
	if oldVersion {
		s.Equal(uint16(0), resultPartySfTiny1)
		s.Equal(false, resultPartySfBool1)
		s.Equal(int64(0), resultPartySfInt1.Int64())
	} else {
		s.Equal(sfTiny1, resultPartySfTiny1)
		s.Equal(sfBool1, resultPartySfBool1)
		s.Equal(sfInt1, resultPartySfInt1)
	}

	keyItem2 := key + "_02"
	key2 := infr.B32(keyItem2)
	value2 := infr.B32(keyItem2 + ": Value")
	type2 := infr.B32(keyItem2 + ": Type")
	descr2 := infr.B32(keyItem2 + ": Descr")
	// --- Добавленные поля ---
	sfTiny2 := uint16(987)
	sfBool2 := true
	sfInt2 := big.NewInt(8973)

	if withSet {
		tx, err := proto.SetProtoModItem2(s.rootAuth, key2, value2, type2, descr2, sfTiny2, sfBool2, sfInt2)
		s.Empty(err)
		s.sim.Commit()
		fmt.Println("SetProtoItem2: Потрачено газа", infr.WeiToGwei(tx.Cost()))

		event := <-channel2
		s.Equal(key2, event.Key)
		s.Equal(value2, event.Value)
		s.Equal(type2, event.DataType)
		s.Equal(descr2, event.DataDescr)
		// --- Добавленные поля ---
		s.Equal(sfTiny2, event.SfTiny)
		s.Equal(sfBool2, event.SfBool)
		s.Equal(sfInt2, event.SfInt)

		eventParty2 := <-channelParty2
		s.Equal(key2, eventParty2.Key)
		s.Equal(value2, eventParty2.Value)
		s.Equal(type2, eventParty2.DataType)
		s.Equal(descr2, eventParty2.DataDescr)
		// --- Добавленные поля ---
		s.Equal(sfTiny2, eventParty2.SfTiny)
		s.Equal(sfBool2, eventParty2.SfBool)
		s.Equal(sfInt2, eventParty2.SfInt)

		tx, err = party2.SetPartyModItem2(s.rootAuth, key2, value2, type2, descr2, sfTiny2, sfBool2, sfInt2)
		s.NotEmpty(err)
		fmt.Println("Party2 SetPartyItem2(): Ошибка", err)
	}

	resultValue2, resultDataType2, resultDataDescr2,
		resultSfTiny2, resultSfBool2, resultSfInt2, err := proto.GetProtoModItem2(&bind.CallOpts{From: s.rootAuth.From}, key2)
	s.Equal(value2, resultValue2)
	s.Equal(type2, resultDataType2)
	s.Equal(descr2, resultDataDescr2)
	// --- Добавленные поля ---
	if oldVersion {
		s.Equal(uint16(0), resultSfTiny2)
		s.Equal(false, resultSfBool2)
		s.Equal(int64(0), resultSfInt2.Int64())
	} else {
		s.Equal(sfTiny2, resultSfTiny2)
		s.Equal(sfBool2, resultSfBool2)
		s.Equal(sfInt2, resultSfInt2)
	}

	resultPartyValue2, resultPartyType2, resultPartyDescr2,
		resultPartySfTiny2, resultPartySfBool2, resultPartySfInt2, err := party2.GetPartyModItem2(&bind.CallOpts{From: s.rootAuth.From}, key2)
	s.Empty(err)
	s.Equal(value2, resultPartyValue2)
	s.Equal(type2, resultPartyType2)
	s.Equal(descr2, resultPartyDescr2)
	// --- Добавленные поля ---
	if oldVersion {
		s.Equal(uint16(0), resultPartySfTiny2)
		s.Equal(false, resultPartySfBool2)
		s.Equal(int64(0), resultPartySfInt2.Int64())
	} else {
		s.Equal(sfTiny2, resultPartySfTiny2)
		s.Equal(sfBool2, resultPartySfBool2)
		s.Equal(sfInt2, resultPartySfInt2)
	}
}
