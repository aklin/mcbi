package infrastructure

import (
	"bytes"
	"context"
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/aklin/linkch/accounts/abi/bind"
	"gitlab.com/aklin/linkch/ethclient"
	"gitlab.com/aklin/mcbi/cmd/dao-start/utils"
	"gitlab.com/aklin/mcbi/cmd/dao-start/utils/configs"
	"math/big"
)

type Bytes32 = [32]byte

type Client struct {
	ETHClient *ethclient.Client
	TxSession *bind.TransactSession
}

func getConfig(ctx *cli.Context) *utils.Config {
	cfg := utils.Config{}
	clientCfg := configs.ClientConfig{}
	utils.LoadConfig(utils.ConfigPath, &clientCfg)
	cfg.ClientConfig = clientCfg
	//println(fmt.Sprintf("%#v\n", cfg))
	return &cfg
}

func NewClientWithContext(ctx *cli.Context) (client *Client, err error) {
	cfg := getConfig(ctx)
	return NewClient(cfg)
}

func NewClient(cfg *utils.Config) (client *Client, err error) {
	c := Client{}
	c.ETHClient, err = ethclient.DialContext(context.Background(), cfg.ETHURL)
	if err != nil {
		return nil, fmt.Errorf("ошибка NewClient:DialContext - %s", err)
	}
	c.TxSession, err = bind.NewTransactSession(c.ETHClient, cfg.KeyStore, "djhjy7", big.NewInt(int64(cfg.ChainID)))
	if err != nil {
		return nil, fmt.Errorf("ошибка NewClient:NewTransactSession - %s", err)
	}
	return &c, nil
}

func B32(s string) (r Bytes32) {
	copy(r[:], s)
	return
}

func ToString(b Bytes32) string {
	return string(bytes.Trim(b[:], "\000"))
}
