package main

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/aklin/mcbi/cmd/dao-start/utils"
	"os"
)

var app = cli.NewApp()

func init() {
	app.Action = runApp
	app.Name = "McMi 0.01"
	app.Description = "McMi 0.01"
	app.Copyright = "AVKLIN 2023"
	app.Flags = utils.GeneralFlags
	app.Commands = []*cli.Command{
		//&base_tests.Template,
		//&store.Store,
	}
}

func main() {
	if err := app.Run(os.Args); err != nil {
		panic(fmt.Sprintf("Init app error: %+v", err))
	}
}

func runApp(ctx *cli.Context) error {
	return nil
}
