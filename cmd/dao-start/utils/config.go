package utils

import (
	"bufio"
	"encoding/json"
	"github.com/naoina/toml"
	"gitlab.com/aklin/linkch/log"
	"gitlab.com/aklin/mcbi/cmd/dao-start/utils/configs"
	"os"
	"reflect"
)

type Config struct {
	configs.ClientConfig
}

func LoadConfig(path string, config interface{}) {
	f, err := os.Open(path)
	if err != nil {
		log.Error("Failed to open config file", err, "file path", path)
	} else {
		var tomlSettings = toml.Config{
			NormFieldName: func(rt reflect.Type, key string) string {
				return key
			},
			FieldToKey: func(rt reflect.Type, field string) string {
				return field
			},
			MissingField: func(rt reflect.Type, field string) error {
				return nil
			},
		}
		err = tomlSettings.NewDecoder(bufio.NewReader(f)).Decode(config)
		if err != nil {
			log.Crit("Failed to decode config parameter", err, "file path ", path)
		}
		//_ = f.Close()
	}
}

func applyArgs(cfg *Config) *Config {
	cfgJson, err := json.Marshal(cfg)
	if err != nil {
		log.Crit("Failed to marshal config", err)
	}
	err = json.Unmarshal(cfgJson, &cliConfig)
	if err != nil {
		log.Crit("Failed to unmarshal config", err)
	}
	return &cliConfig
}

func LoadConfigAndArgs() (cfg *Config) {
	clientCfg := configs.ClientConfig{}
	LoadConfig(ConfigPath, &clientCfg)
	cfg = &Config{
		ClientConfig: clientCfg,
	}
	cfg = applyArgs(cfg)
	return cfg
}
