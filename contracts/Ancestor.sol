// SPDX-License-Identifier: MIT
//pragma solidity ^0.8.0;
pragma solidity ^0.6.4;
pragma experimental ABIEncoderV2;

contract Ancestor {
    //
    // Модели данных контракта
    //
    struct AncestorItem {
        bytes32 value;
        bytes32 dataType;
        bytes32 dataDescr;
    }
    struct AncestorItem2 {
        bytes32 value;
        bytes32 dataType;
        bytes32 dataDescr;
    }

    // Модели private, internal и public-данных MCBI-шаблона
    //
    struct AncestorPrivateModel { // Private !!!
        uint iProd;
    }
    struct AncestorInternalModel { // Internal !!!
        address owner;
        mapping (bytes32 => AncestorItem) items;
        mapping (bytes32 => AncestorItem2) items2;
    }
    struct AncestorPublicModel { // Public !!!
        string version;
        uint i;
    }

    // Константы
    //
    // PROD это индекс ячейки в мэппингах данных контракта.
    // Теоретически, можно изменить индекс ячейки (это можно применять для отладки и внесения тестовых данных
    // прямо на горячую, в живом мете, в проме), или управлять индексом ячейки в зависимости от адреса исполнителя транзакции
    // или еще в каком-нибудь случае. По большому счету, это сулит захватывающие перспективы, но этот подход требует
    // ОЧЕНЬ СЕРЬЕЗНЫХ мотиваций, предварительно тщательной проработки и масштабного тестирования.
    uint constant PROD = 1;
    uint32 constant ccc1 = 555;

    // События
    //
    event AncItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr);
    event AncItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr);

    // Поля данных контракта по MCBI-шаблону
    //
    mapping (uint => AncestorPrivateModel) private ancestorPrivateData;
    mapping (uint => AncestorInternalModel) internal ancestorInternalData;
    mapping (uint => AncestorPublicModel) public ancestorPublicData;

    // Резерв, на случай, если логика дерева наследования потребует изменений. Если некий контракт
    // унаследует текущий, а уже от того контракта будет наследоваться тот, который сейчас наследуется от текущего.
    // Иными словами, если:
    //      Было:  Ancestor -> Proto
    //      Стало: Ancestor -> SomeOther -> Proto
    // Тогда этот резерв (три слота) удаляется отсюда и переносится в SomeOther, в виде трех слотов, а именно:
    //      mapping (uint => SomeOtherPrivateModel) private someOtherPrivateData;
    //      mapping (uint => SomeOtherInternalModel) internal someOtherInternalData;
    //      mapping (uint => SomeOtherPublicModel) public someOtherPublicData;
    // Если же предполагается более широкая вставка, в виде 2-х наследников, как то
    //      Ancestor -> SomeOther -> SomeOther2 -> Proto
    // То SomeOther2 не сможет иметь своих слотов данных и должен использовать слоты someOtherInternalData
    // и someOtherPublicData своего предка (то есть у него не будет Private-данных).
    uint256[3] __ancestorReserve;

    // Поля данных контракта, добавляемые на этапе первоначальной разработки
    //
    uint public f1;

    // Модифаеры
    //
    modifier onlyOwner {
        require(msg.sender == ancestorInternalData[PROD].owner, "Only owner can exec function");
        _;
    }

    // Геттеры, сеттеры и прочие функции...
    //

    function setIProd() internal {
        // Это такая хитрая проверка, не затрется лм информация из первой ячейки сториджа (0-й по индексу)
        // а ancestorPublicData находится именно в такой ячейке, но по отношению к данному контракту,
        // в BI-контракте после отработки связки наследования в:
        // contract Proto is BIRoot, Ancestor {
        // Если поменять местами BIRoot, Ancestor, т.е. так:
        // contract Proto is Ancestor, BIRoot {
        // То тесты конечно же упадут!
        // Поэтому: во всяком деплойном (т.е. конечным в цепочке наследования) контракте,
        // BIRoot ДОЛЖЕН БЫТЬ ПЕРВЫМ В СПИСКЕ КОНТРАКТОВ-РОДИТЕЛЕЙ !!!
        // Смотри Proto.sol
        ancestorPrivateData[PROD].iProd = PROD;
    }

    function setAncestorVersion(string memory value) public onlyOwner {
        ancestorPublicData[PROD].version = value;
    }

    function getAncestorVersion() public view returns(string memory ret) {
        // Берем индекс ячейки данных не из PROD. Почему?
        // Смотри комментарий у функции setIProd
        ret = ancestorPublicData[ancestorPrivateData[PROD].iProd].version;
        return ret;
    }

    function setAncestorItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) public onlyOwner {
        ancestorInternalData[PROD].items[key].value = value;
        ancestorInternalData[PROD].items[key].dataType = dataType;
        ancestorInternalData[PROD].items[key].dataDescr = dataDescr;
        emit AncItemSet(key,
            ancestorInternalData[PROD].items[key].value,
            ancestorInternalData[PROD].items[key].dataType,
            ancestorInternalData[PROD].items[key].dataDescr
        );
    }

    function getAncestorItem(bytes32 key) public view returns (bytes32, bytes32, bytes32) {
        AncestorItem memory ret = ancestorInternalData[ancestorPrivateData[PROD].iProd].items[key];
        return (ret.value, ret.dataType, ret.dataDescr);
    }

    function setAncestorItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) public onlyOwner {
        AncestorItem2 memory data;
        data.value = value;
        data.dataType = dataType;
        data.dataDescr = dataDescr;
        ancestorInternalData[PROD].items2[key] = data;
        emit AncItem2Set(key, data.value, data.dataType, data.dataDescr);
    }

    function getAncestorItem2(bytes32 key) public view returns (bytes32, bytes32, bytes32) {
        AncestorItem2 memory ret = ancestorInternalData[ancestorPrivateData[PROD].iProd].items2[key];
        return (ret.value, ret.dataType, ret.dataDescr);
    }
}
