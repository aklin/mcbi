// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts (last updated v4.6.0) (proxy/Proxy.sol)

//pragma solidity ^0.8.0;
pragma solidity ^0.6.4;
pragma experimental ABIEncoderV2;

/**
 * @dev This abstract contract provides a fallback function that delegates all calls to another contract using the EVM
 * instruction `delegatecall`. We refer to the second contract as the _implementation_ behind the proxy, and it has to
 * be specified by overriding the virtual {_implementation} function.
 *
 * Additionally, delegation to the implementation can be triggered manually through the {_fallback} function, or to a
 * different contract through the {_delegate} function.
 *
 * The success and return data of the delegated call will be returned back to the caller of the proxy.
 */
abstract contract BIAbstract {
    /**
     * @dev Delegates the current call to `implementation`.
     *
     * This function does not return to its internal call site, it will return directly to the external caller.
     */
    function _delegate(address implementation) internal virtual {
        assembly {
        // Copy msg.data. We take full control of memory in this inline assembly
        // block because it will not return to Solidity code. We overwrite the
        // Solidity scratch pad at memory position 0.
            calldatacopy(0, 0, calldatasize())

        // Call the implementation.
        // out and outsize are 0 because we don't know the size yet.
            let result := delegatecall(gas(), implementation, 0, calldatasize(), 0, 0)

        // Copy the returned data.
            returndatacopy(0, 0, returndatasize())

            switch result
            // delegatecall returns 0 on error.
            case 0 {
                revert(0, returndatasize())
            }
            default {
                return(0, returndatasize())
            }
        }
    }

    /**
     * @dev This is a virtual function that should be overridden so it returns the address to which the fallback
     * function and {_fallback} should delegate.
     */
    function _implementation() internal virtual returns (address);

    /**
     * @dev Delegates the current call to the address returned by `_implementation()`.
     *
     * This function does not return to its internal call site, it will return directly to the external caller.
     */
    function _fallback() internal {
        _delegate(_implementation());
    }

    /**
     * @dev Fallback function that delegates calls to the address returned by `_implementation()`. Will run if no other
     * function in the contract matches the call data.
     */
    fallback() external {
        _fallback();
    }
}

contract BIEngine is BIAbstract {
    struct BIRoot {
        address MCATb;
        bytes32 implKey;
    }
    mapping(uint => BIRoot) BIData;

    constructor (address _MCATb, bytes32 _implKey) public {
        BIRoot memory _BIRoot;
        _BIRoot.MCATb = _MCATb;
        _BIRoot.implKey = _implKey;
        BIData[1] = _BIRoot;
    }

    function _implementation() override internal returns (address) {
        (bool success, bytes memory data) = BIData[1].MCATb.call(abi.encodeWithSignature("getMC(bytes32)", BIData[1].implKey));
        require(success, "MCAT contract getMC execution Failed");
        (address impl) = abi.decode(data, (address));
        return impl;
    }

    function fabriceBI(bytes32 _key) public returns(address) {
        (bool success, bytes memory data) = BIData[1].MCATb.call(abi.encodeWithSignature("fabriceBI(bytes32)", _key));
        require(success, "MCAT contract fabriceBI execution Failed");
        (address _BI) = abi.decode(data, (address));
        return _BI;
    }
}
