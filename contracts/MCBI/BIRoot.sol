// SPDX-License-Identifier: MIT
//pragma solidity ^0.8.0;
pragma solidity ^0.6.4;
pragma experimental ABIEncoderV2;

contract BIRoot {
    struct BIRootModel {
        mapping (uint => bool) justInitSlots;
    }
    uint constant _PROD = 1;
    // Этот слот (slot[0]) нельзя использовать ни в коем случае!
    // Он экранирует BIData (slot[0]) в BIEngine
    uint256[1] private __ProhibitedSlot;
    mapping(uint => BIRootModel) internal __BIRootData;

    modifier initOnlyOnce(uint cell) {
        require(!__BIRootData[_PROD].justInitSlots[cell], "Contracts data in cell just initialized");
        _;
        __BIRootData[_PROD].justInitSlots[cell] = true;
    }
}
