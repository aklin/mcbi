// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts (last updated v4.6.0) (proxy/Proxy.sol)

//pragma solidity ^0.8.0;
pragma solidity ^0.6.4;
pragma experimental ABIEncoderV2;

import "./BIEngine.sol";

contract MCATEngine {
    // address immutable owner; // Появилось только в 0.6.5! Лучше применять, если будем использовать версию solidity выше 0.6.4
    address private owner;
    mapping(bytes32 => address) private MCATable;

    constructor () public {
        owner = msg.sender;
    }

    function setMC(bytes32 _key, address _impl) external {
        require(msg.sender == owner, "Setting implementation can only owner");
        MCATable[_key] = _impl;
    }

    function getMC(bytes32 _key) public view returns(address) {
        return MCATable[_key];
    }

    function fabriceBI(bytes32 _key) public returns(address) {
        require(MCATable[_key] != address(0), "MC address is zero");
        BIEngine _BI = new BIEngine(address(this), _key);
        return address(_BI);
    }
}
