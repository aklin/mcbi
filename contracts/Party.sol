// SPDX-License-Identifier: MIT
//pragma solidity ^0.8.0;
pragma solidity ^0.6.4;
pragma experimental ABIEncoderV2;

import "./MCBI/BIRoot.sol";
import "./Ancestor.sol";

contract Party is BIRoot, Ancestor {
    //
    // Модели данных контракта
    //
    struct PartyItem {
        bytes32 value;
        bytes32 dataType;
        bytes32 dataDescr;
    }
    struct PartyItem2 {
        bytes32 value;
        bytes32 dataType;
        bytes32 dataDescr;
    }

    // Модели private, internal и public-данных MCBI-шаблона
    //
    struct PartyPrivateModel { // Private !!!
        uint i;
    }
    struct PartyInternalModel { // Internal !!!
        mapping (bytes32 => PartyItem) items;
        mapping (bytes32 => PartyItem2) items2;
    }
    struct PartyPublicModel { // Public !!!
        string version;
        uint i;
    }

    // Константы
    //
    uint32 constant zzz1 = 555;

    // События
    //
    event ItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr);
    event Item2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr);

    // Поля данных контракта по MCBI-шаблону
    //
    mapping (uint => PartyPrivateModel) private partyPrivateData;
    mapping (uint => PartyInternalModel) internal partyInternalData;
    mapping (uint => PartyPublicModel) public partyPublicData;
    uint256[3] __partyReserve;

    // Поля данных контракта
    //
    uint public z1;


    // Инициализатор
    //
    function init(address _owner) public initOnlyOnce(PROD) {
        setIProd();
        ancestorInternalData[PROD].owner = _owner;
    }

    // Геттеры, сеттеры и прочие функции...
    //

    function setPartyVersion(string memory value) public onlyOwner {
        partyPublicData[PROD].version = value;
    }

    function getPartyVersion() public view returns(string memory ret) {
        ret = partyPublicData[PROD].version;
        return ret;
    }

    function setPartyItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) public onlyOwner {
        partyInternalData[PROD].items[key].value = value;
        partyInternalData[PROD].items[key].dataType = dataType;
        partyInternalData[PROD].items[key].dataDescr = dataDescr;
        emit ItemSet(key,
            partyInternalData[PROD].items[key].value,
            partyInternalData[PROD].items[key].dataType,
            partyInternalData[PROD].items[key].dataDescr
        );
    }

    function getPartyItem(bytes32 key) public view returns (bytes32, bytes32, bytes32) {
        PartyItem memory ret = partyInternalData[PROD].items[key];
        return (ret.value, ret.dataType, ret.dataDescr);
    }

    function setPartyItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) public onlyOwner {
        PartyItem2 memory data;
        data.value = value;
        data.dataType = dataType;
        data.dataDescr = dataDescr;
        partyInternalData[PROD].items2[key] = data;
        emit Item2Set(key, data.value, data.dataType, data.dataDescr);
    }

    function getPartyItem2(bytes32 key) public view returns (bytes32, bytes32, bytes32) {
        PartyItem2 memory ret = partyInternalData[PROD].items2[key];
        return (ret.value, ret.dataType, ret.dataDescr);
    }
}
