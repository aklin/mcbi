// SPDX-License-Identifier: MIT
//pragma solidity ^0.8.0;
pragma solidity ^0.6.4;
pragma experimental ABIEncoderV2;

import "./MCBI/BIRoot.sol";
import "./MCBI/BIEngine.sol";
import "./AncestorMod.sol";
import "./PartyMod.sol";

contract ProtoMod is BIRoot, AncestorMod {
    //
    // Модели данных контракта
    //
    struct ProtoItem {
        bytes32 value;
        bytes32 dataType;
        bytes32 dataDescr;
        // --- Добавленные поля ---
        uint16 suffixTiny;
        bool suffixBool;
        uint suffixInt;
    }
    struct ProtoItem2 {
        bytes32 value;
        bytes32 dataType;
        bytes32 dataDescr;
        // --- Добавленные поля ---
        uint16 suffixTiny;
        bool suffixBool;
        uint suffixInt;
    }

    // Модели private, internal и public-данных MCBI-шаблона
    //
    struct ProtoPrivateModel { // Private !!!
        uint i;
        PartyMod party;
        PartyMod party2;
        // --- Добавленные поля ---
        uint16 suffixTiny;
        bool suffixBool;
        uint suffixInt;
    }
    struct ProtoInternalModel { // Internal !!!
        mapping (bytes32 => ProtoItem) items;
        mapping (bytes32 => ProtoItem2) items2;
        // --- Добавленные поля ---
        uint16 suffixTiny;
        bool suffixBool;
        uint suffixInt;
    }
    struct ProtoPublicModel { // Public !!!
        string version;
        uint i;
        // --- Добавленные поля ---
        uint16 suffixTiny;
        bool suffixBool;
        uint suffixInt;
    }

    // Константы
    //
    uint32 constant zzz1 = 555;

    // События
    //
    event ItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr);
    event Item2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr);
    // --- События, с учетом добавленных полей ---
    event ModItemSet(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr,
        uint16 sfTiny, bool sfBool, uint sfInt);
    event ModItem2Set(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr,
        uint16 sfTiny, bool sfBool, uint sfInt);

    // Поля данных контракта по MCBI-шаблону
    //
    mapping (uint => ProtoPrivateModel) private protoPrivateData;
    mapping (uint => ProtoInternalModel) internal protoInternalData;
    mapping (uint => ProtoPublicModel) public protoPublicData;
    uint256[3] __protoReserve;

    // Поля данных контракта
    //
    uint public z1;
    // --- Добавленные поля ---
    uint16 suffixTiny;
    bool suffixBool;
    uint suffixInt;


    // Инициализатор
    //
    function init(address _owner, address _party) public initOnlyOnce(PROD) {
        setIProd();
        initParty(_party);
        fabriceParty2();
        ancestorInternalData[PROD].owner = _owner;
    }

    function initParty(address _party) private {
        protoPrivateData[PROD].party = PartyMod(_party);
    }

    function fabriceParty2() private {
        BIEngine fabrice = BIEngine(address(protoPrivateData[PROD].party));
        protoPrivateData[PROD].party2 = PartyMod(fabrice.fabriceBI("party"));
        protoPrivateData[PROD].party2.init(address(this));
    }

    // Геттеры, сеттеры и прочие функции...
    //

    function getParty() public view returns(address) {
        return address(protoPrivateData[PROD].party);
    }

    function getParty2() public view returns(address) {
        return address(protoPrivateData[PROD].party2);
    }

    function setProtoVersion(string memory value) public onlyOwner {
        protoPublicData[PROD].version = value;
    }

    function getProtoVersion() public view returns(string memory ret) {
        ret = protoPublicData[PROD].version;
        return ret;
    }

    function setProtoItem(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) public onlyOwner {
        protoPrivateData[PROD].party.setPartyItem(key, value, dataType, dataDescr);
        protoInternalData[PROD].items[key].value = value;
        protoInternalData[PROD].items[key].dataType = dataType;
        protoInternalData[PROD].items[key].dataDescr = dataDescr;
        emit ItemSet(key,
            protoInternalData[PROD].items[key].value,
            protoInternalData[PROD].items[key].dataType,
            protoInternalData[PROD].items[key].dataDescr
        );
    }

    function getProtoItem(bytes32 key) public view returns (bytes32, bytes32, bytes32) {
        ProtoItem memory ret = protoInternalData[PROD].items[key];
        return (ret.value, ret.dataType, ret.dataDescr);
    }

    function setProtoItem2(bytes32 key, bytes32 value, bytes32 dataType, bytes32 dataDescr) public onlyOwner {
        protoPrivateData[PROD].party2.setPartyItem2(key, value, dataType, dataDescr);
        ProtoItem2 memory data;
        data.value = value;
        data.dataType = dataType;
        data.dataDescr = dataDescr;
        protoInternalData[PROD].items2[key] = data;
        emit Item2Set(key, data.value, data.dataType, data.dataDescr);
    }

    function getProtoItem2(bytes32 key) public view returns (bytes32, bytes32, bytes32) {
        ProtoItem2 memory ret = protoInternalData[PROD].items2[key];
        return (ret.value, ret.dataType, ret.dataDescr);
    }

    // ----------------------------------------------------------------------------------
    // --- Модификация с учетом обработки добавленных полей -----------------------------
    // ----------------------------------------------------------------------------------

    function setProtoModItem(bytes32 key,
        bytes32 value, bytes32 dataType, bytes32 dataDescr,
        uint16 sfTiny, bool sfBool, uint sfInt) public onlyOwner {
        protoPrivateData[PROD].party.setPartyModItem(key,
            value, dataType, dataDescr, sfTiny, sfBool, sfInt);
        protoInternalData[PROD].items[key].value = value;
        protoInternalData[PROD].items[key].dataType = dataType;
        protoInternalData[PROD].items[key].dataDescr = dataDescr;
        // --- Добавленные поля ---
        protoInternalData[PROD].items[key].suffixTiny = sfTiny;
        protoInternalData[PROD].items[key].suffixBool = sfBool;
        protoInternalData[PROD].items[key].suffixInt = sfInt;
        emit ModItemSet(key,
            protoInternalData[PROD].items[key].value,
            protoInternalData[PROD].items[key].dataType,
            protoInternalData[PROD].items[key].dataDescr,
        // --- Добавленные поля ---
            protoInternalData[PROD].items[key].suffixTiny,
            protoInternalData[PROD].items[key].suffixBool,
            protoInternalData[PROD].items[key].suffixInt
        );
    }

    function getProtoModItem(bytes32 key) public view returns (bytes32, bytes32, bytes32, uint16, bool, uint) {
        ProtoItem memory ret = protoInternalData[PROD].items[key];
        return (ret.value, ret.dataType, ret.dataDescr,
                ret.suffixTiny, ret.suffixBool, ret.suffixInt);
    }

    function setProtoModItem2(bytes32 key,
        bytes32 value, bytes32 dataType, bytes32 dataDescr,
        uint16 sfTiny, bool sfBool, uint sfInt) public onlyOwner {
        protoPrivateData[PROD].party2.setPartyModItem2(key,
            value, dataType, dataDescr, sfTiny, sfBool, sfInt);
        ProtoItem2 memory data;
        data.value = value;
        data.dataType = dataType;
        data.dataDescr = dataDescr;
        // --- Добавленные поля ---
        data.suffixTiny = sfTiny;
        data.suffixBool = sfBool;
        data.suffixInt = sfInt;
        protoInternalData[PROD].items2[key] = data;
        emit ModItem2Set(key, data.value, data.dataType, data.dataDescr,
                              // --- Добавленные поля ---
                              data.suffixTiny, data.suffixBool, data.suffixInt);
    }

    function getProtoModItem2(bytes32 key) public view returns (bytes32, bytes32, bytes32, uint16, bool, uint) {
        ProtoItem2 memory ret = protoInternalData[PROD].items2[key];
        return (ret.value, ret.dataType, ret.dataDescr,
                ret.suffixTiny, ret.suffixBool, ret.suffixInt);
    }
}
